# task overflow

Cyberpunk-themed todo app and RPG

[![Built with Cookiecutter Django](https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg?logo=cookiecutter)](https://github.com/cookiecutter/cookiecutter-django/)
[![Ruff](https://img.shields.io/endpoint?url=https://raw.githubusercontent.com/astral-sh/ruff/main/assets/badge/v2.json)](https://github.com/astral-sh/ruff)

License: GPLv3

## Settings

Moved to [settings](http://cookiecutter-django.readthedocs.io/en/latest/settings.html).

## Basic Commands

### Setting Up Your Users

- To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

- To create a **superuser account**, use this command:

      $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

### Type checks

Running type checks with mypy:

    $ mypy task_overflow

### Test coverage

To run the tests, check your test coverage, and generate an HTML coverage report:

    $ coverage run -m pytest
    $ coverage html
    $ open htmlcov/index.html

#### Running tests with pytest

    $ pytest

### Live reloading and Sass CSS compilation

Moved to [Live reloading and SASS compilation](https://cookiecutter-django.readthedocs.io/en/latest/developing-locally.html#sass-compilation-live-reloading).

### Celery

This app comes with Celery.

To run a celery worker:

```bash
cd task_overflow
celery -A config.celery_app worker -l info
```

Please note: For Celery's import magic to work, it is important _where_ the celery commands are run. If you are in the same folder with _manage.py_, you should be right.

To run [periodic tasks](https://docs.celeryq.dev/en/stable/userguide/periodic-tasks.html), you'll need to start the celery beat scheduler service. You can start it as a standalone process:

```bash
cd task_overflow
celery -A config.celery_app beat
```

or you can embed the beat service inside a worker with the `-B` option (not recommended for production use):

```bash
cd task_overflow
celery -A config.celery_app worker -B -l info
```

## Deployment

The following details how to deploy this application.

### Docker

See detailed [cookiecutter-django Docker documentation](http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html).


### Assets Attribution

Default character avatar image is [CyberpunkGirl 8-Bit](https://www.deviantart.com/r-o-m-y/art/CyberpunkGirl-8-Bit-779247640) by r-o-m-y. Creative Commons Attribution 3.0 License.

Default monster avatar image is [V - Cyberpunk pixels](https://www.deviantart.com/maicakes/art/V-Cyberpunk-pixels-863569796) by maicakes. Creative Commons Attribution-NonCommercial-No Derivatives Works 3.0 License.

Other monster images:

[Punk cyberpunk](https://flickr.com/photos/liftarn/53031722898/in/photolist-2oNerBY-24GP2dc-2i1LfFS-2oNcSwH-2p6pLoT-2i1HSom-2p2gian-2oN7FSE-2p2c63y-2p283QZ-2cWUfCT-bjjSoZ-2o9p25M-73K7nh-2nc2WeQ-PH3Kc-2i1HSMx-2i1HRJF-6eUUfY-2feGjj3-2i4AJdn-2i1Mrcf-2i1LhNH-npaEE-4GNe2a-2mWkBtp-4LSCxj-2i1Li4N-2i1Lihy-2kfkqQw-54JBiQ-29R9S2L-2i1HT3n-76JU7L-2i1MrwU-2jcsWhd-2Uy1hY-2i1HTix-NoS8mm-2hFutqR-82nyGD-26QgiWE-25YCDNT-2oNd9zt-2hVni5H-Jj24bG-FhUioU-2UxZZN-241dDq4-M2jZu4) by Staffan Vilcans. Creative Commons Attribution-ShareAlike 2.0 Generic.

[Cyberpunk cyborg](https://flickr.com/photos/liftarn/53226117607/in/photolist-2p6pLoT-2i1HSom-2p2gian-2oN7FSE-2p2c63y-2p283QZ-2cWUfCT-bjjSoZ-2o9p25M-73K7nh-2nc2WeQ-PH3Kc-2i1HSMx-2i1HRJF-6eUUfY-2feGjj3-2i4AJdn-2i1Mrcf-2i1LhNH-npaEE-4GNe2a-2mWkBtp-4LSCxj-2i1Li4N-2i1Lihy-2kfkqQw-54JBiQ-29R9S2L-2i1HT3n-76JU7L-2i1MrwU-2jcsWhd-2Uy1hY-2i1HTix-NoS8mm-2hFutqR-82nyGD-26QgiWE-25YCDNT-2oNd9zt-2hVni5H-Jj24bG-FhUioU-2UxZZN-241dDq4-M2jZu4-zi7am-2hFut2e-2hFvvV4-2pLBFYz) by Staffan Vilcans. Creative Commons Attribution-ShareAlike 2.0 Generic.
