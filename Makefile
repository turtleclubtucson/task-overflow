test:
	docker compose -f local.yml run --rm django python manage.py collectstatic --no-input
	docker compose -f local.yml run --rm django pytest
console:
	docker compose -f local.yml run --rm django bash
shell:
	docker compose -f local.yml run --rm django python manage.py shell
superuser:
	docker compose -f local.yml run --rm django python manage.py createsuperuser
empty:
	docker compose -f local.yml run --rm django python manage.py makemigrations $(APP) --name $(NAME) --empty
migrations:
	docker compose -f local.yml run --rm django python manage.py makemigrations
migrate:
	docker compose -f local.yml run --rm django python manage.py migrate
static:
	docker compose -f local.yml run --rm django python manage.py collectstatic
lint:
	ruff check .
	djlint --check .
format:
	ruff check --fix .
	djlint --reformat .
restart:
	docker compose -f local.yml restart
start:
	docker compose -f local.yml up -d
build:
	docker compose -f local.yml up -d --build
stop:
	docker compose -f local.yml down
logs:
	docker logs task_overflow_local_django
celery:
	docker logs task_overlow_local_celeryworker
publish:
	docker build -t registry.gitlab.com/turtleclubtucson/task-overflow -f compose/production/django/Dockerfile .
	docker push registry.gitlab.com/turtleclubtucson/task-overflow
