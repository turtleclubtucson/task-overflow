from pathlib import Path

TEST_DATA_ROOT = Path(__file__).parent
TEST_IMAGE_FILE = TEST_DATA_ROOT / "test.jpg"
