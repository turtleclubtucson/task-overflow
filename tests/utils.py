from http import HTTPStatus

from django.urls import reverse

PATH_CONTAINS = "PATH_CONTAINS"
PATH_IS_EXACT = "PATH_IS_EXACT"
PATH_STARTS_WITH = "PATH_STARTS_WITH"


def is_htmx_trigger(response, *trigger_names: str) -> bool:
    try:
        header_triggers = [t.strip() for t in response.headers["HX-Trigger"].split(",")]
    except KeyError:
        return False
    return set(header_triggers) == set(trigger_names)


def is_login_redirect(
    response,
    *,
    for_htmx: bool = False,
) -> bool:
    return is_redirect(
        response,
        for_path=reverse("account_login"),
        matching_strategy=PATH_STARTS_WITH,
        for_htmx=for_htmx,
    )


def is_redirect(
    response,
    for_path: str,
    *,
    matching_strategy: str = PATH_IS_EXACT,
    for_htmx: bool = False,
) -> bool:
    if for_htmx:
        try:
            match_url_against = response.headers["HX-Redirect"]
        except KeyError:
            return False
        status_match = response.status_code in (
            HTTPStatus.OK,
            HTTPStatus.FOUND,
        )
    else:
        if "HX-Redirect" in response.headers:
            msg = f"HX-Redirect headers found: {response}"
            raise AttributeError(msg)
        status_match = response.status_code == HTTPStatus.FOUND
        try:
            match_url_against = response.url
        except AttributeError:
            return False

    if matching_strategy == PATH_IS_EXACT:
        url_match = match_url_against == for_path
    elif matching_strategy == PATH_STARTS_WITH:
        url_match = match_url_against.startswith(for_path)
    elif matching_strategy == PATH_CONTAINS:
        url_match = for_path in match_url_against
    else:
        msg = f"Invalid matching strategy: {matching_strategy}"
        raise TypeError(msg)

    return status_match and url_match
