from django.urls import path

from .views import encounter_complete_view
from .views import encounter_detail_view
from .views import encounter_hx_attack_monster_view

app_name = "encounters"
urlpatterns = [
    path(
        "<int:pk>/",
        view=encounter_detail_view,
        name="detail",
    ),
    path(
        "<int:pk>/complete/",
        view=encounter_complete_view,
        name="complete",
    ),
    path(
        "hx/<int:pk>/attack/<int:monster>/",
        view=encounter_hx_attack_monster_view,
        name="encounter-hx-attack-monster",
    ),
]
