from datetime import timedelta
from typing import TYPE_CHECKING

import pytest
from django.db import IntegrityError
from django.utils import timezone

from task_overflow.characters.tests.factories import CharacterDeathFactory
from task_overflow.encounters.models import Encounter
from task_overflow.encounters.tests.factories import EncounterFactory
from task_overflow.encounters.tests.factories import EventFactory

if TYPE_CHECKING:
    from task_overflow.characters.models import CharacterDay
    from task_overflow.monsters.models import Monster


def test_encounter_cannot_have_two_active_encounters_for_same_character_day(
    user1_character_day: "CharacterDay",
):
    character_day = user1_character_day

    EncounterFactory(
        character_day=character_day,
        is_active=True,
    )
    with pytest.raises(IntegrityError):
        EncounterFactory(
            character_day=character_day,
            is_active=True,
        )


def test_encounter_recent_ordered_events_returns_only_most_recently_created_events(
    user1_character_day_encounter: "Encounter",
):
    encounter = user1_character_day_encounter
    events = []
    for i in range(10):
        event = EventFactory(encounter=encounter)
        hours_past = 10 - i
        event.created = timezone.now() - timedelta(hours=hours_past)
        event.save()
        events.append(event)

    assert list(encounter.recent_ordered_events()) == events[5:]


@pytest.mark.usefixtures(
    "_user1_character_day_encounter_attacker_always_hits",
    "_user1_character_fists_were_equipped",
)
def test_encounter_handle_attack_handles_round_of_attacks(
    user1_character_day_encounter_monster: "Monster",
):
    monster = user1_character_day_encounter_monster
    encounter = monster.encounter

    encounter.handle_attack(monster)
    assert encounter.event_set.count() == 2
    assert encounter.event_set.filter(text__contains="hits").count() == 2


def test_encounter_ready_to_complete_is_true_if_character_is_dead(
    user1_character_day_encounter_monster: "Monster",
):
    encounter = user1_character_day_encounter_monster.encounter
    character = encounter.character_day.character
    CharacterDeathFactory(character=character)

    assert encounter.ready_to_complete() is True


def test_encounter_ready_to_complete_is_true_if_no_active_monsters_exist(
    user1_character_day_encounter_monster: "Monster",
):
    monster = user1_character_day_encounter_monster
    monster.is_active = False
    monster.save()
    encounter = monster.encounter

    assert encounter.ready_to_complete() is True


def test_encounter_ready_to_complete_is_false_if_active_monsters_exist(
    user1_character_day_encounter_monster: "Monster",
):
    monster = user1_character_day_encounter_monster
    encounter = monster.encounter

    assert encounter.ready_to_complete() is False


def test_encounter_complete_sets_encounter_active_status_and_adds_credits_if_won(
    mocker,
    user1_character_day_encounter: "Encounter",
):
    # Rolls to figure out credits.
    mocker.patch("random.randint", return_value=1)

    encounter = user1_character_day_encounter
    character = encounter.character_day.character
    expected_credits = character.credits + (character.level + 1)

    encounter.complete()
    encounter.refresh_from_db()
    assert encounter.is_active is False

    character.refresh_from_db()
    assert character.charactermessage_set.filter(
        text=Encounter.FOUND_CREDITS_MESSAGE_TEMPLATE.format(
            credits=character.level + 1,
        ),
    ).exists()
    assert character.credits == expected_credits


def test_encounter_complete_sets_encounter_active_status_and_deducts_credits_if_lost(
    mocker,
    user1_character_day_encounter: "Encounter",
):
    # Rolls to figure out credits.
    mocker.patch("random.randint", return_value=1)

    encounter = user1_character_day_encounter

    character = encounter.character_day.character
    CharacterDeathFactory(character=character)
    expected_credits = character.credits - (character.level + 1)

    encounter.complete()
    encounter.refresh_from_db()
    assert encounter.is_active is False

    character.refresh_from_db()
    assert character.charactermessage_set.filter(
        text=Encounter.STOLEN_CREDITS_MESSAGE_TEMPLATE.format(
            credits=character.level + 1,
        ),
    ).exists()
    assert character.credits == expected_credits


@pytest.mark.usefixtures("user1_character_day_encounter_monster")
def test_encounter_complete_raises_error_if_encounter_not_ready_to_complete(
    user1_character_day_encounter: "Encounter",
):
    encounter = user1_character_day_encounter

    with pytest.raises(Encounter.IsNotCompletedError):
        encounter.complete()
