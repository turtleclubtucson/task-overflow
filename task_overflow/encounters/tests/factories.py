from factory.django import DjangoModelFactory

from task_overflow.encounters.models import Encounter
from task_overflow.encounters.models import Event


class EncounterFactory(DjangoModelFactory):
    class Meta:
        model = Encounter


class EventFactory(DjangoModelFactory):
    class Meta:
        model = Event
