from http import HTTPStatus
from typing import TYPE_CHECKING

import pytest
from django.urls import reverse

from task_overflow.hx.triggers import CHARACTER_DETAILS_CHANGED
from tests.utils import is_htmx_trigger
from tests.utils import is_login_redirect
from tests.utils import is_redirect

if TYPE_CHECKING:
    from django.test.client import Client

    from task_overflow.encounters.models import Encounter
    from task_overflow.monsters.models import Monster


def test_encounter_detail_view_loads_for_logged_in_user(
    user1_character_day_encounter_monster: "Monster",
    user1_client: "Client",
):
    monster = user1_character_day_encounter_monster
    encounter = monster.encounter
    character = encounter.character()
    resp = user1_client.get(
        reverse(
            "encounters:detail",
            kwargs={
                "pk": encounter.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert character.name.encode() in resp.content
    assert monster.name.encode() in resp.content


def test_encounter_detail_view_does_not_load_for_non_owner(
    user1_character_day_encounter: "Encounter",
    user2_client: "Client",
):
    encounter = user1_character_day_encounter
    resp = user2_client.get(
        reverse(
            "encounters:detail",
            kwargs={
                "pk": encounter.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_encounter_detail_view_does_not_load_for_anon_user(
    user1_character_day_encounter: "Encounter",
    client: "Client",
):
    encounter = user1_character_day_encounter
    resp = client.get(
        reverse(
            "encounters:detail",
            kwargs={
                "pk": encounter.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


@pytest.mark.usefixtures("_user1_character_fists_were_equipped")
def test_encounter_hx_attack_monster_view_loads_for_logged_in_user(
    user1_character_day_encounter_monster: "Monster",
    user1_client: "Client",
):
    monster = user1_character_day_encounter_monster
    encounter = monster.encounter

    resp = user1_client.get(
        reverse(
            "encounters:encounter-hx-attack-monster",
            kwargs={"pk": encounter.pk, "monster": monster.pk},
        ),
    )
    assert is_htmx_trigger(resp, CHARACTER_DETAILS_CHANGED)


def test_encounter_hx_attack_monster_view_does_not_load_for_non_owner(
    user1_character_day_encounter_monster: "Monster",
    user2_client: "Client",
):
    monster = user1_character_day_encounter_monster
    encounter = monster.encounter

    resp = user2_client.get(
        reverse(
            "encounters:encounter-hx-attack-monster",
            kwargs={"pk": encounter.pk, "monster": monster.pk},
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_encounter_hx_attack_monster_view_does_not_load_for_anon_user(
    user1_character_day_encounter_monster: "Monster",
    client: "Client",
):
    monster = user1_character_day_encounter_monster
    encounter = monster.encounter

    resp = client.get(
        reverse(
            "encounters:encounter-hx-attack-monster",
            kwargs={"pk": encounter.pk, "monster": monster.pk},
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_encounter_complete_view_loads_for_logged_in_user(
    user1_character_day_encounter: "Encounter",
    user1_client: "Client",
):
    encounter = user1_character_day_encounter
    character = encounter.character()

    resp = user1_client.get(
        reverse(
            "encounters:complete",
            kwargs={
                "pk": encounter.pk,
            },
        ),
    )
    assert is_redirect(
        resp,
        for_path=character.get_absolute_url(),
    )

    encounter.refresh_from_db()
    assert encounter.is_active is False


def test_encounter_complete_view_does_not_load_for_non_owner(
    user1_character_day_encounter: "Encounter",
    user2_client: "Client",
):
    encounter = user1_character_day_encounter

    resp = user2_client.get(
        reverse(
            "encounters:complete",
            kwargs={
                "pk": encounter.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND
    # Encounter was not completed.
    encounter.refresh_from_db()
    assert encounter.is_active is True


def test_encounter_complete_view_does_not_load_for_anon_user(
    user1_character_day_encounter: "Encounter",
    client: "Client",
):
    encounter = user1_character_day_encounter

    resp = client.get(
        reverse(
            "encounters:complete",
            kwargs={
                "pk": encounter.pk,
            },
        ),
    )
    assert is_login_redirect(resp)
    # Encounter was not completed.
    encounter.refresh_from_db()
    assert encounter.is_active is True
