from typing import TYPE_CHECKING

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404
from django.shortcuts import redirect
from django.views.generic.detail import DetailView

from task_overflow.hx.mixins import HXLoginRequiredMixin
from task_overflow.hx.triggers import CHARACTER_DETAILS_CHANGED
from task_overflow.hx.utils import htmx_trigger

from .models import Encounter

if TYPE_CHECKING:
    from task_overflow.monsters.models import Monster


class EncounterDetailView(LoginRequiredMixin, DetailView):
    model = Encounter

    def get_queryset(self) -> QuerySet["Encounter"]:
        return self.request.user.encounters()


encounter_detail_view = EncounterDetailView.as_view()


class EncounterHXAttackMonsterView(HXLoginRequiredMixin, DetailView):
    model = Encounter
    template_name = "encounters/includes/encounter_details.html"

    def monster(self) -> "Monster":
        return get_object_or_404(
            self.object.monster_set.all(),
            pk=self.kwargs["monster"],
        )

    def get_queryset(self) -> QuerySet["Encounter"]:
        return self.request.user.encounters()

    def get(self, *args, **kwargs):
        resp = super().get(*args, **kwargs)
        self.object.handle_attack(
            monster=self.monster(),
        )
        return htmx_trigger(CHARACTER_DETAILS_CHANGED, response=resp)


encounter_hx_attack_monster_view = EncounterHXAttackMonsterView.as_view()


@login_required()
def encounter_complete_view(request, pk: int):
    encounter = get_object_or_404(request.user.encounters(), pk=pk)
    encounter.complete()
    return redirect(encounter.character().get_absolute_url())
