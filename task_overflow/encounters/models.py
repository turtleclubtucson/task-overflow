from typing import TYPE_CHECKING

from django.db import models
from model_utils.models import TimeStampedModel

from task_overflow.utils import d10
from task_overflow.utils import d20
from task_overflow.utils import roll_dice

if TYPE_CHECKING:
    from task_overflow.characters.models import Character
    from task_overflow.monsters.models import Monster


CRITICAL_HIT = 20


class Encounter(TimeStampedModel):
    FOUND_CREDITS_MESSAGE_TEMPLATE = "Found {credits} cryptobucks!"
    STOLEN_CREDITS_MESSAGE_TEMPLATE = "{credits} cryptobucks were stolen in the attack!"

    class IsNotCompletedError(Exception):
        pass

    character_day = models.ForeignKey(
        "characters.CharacterDay",
        on_delete=models.CASCADE,
    )
    is_active = models.BooleanField(default=True)

    def character(self) -> "Character":
        return self.character_day.character

    def recent_ordered_events(self) -> models.query.QuerySet["Event"]:
        return reversed(self.event_set.order_by("-created")[:5])

    def handle_attack(
        self,
        monster: "Monster",
    ) -> None:
        character = self.character()
        attacker1, attacker2 = self._determine_initiative(
            character=character,
            monster=monster,
        )
        self._handle_attack_round(attacker=attacker1, defender=attacker2)
        if attacker2.is_active:
            self._handle_attack_round(attacker=attacker2, defender=attacker1)

    @staticmethod
    def _determine_initiative(character: "Character", monster: "Monster") -> tuple:
        character_initiative = d20() + character.initiative
        monster_initiative = d20() + monster.initiative
        if character_initiative >= monster_initiative:
            return (character, monster)
        return (monster, character)

    def _handle_attack_round(self, attacker, defender) -> None:
        attacker_hits = self._check_if_hit(attacker=attacker, defender=defender)
        if attacker_hits:
            self.event_set.create(text=f"{attacker.name} hits {defender.name}")
            damage = (
                attacker.weapon.calculate_melee_damage()
                + self._calculate_strength_bonus(attacker)
            )
            defender.hit_points -= damage
            defender.save()
        else:
            self.event_set.create(text=f"{attacker.name} misses {defender.name}")

    @staticmethod
    def _check_if_hit(attacker, defender) -> bool:
        attack_roll = d20()
        if attack_roll == CRITICAL_HIT:
            return True
        if attack_roll <= attacker.dexterity:
            defend_roll = d20()
            return defend_roll != CRITICAL_HIT and defend_roll > defender.dexterity
        return False

    @staticmethod
    def _calculate_strength_bonus(attacker) -> int:
        bonus_roll = d20()
        if bonus_roll == CRITICAL_HIT or bonus_roll <= attacker.strength:
            max_bonus = attacker.strength // 2
            return roll_dice(num_sides=max_bonus, num_dice=1)
        return 0

    def ready_to_complete(self) -> bool:
        return (
            not self.character().is_active
            or not self.monster_set.filter(is_active=True).exists()
        )

    def complete(self) -> None:
        if not self.ready_to_complete():
            raise Encounter.IsNotCompletedError(self.pk)

        self.is_active = False
        self.save()
        character = self.character()
        if not character.is_active:
            credits_stolen = min(d10(character.level + 1), character.credits)
            character.credits = character.credits - credits_stolen
            character.save()
            character.add_message(
                Encounter.STOLEN_CREDITS_MESSAGE_TEMPLATE.format(
                    credits=credits_stolen,
                ),
            )
        else:
            credits_found = d10(character.level + 1)
            character.credits += credits_found
            character.save()
            character.add_message(
                Encounter.FOUND_CREDITS_MESSAGE_TEMPLATE.format(credits=credits_found),
            )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["character_day", "is_active"],
                condition=models.Q(is_active=True),
                name="single_active_character_daily_encounter",
            ),
        ]


class Event(TimeStampedModel):
    encounter = models.ForeignKey(
        "Encounter",
        on_delete=models.CASCADE,
    )
    text = models.TextField()
