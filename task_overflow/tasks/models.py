from datetime import date
from datetime import timedelta
from typing import TYPE_CHECKING

from dateutil.relativedelta import relativedelta
from django.core.validators import MinValueValidator
from django.db import models
from django.dispatch import receiver
from django.utils import timezone
from model_utils.models import TimeStampedModel

from .utils import calculate_damage

if TYPE_CHECKING:
    from task_overflow.users.models import User


class Category(TimeStampedModel):
    character = models.ForeignKey(
        "characters.Character",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=127)
    priority = models.IntegerField(default=0)

    def __str__(self) -> str:
        return self.name


class TodoQuerySet(models.QuerySet):
    class InvalidFilterError(Exception):
        pass

    ALLOWED_FILTERS = [
        "pending",
        "backlog",
    ]

    def apply_filter(
        self,
        filter_name: str,
        *args,
        **kwargs,
    ) -> models.QuerySet["Todo"]:
        if filter_name not in TodoQuerySet.ALLOWED_FILTERS:
            raise TodoQuerySet.InvalidFilterError(filter_name)
        return getattr(self, filter_name)(*args, **kwargs)

    def default(self) -> models.QuerySet["Todo"]:
        return self.pending()

    def backlog(self) -> models.QuerySet["Todo"]:
        return (
            self.exclude(pk__in=self.pending().values("pk"))
            .filter(is_complete=False)
            .order_by(
                models.F("due_date").asc(nulls_last=True),
                "created",
            )
        )

    def pending(self) -> models.QuerySet["Todo"]:
        cutoff = timezone.now().date() + timedelta(days=1)
        return self.filter(
            is_complete=False,
            due_date__isnull=False,
            due_date__lte=cutoff,
        ).order_by(
            "due_date",
        )

    def for_category(self, category_name: str) -> models.QuerySet["Todo"]:
        return self.filter(category__name=category_name)


class Todo(TimeStampedModel):
    COMPLETED_MESSAGE_TEMPLATE = "Completed {name}!"
    FAILURE_MESSAGE_TEMPLATE = "Failed {name}!"

    class RecurringSchedule(models.TextChoices):
        NONE = "", ""
        WEEKLY = "1 week"
        MONTHLY = "1 month"
        YEARLY = "1 year"

    RECURRING_SCHEDULE_RELATIVEDELTA = {
        RecurringSchedule.WEEKLY: {"weeks": 1},
        RecurringSchedule.MONTHLY: {"months": 1},
        RecurringSchedule.YEARLY: {"years": 1},
    }

    character = models.ForeignKey(
        "characters.Character",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        "Category",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
    )
    name = models.CharField(
        max_length=255,
    )
    experience_points = models.IntegerField(
        validators=[MinValueValidator(0)],
    )
    due_date = models.DateField(
        default=None,
        blank=True,
        null=True,
    )
    recurring_schedule = models.CharField(
        max_length=7,
        choices=RecurringSchedule.choices,
        default="",
        blank=True,
    )
    notes = models.TextField(
        default="",
        blank=True,
    )
    is_failed = models.BooleanField(
        default=False,
    )
    is_complete = models.BooleanField(
        default=False,
    )

    objects = TodoQuerySet.as_manager()

    @property
    def completed_message_text(self) -> str:
        return Todo.COMPLETED_MESSAGE_TEMPLATE.format(name=self.name)

    @property
    def failed_message_text(self) -> str:
        return Todo.FAILURE_MESSAGE_TEMPLATE.format(name=self.name)

    def complete(self) -> None:
        self.character.experience_points += self.experience_points
        self.character.save()
        self.character.add_message(self.completed_message_text)
        if self.recurring_schedule:
            self.due_date = self._calculate_next_recurring_schedule_due_date(
                self.due_date,
                self.recurring_schedule,
            )
            self.save()
            return
        self.is_complete = True
        self.save()

    @staticmethod
    def _calculate_next_recurring_schedule_due_date(
        due_date: date,
        recurring_schedule: str,
    ) -> "date":
        delta_kwargs = Todo.RECURRING_SCHEDULE_RELATIVEDELTA[recurring_schedule]
        return due_date + relativedelta(**delta_kwargs)

    def fail(self) -> None:
        self.character.hit_points -= calculate_damage(self)
        self.character.save()
        self.experience_points = 0
        self.is_failed = True
        self.save()
        self.character.add_message(self.failed_message_text)


class Habit(TimeStampedModel):
    class DidNotExistError(Exception):
        pass

    COMPLETED_MESSAGE_TEMPLATE = "Completed habit {name}!"
    FAILED_MESSAGE_TEMPLATE = "Failed habit {name}!"

    character = models.ForeignKey(
        "characters.Character",
        on_delete=models.CASCADE,
    )
    category = models.ForeignKey(
        "Category",
        on_delete=models.SET_NULL,
        null=True,
        blank=True,
        default=None,
    )
    name = models.CharField(
        max_length=255,
    )
    experience_points = models.IntegerField(
        validators=[MinValueValidator(0)],
    )

    def user(self) -> "User":
        return self.character.user

    @property
    def completed_message_text(self) -> str:
        return Habit.COMPLETED_MESSAGE_TEMPLATE.format(name=self.name)

    @property
    def failed_message_text(self) -> str:
        return Habit.FAILED_MESSAGE_TEMPLATE.format(name=self.name)

    def do(self) -> None:
        added_xp = self.experience_points + (self.streak() * 10)
        self.character.experience_points += added_xp
        self.character.save()
        self.habitdone_set.create()
        self.character.add_message(self.completed_message_text)

    def fail(self) -> None:
        self.character.hit_points -= calculate_damage(self)
        self.character.save()
        self.character.add_message(self.failed_message_text)

    def streak(self) -> int:
        # Today's habit activity adds to a streak, but the streak isn't broken yet
        # if a player hasn't finished their habit yet today.
        current_character_day = self.character.character_day()
        if current_character_day.habitdone_set.filter(habit=self).exists():
            count = 1
        else:
            count = 0

        character_day = self.character.characterday_set.filter(
            day=current_character_day.day - timedelta(days=1),
        ).first()
        while character_day:
            if character_day.habitdone_set.filter(habit=self).exists():
                count += 1
                character_day = self.character.characterday_set.filter(
                    day=character_day.day - timedelta(days=1),
                ).first()
            else:
                break
        return count


class HabitDone(TimeStampedModel):
    habit = models.ForeignKey(
        "Habit",
        on_delete=models.CASCADE,
    )
    character_day = models.ForeignKey(
        "characters.CharacterDay",
        on_delete=models.CASCADE,
        null=True,
        default=None,
    )


@receiver(models.signals.pre_save, sender=HabitDone)
def set_character_day_based_on_habit(sender, instance, **kwargs) -> None:
    if not instance.character_day:
        instance.character_day = instance.habit.character.character_day()
