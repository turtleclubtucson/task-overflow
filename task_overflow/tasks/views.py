from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.urls import reverse
from django.views.decorators.http import require_http_methods
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.edit import UpdateView
from django.views.generic.list import ListView

from task_overflow.hx.decorators import hx_login_required
from task_overflow.hx.mixins import HXFormViewMixin
from task_overflow.hx.mixins import HXLoginRequiredMixin
from task_overflow.hx.triggers import CHARACTER_DETAILS_CHANGED
from task_overflow.hx.triggers import TODO_ADDED
from task_overflow.hx.utils import htmx_trigger
from task_overflow.mixins import CharacterRelatedDataViewMixin
from task_overflow.mixins import DefaultFormViewMixin
from task_overflow.mixins import UserFormViewMixin
from task_overflow.utils import empty_response

from .forms import HabitForm
from .forms import TodoForm
from .models import Category
from .models import Habit
from .models import Todo

DEFAULT_FILTER = "pending"


class CategoryCreateView(
    LoginRequiredMixin,
    CharacterRelatedDataViewMixin,
    DefaultFormViewMixin,
    CreateView,
):
    model = Category
    fields = [
        "name",
        "priority",
    ]
    success_message = "Category created"
    form_header = "Create Category"
    redirect_to_character_on_success = True
    form_instance_assignments = {"character": "character"}

    @property
    def form_message(self) -> str:
        return self.character().name


category_create_view = CategoryCreateView.as_view()


class CategoryDetailView(LoginRequiredMixin, DetailView):
    model = Category

    def get_queryset(self) -> QuerySet["Category"]:
        return self.request.user.categories()


category_detail_view = CategoryDetailView.as_view()


class TodoHXCreateView(
    HXLoginRequiredMixin,
    HXFormViewMixin,
    CharacterRelatedDataViewMixin,
    UserFormViewMixin,
    CreateView,
):
    model = Todo
    form_class = TodoForm
    template_name = "tasks/includes/todo_list_form.html"
    success_message = "Todo created"
    form_header = "Create Todo"
    form_instance_assignments = {"character": "character"}
    hx_target_id = "createTodoForm"
    hx_template_name = "tasks/includes/todo_list_item.html"
    hx_triggers = [
        TODO_ADDED,
    ]

    def category(self) -> "Category | None":
        param_category = self.request.GET.get("c")
        if not param_category:
            return None
        return self.character().category_set.filter(name=param_category).first()

    def get_initial(self, *args, **kwargs) -> dict:
        initial = super().get_initial(*args, **kwargs)
        initial["category"] = self.category()
        return initial


todo_hx_create_view = TodoHXCreateView.as_view()


class TodoHXListView(
    HXLoginRequiredMixin,
    CharacterRelatedDataViewMixin,
    ListView,
):
    model = Todo
    template_name = "tasks/includes/todo_list.html"

    @property
    def category_name(self) -> str:
        return self.request.GET.get("c", None)

    @property
    def filter_name(self) -> str:
        return self.request.GET.get("f", DEFAULT_FILTER)

    def filtered_todos(self) -> QuerySet["Todo"]:
        filtered = self.character().todo_set.apply_filter(self.filter_name)
        if self.category_name:
            filtered = filtered.for_category(self.category_name)
        return filtered

    def get_queryset(self) -> QuerySet["Todo"]:
        return self.filtered_todos()


todo_hx_list_view = TodoHXListView.as_view()


class TodoHXListItemView(HXLoginRequiredMixin, DetailView):
    model = Todo
    template_name = "tasks/includes/todo_list_item.html"

    def get_queryset(self) -> QuerySet["Todo"]:
        return self.request.user.todos()


todo_hx_list_item_view = TodoHXListItemView.as_view()


class TodoHXNotesShowView(HXLoginRequiredMixin, DetailView):
    model = Todo
    template_name = "tasks/includes/todo_notes_show.html"

    def get_queryset(self) -> QuerySet["Todo"]:
        return self.request.user.todos()


todo_hx_notes_show_view = TodoHXNotesShowView.as_view()


class TodoHXNotesHideView(HXLoginRequiredMixin, DetailView):
    model = Todo
    template_name = "tasks/includes/todo_notes_hide.html"

    def get_queryset(self) -> QuerySet["Todo"]:
        return self.request.user.todos()


todo_hx_notes_hide_view = TodoHXNotesHideView.as_view()


class TodoHXUpdateView(
    HXLoginRequiredMixin,
    HXFormViewMixin,
    CharacterRelatedDataViewMixin,
    UserFormViewMixin,
    UpdateView,
):
    model = Todo
    form_class = TodoForm
    template_name = "tasks/includes/todo_list_form.html"
    success_message = "Todo updated"
    form_header = "Update Todo"
    hx_template_name = "tasks/includes/todo_list_item.html"

    @property
    def cancel_path(self) -> str:
        return reverse(
            "tasks:todo-hx-list-item",
            kwargs={
                "pk": self.object.pk,
            },
        )

    @property
    def hx_target_id(self) -> str:
        return f"todo-{self.object.pk}"

    def get_queryset(self) -> QuerySet["Todo"]:
        return self.request.user.todos()


todo_hx_update_view = TodoHXUpdateView.as_view()


@require_http_methods(["POST"])
@hx_login_required
def todo_hx_complete_view(request, pk: int):
    todo = get_object_or_404(request.user.incomplete_todos(), pk=pk)
    todo.complete()
    return htmx_trigger(CHARACTER_DETAILS_CHANGED)


@require_http_methods(["POST"])
@hx_login_required
def todo_hx_fail_view(request, pk: int):
    todo = get_object_or_404(request.user.todos(), pk=pk)
    todo.fail()
    return htmx_trigger(
        CHARACTER_DETAILS_CHANGED,
        response=render(
            request,
            "tasks/includes/todo_list_item.html",
            context={
                "todo": todo,
            },
        ),
    )


@require_http_methods(["POST"])
@hx_login_required
def todo_hx_due_today_view(request, pk: int):
    todo = get_object_or_404(request.user.todos(), pk=pk)
    todo.due_date = request.user.today()
    todo.save()
    return render(
        request,
        "tasks/includes/todo_list_item.html",
        context={
            "todo": todo,
        },
    )


@require_http_methods(["POST"])
@hx_login_required
def todo_hx_delete_view(request, pk: int):
    todo = get_object_or_404(request.user.todos(), pk=pk)
    todo.delete()
    return empty_response()


class HabitCreateView(
    LoginRequiredMixin,
    UserFormViewMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    CreateView,
):
    model = Habit
    form_class = HabitForm
    form_header = "Create Habit"
    redirect_to_character_on_success = True
    form_instance_assignments = {"character": "character"}

    @property
    def form_message(self) -> str:
        return self.character().name


habit_create_view = HabitCreateView.as_view()


class HabitUpdateView(
    LoginRequiredMixin,
    UserFormViewMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    UpdateView,
):
    model = Habit
    form_class = HabitForm
    form_header = "Update Habit"
    redirect_to_character_on_success = True

    @property
    def form_message(self) -> str:
        return self.object.name

    def get_queryset(self) -> QuerySet["Habit"]:
        return self.request.user.habits()


habit_update_view = HabitUpdateView.as_view()


class HabitDeleteView(
    LoginRequiredMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    DeleteView,
):
    model = Habit
    form_header = "Delete Habit"
    redirect_to_character_on_success = True

    @property
    def form_message(self) -> str:
        return self.object.name

    def get_queryset(self) -> QuerySet["Habit"]:
        return self.request.user.habits()


habit_delete_view = HabitDeleteView.as_view()


class HabitDoView(
    LoginRequiredMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    UpdateView,
):
    model = Habit
    fields = []
    form_header = "Do Habit"
    redirect_to_character_on_success = True

    @property
    def form_message(self) -> str:
        return f"Are you sure you want to do {self.object.name}?"

    def get_queryset(self) -> QuerySet["Habit"]:
        return self.request.user.habits()

    def form_valid(self, form):
        resp = super().form_valid(form)
        form.instance.do()
        return resp


habit_do_view = HabitDoView.as_view()
