from factory import Faker
from factory.django import DjangoModelFactory

from task_overflow.tasks.models import Category
from task_overflow.tasks.models import Habit
from task_overflow.tasks.models import HabitDone
from task_overflow.tasks.models import Todo


class CategoryFactory(DjangoModelFactory):
    name = Faker("name")
    priority = Faker(
        "pyint",
        min_value=1,
        max_value=100,
    )

    class Meta:
        model = Category


class TodoFactory(DjangoModelFactory):
    name = Faker("name")
    experience_points = Faker(
        "pyint",
        min_value=1,
        max_value=10,
    )

    class Meta:
        model = Todo


class HabitFactory(DjangoModelFactory):
    name = Faker("name")
    experience_points = Faker(
        "pyint",
        min_value=0,
        max_value=10,
    )

    class Meta:
        model = Habit


class HabitDoneFactory(DjangoModelFactory):
    class Meta:
        model = HabitDone
