from datetime import timedelta
from http import HTTPStatus
from typing import TYPE_CHECKING

import pytest
from django.urls import reverse

from task_overflow.hx.triggers import CHARACTER_DETAILS_CHANGED
from task_overflow.hx.triggers import TODO_ADDED
from task_overflow.tasks.models import Habit
from task_overflow.tasks.models import Todo
from task_overflow.tasks.utils import todo_path_with_filters
from tests.utils import is_htmx_trigger
from tests.utils import is_login_redirect
from tests.utils import is_redirect

from .factories import CategoryFactory
from .factories import TodoFactory

if TYPE_CHECKING:
    from django.test.client import Client

    from task_overflow.characters.models import Character
    from task_overflow.tasks.models import Category


def test_category_create_loads_for_character_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character

    resp = user1_client.get(
        reverse(
            "tasks:character-category-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert character.name in resp.content.decode()


def test_category_create_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character

    resp = user2_client.get(
        reverse(
            "tasks:character-category-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_category_create_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character

    resp = client.get(
        reverse(
            "tasks:character-category-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_category_create_saves_for_character_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    data = {
        "name": "Category #1",
        "priority": 123,
    }

    resp = user1_client.post(
        reverse(
            "tasks:character-category-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())
    assert character.category_set.count() == 1
    assert character.category_set.filter(
        name="Category #1",
        priority=123,
    ).exists()


def test_category_create_does_not_save_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    data = {
        "name": "Category #1",
        "priority": 123,
    }

    resp = user2_client.post(
        reverse(
            "tasks:character-category-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Category was not created.
    assert character.category_set.count() == 0


def test_category_create_does_not_save_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    data = {
        "name": "Category #1",
        "priority": 123,
    }

    resp = client.post(
        reverse(
            "tasks:character-category-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(resp)

    # Category was not created.
    assert character.category_set.count() == 0


def test_category_detail_loads_for_logged_in_owner(
    user1_character_category: "Category",
    user1_client: "Client",
):
    category = user1_character_category
    resp = user1_client.get(
        reverse(
            "tasks:category-detail",
            kwargs={
                "pk": category.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert category.name.encode() in resp.content
    assert str(category.priority).encode() in resp.content


def test_category_detail_does_not_load_for_non_owner(
    user1_character_category: "Category",
    user2_client: "Client",
):
    category = user1_character_category
    resp = user2_client.get(
        reverse(
            "tasks:category-detail",
            kwargs={
                "pk": category.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_category_detail_does_not_load_for_anon_user(
    user1_character_category: "Category",
    client: "Client",
):
    category = user1_character_category
    resp = client.get(
        reverse(
            "tasks:category-detail",
            kwargs={
                "pk": category.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_todo_hx_create_view_loads_for_logged_in_owner_of_character(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character

    resp = user1_client.get(
        reverse(
            "tasks:character-todo-hx-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert b"Create Todo" in resp.content


def test_todo_hx_create_view_does_not_load_for_non_onwer(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character

    resp = user2_client.get(
        reverse(
            "tasks:character-todo-hx-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_todo_hx_create_view_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character

    resp = client.get(
        reverse(
            "tasks:character-todo-hx-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_todo_hx_create_view_saves_for_logged_in_owner_of_character(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    data = {
        "name": "Impossible Task",
        "experience_points": 999,
    }

    resp = user1_client.post(
        reverse(
            "tasks:character-todo-hx-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert is_htmx_trigger(resp, TODO_ADDED)
    assert b"Impossible Task" in resp.content
    assert character.todo_set.count() == 1
    assert character.todo_set.filter(
        name="Impossible Task",
        experience_points=999,
    ).exists()


def test_todo_hx_create_view_does_not_save_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    data = {
        "name": "Impossible Task",
        "experience_points": 999,
    }

    resp = user2_client.post(
        reverse(
            "tasks:character-todo-hx-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_todo_hx_create_view_does_not_save_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    data = {
        "name": "Impossible Task",
        "experience_points": 999,
    }

    resp = client.post(
        reverse(
            "tasks:character-todo-hx-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


@pytest.mark.usefixtures("_user1_character_todo_is_due_today")
def test_todo_hx_list_view_loads_for_logged_in_owner(
    user2_character_todo: "Todo",
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    other_character_todo = user2_character_todo
    character = todo.character
    resp = user1_client.get(
        reverse(
            "tasks:character-todo-hx-list",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert todo.name.encode() in resp.content

    # Other user's todos are not included.
    assert other_character_todo.name.encode() not in resp.content


def test_todo_hx_list_view_applies_named_filter(
    user1_character: "Character",
    user2_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    today = character.user.today()
    other_character = user2_character

    # Character's todos matching filter will be included.
    todo = TodoFactory(
        character=character,
        due_date=today + timedelta(days=7),
    )
    # Character's todos not matching filter will be included.
    not_in_filter_todo = TodoFactory(character=character, due_date=today)
    # Other character's todos matching filter will not be included.
    other_character_todo = TodoFactory(
        character=other_character,
        due_date=today + timedelta(days=7),
    )
    resp = user1_client.get(
        todo_path_with_filters(
            reverse(
                "tasks:character-todo-hx-list",
                kwargs={
                    "character": character.pk,
                },
            ),
            f="backlog",
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert todo.name.encode() in resp.content
    assert not_in_filter_todo.name.encode() not in resp.content
    assert other_character_todo.name.encode() not in resp.content


def test_todo_hx_list_view_applies_category_filter(
    user1_character: "Character",
    user2_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    today = character.user.today()
    other_character = user2_character

    # Character's todos matching category will be included.
    category = CategoryFactory(
        character=character,
        name="cat1",
    )
    todo = TodoFactory(
        character=character,
        due_date=today,
        category=category,
    )
    # Character's todos not matching category will be included.
    not_in_filter_todo = TodoFactory(character=character, due_date=today)
    # Other character's todos will not be included.
    other_character_todo = TodoFactory(
        character=other_character,
    )
    resp = user1_client.get(
        todo_path_with_filters(
            reverse(
                "tasks:character-todo-hx-list",
                kwargs={
                    "character": character.pk,
                },
            ),
            c="cat1",
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert todo.name.encode() in resp.content
    assert not_in_filter_todo.name.encode() not in resp.content
    assert other_character_todo.name.encode() not in resp.content


def test_todo_hx_list_view_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    resp = user2_client.get(
        reverse(
            "tasks:character-todo-hx-list",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_todo_hx_list_view_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    resp = client.get(
        reverse(
            "tasks:character-todo-hx-list",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_todo_hx_list_item_view_loads_for_logged_in_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.get(
        reverse(
            "tasks:todo-hx-list-item",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert todo.name.encode() in resp.content


def test_todo_hx_list_item_view_does_not_load_for_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    resp = user2_client.get(
        reverse(
            "tasks:todo-hx-list-item",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_todo_hx_list_item_view_does_not_load_for_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    resp = client.get(
        reverse(
            "tasks:todo-hx-list-item",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_todo_hx_notes_show_view_loads_for_logged_in_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.get(
        reverse(
            "tasks:todo-hx-notes-show",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert todo.notes.encode() in resp.content
    assert (
        reverse("tasks:todo-hx-notes-hide", kwargs={"pk": todo.pk}).encode()
        in resp.content
    )


def test_todo_hx_notes_show_view_does_not_load_for_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    resp = user2_client.get(
        reverse(
            "tasks:todo-hx-notes-show",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_todo_hx_notes_show_view_does_not_load_for_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    resp = client.get(
        reverse(
            "tasks:todo-hx-notes-show",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_todo_hx_notes_hide_view_loads_for_logged_in_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.get(
        reverse(
            "tasks:todo-hx-notes-hide",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert (
        reverse("tasks:todo-hx-notes-show", kwargs={"pk": todo.pk}).encode()
        in resp.content
    )


def test_todo_hx_notes_hide_view_does_not_load_for_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    resp = user2_client.get(
        reverse(
            "tasks:todo-hx-notes-hide",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_todo_hx_notes_hide_view_does_not_load_for_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    resp = client.get(
        reverse(
            "tasks:todo-hx-notes-hide",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_todo_hx_update_loads_for_logged_in_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.get(
        reverse(
            "tasks:todo-hx-update",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert todo.name.encode() in resp.content


def test_todo_hx_update_does_not_load_for_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    resp = user2_client.get(
        reverse(
            "tasks:todo-hx-update",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_todo_hx_update_does_not_load_for_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    resp = client.get(
        reverse(
            "tasks:todo-hx-update",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_todo_hx_update_saves_for_logged_in_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    data = {
        "name": "Updated Name",
        "experience_points": 123,
    }
    resp = user1_client.post(
        reverse(
            "tasks:todo-hx-update",
            kwargs={
                "pk": todo.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.OK
    assert b"Updated Name" in resp.content
    assert b"123" in resp.content

    todo.refresh_from_db()
    assert todo.name == "Updated Name"
    assert todo.experience_points == 123


def test_todo_hx_update_does_not_save_for_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    old_name = todo.name
    old_xp = todo.experience_points
    data = {
        "name": "Updated Name",
        "experience_points": 123,
    }
    resp = user2_client.post(
        reverse(
            "tasks:todo-hx-update",
            kwargs={
                "pk": todo.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved
    todo.refresh_from_db()
    assert todo.name == old_name
    assert todo.experience_points == old_xp


def test_todo_hx_update_does_not_save_for_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    old_name = todo.name
    old_xp = todo.experience_points
    data = {
        "name": "Updated Name",
        "experience_points": 123,
    }
    resp = client.post(
        reverse(
            "tasks:todo-hx-update",
            kwargs={
                "pk": todo.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )

    # Was not saved
    todo.refresh_from_db()
    assert todo.name == old_name
    assert todo.experience_points == old_xp


def test_todo_hx_complete_saves_for_logged_in_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.post(
        reverse(
            "tasks:todo-hx-complete",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_htmx_trigger(resp, CHARACTER_DETAILS_CHANGED)

    todo.refresh_from_db()
    assert todo.is_complete is True


def test_todo_hx_complete_does_not_respond_to_invald_http_methods(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.get(
        reverse(
            "tasks:todo-hx-complete",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.METHOD_NOT_ALLOWED

    # Was not saved
    todo.refresh_from_db()
    assert todo.is_complete is False


def test_todo_hx_complete_does_not_save_for_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    resp = user2_client.post(
        reverse(
            "tasks:todo-hx-complete",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved
    todo.refresh_from_db()
    assert todo.is_complete is False


def test_todo_hx_complete_does_not_save_for_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    resp = client.post(
        reverse(
            "tasks:todo-hx-complete",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )

    # Was not saved
    todo.refresh_from_db()
    assert todo.is_complete is False


def test_todo_hx_fail_saves_for_logged_in_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.post(
        reverse(
            "tasks:todo-hx-fail",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_htmx_trigger(resp, CHARACTER_DETAILS_CHANGED)
    assert todo.name.encode() in resp.content

    todo.refresh_from_db()
    assert todo.is_failed is True


def test_todo_hx_fail_does_not_respond_to_invalid_http_methods(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.get(
        reverse(
            "tasks:todo-hx-fail",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.METHOD_NOT_ALLOWED

    # Was not saved
    todo.refresh_from_db()
    assert todo.is_failed is False


def test_todo_hx_fail_does_not_respond_to_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    resp = user2_client.post(
        reverse(
            "tasks:todo-hx-fail",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved
    todo.refresh_from_db()
    assert todo.is_failed is False


def test_todo_hx_fail_does_not_respond_to_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    resp = client.post(
        reverse(
            "tasks:todo-hx-fail",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )

    # Was not saved
    todo.refresh_from_db()
    assert todo.is_failed is False


def test_todo_hx_delete_saves_for_logged_in_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    character = todo.character
    resp = user1_client.post(
        reverse(
            "tasks:todo-hx-delete",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert not resp.content

    assert character.todo_set.count() == 0


def test_todo_hx_delete_does_not_respond_to_invalid_http_methods(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    character = todo.character
    resp = user1_client.get(
        reverse(
            "tasks:todo-hx-delete",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.METHOD_NOT_ALLOWED

    # Was not deleted.
    assert character.todo_set.count() == 1


def test_todo_hx_delete_does_not_respond_to_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    character = todo.character
    resp = user2_client.post(
        reverse(
            "tasks:todo-hx-delete",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not deleted.
    assert character.todo_set.count() == 1


def test_todo_hx_delete_does_not_respond_to_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    character = todo.character
    resp = client.post(
        reverse(
            "tasks:todo-hx-delete",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )

    # Was not deleted.
    assert character.todo_set.count() == 1


def test_todo_hx_due_today_view_saves_for_owner(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    today = todo.character.user.today()
    resp = user1_client.post(
        reverse(
            "tasks:todo-hx-due-today",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert todo.name.encode() in resp.content

    todo.refresh_from_db()
    assert todo.due_date == today


def test_todo_hx_due_today_view_does_not_respond_to_invalid_http_methods(
    user1_character_todo: "Todo",
    user1_client: "Client",
):
    todo = user1_character_todo
    resp = user1_client.get(
        reverse(
            "tasks:todo-hx-due-today",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.METHOD_NOT_ALLOWED

    # Was not saved
    todo.refresh_from_db()
    assert todo.due_date is None


def test_todo_hx_due_today_view_does_not_respond_to_non_owner(
    user1_character_todo: "Todo",
    user2_client: "Client",
):
    todo = user1_character_todo
    resp = user2_client.post(
        reverse(
            "tasks:todo-hx-due-today",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved
    todo.refresh_from_db()
    assert todo.due_date is None


def test_todo_hx_due_today_view_does_not_respond_to_anon_user(
    user1_character_todo: "Todo",
    client: "Client",
):
    todo = user1_character_todo
    resp = client.post(
        reverse(
            "tasks:todo-hx-due-today",
            kwargs={
                "pk": todo.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )

    # Was not saved
    todo.refresh_from_db()
    assert todo.due_date is None


def test_habit_create_loads_for_logged_in_user(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    resp = user1_client.get(
        reverse(
            "tasks:character-habit-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK


def test_habit_create_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    resp = user2_client.get(
        reverse(
            "tasks:character-habit-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_habit_create_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    resp = client.get(
        reverse(
            "tasks:character-habit-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_habit_create_saves_for_logged_in_user(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    data = {"name": "New Habit", "experience_points": 123}
    resp = user1_client.post(
        reverse(
            "tasks:character-habit-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())
    assert character.habit_set.count() == 1
    assert character.habit_set.filter(
        name="New Habit",
        experience_points=123,
    ).exists()


def test_habit_create_does_not_save_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    data = {"name": "New Habit", "experience_points": 123}
    resp = user2_client.post(
        reverse(
            "tasks:character-habit-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND
    assert character.habit_set.count() == 0


def test_habit_create_does_not_save_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    data = {"name": "New Habit", "experience_points": 123}
    resp = client.post(
        reverse(
            "tasks:character-habit-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(resp)
    assert character.habit_set.count() == 0


def test_habit_update_loads_for_logged_in_owner(
    user1_character_habit: "Habit",
    user1_client: "Client",
):
    habit = user1_character_habit

    resp = user1_client.get(
        reverse(
            "tasks:habit-update",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert habit.name.encode() in resp.content


def test_habit_update_does_not_load_for_non_owner(
    user1_character_habit: "Habit",
    user2_client: "Client",
):
    habit = user1_character_habit

    resp = user2_client.get(
        reverse(
            "tasks:habit-update",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_habit_update_does_not_load_for_anon_user(
    user1_character_habit: "Habit",
    client: "Client",
):
    habit = user1_character_habit

    resp = client.get(
        reverse(
            "tasks:habit-update",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_habit_update_saves_for_logged_in_owner(
    user1_character_habit: "Habit",
    user1_client: "Client",
):
    habit = user1_character_habit
    character = habit.character
    data = {
        "name": "Updated Name",
        "experience_points": 123,
    }

    resp = user1_client.post(
        reverse(
            "tasks:habit-update",
            kwargs={
                "pk": habit.pk,
            },
        ),
        data=data,
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    habit.refresh_from_db()
    assert habit.name == "Updated Name"
    assert habit.experience_points == 123


def test_habit_update_does_not_save_for_non_owner(
    user1_character_habit: "Habit",
    user2_client: "Client",
):
    habit = user1_character_habit
    original_name = habit.name
    original_xp = habit.experience_points
    data = {
        "name": "Updated Name",
        "experience_points": 123,
    }

    resp = user2_client.post(
        reverse(
            "tasks:habit-update",
            kwargs={
                "pk": habit.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not updated
    habit.refresh_from_db()
    assert habit.name == original_name
    assert habit.experience_points == original_xp


def test_habit_update_does_not_save_for_anon_user(
    user1_character_habit: "Habit",
    client: "Client",
):
    habit = user1_character_habit
    original_name = habit.name
    original_xp = habit.experience_points
    data = {
        "name": "Updated Name",
        "experience_points": 123,
    }

    resp = client.post(
        reverse(
            "tasks:habit-update",
            kwargs={
                "pk": habit.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(resp)

    # Was not updated
    habit.refresh_from_db()
    assert habit.name == original_name
    assert habit.experience_points == original_xp


def test_habit_do_loads_for_logged_in_user(
    user1_character_habit: "Habit",
    user1_client: "Client",
):
    habit = user1_character_habit
    resp = user1_client.get(
        reverse(
            "tasks:habit-do",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert habit.name.encode() in resp.content


def test_habit_do_does_not_load_for_non_owner(
    user1_character_habit: "Habit",
    user2_client: "Client",
):
    habit = user1_character_habit
    resp = user2_client.get(
        reverse(
            "tasks:habit-do",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_habit_delete_view_loads_for_logged_in_owner(
    user1_character_habit: "Habit",
    user1_client: "Client",
):
    habit = user1_character_habit
    resp = user1_client.get(
        reverse(
            "tasks:habit-delete",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert habit.name.encode() in resp.content


def test_habit_delete_view_does_not_load_for_non_owner(
    user1_character_habit: "Habit",
    user2_client: "Client",
):
    habit = user1_character_habit
    resp = user2_client.get(
        reverse(
            "tasks:habit-delete",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_habit_delete_view_does_not_load_for_anon_user(
    user1_character_habit: "Habit",
    client: "Client",
):
    habit = user1_character_habit
    resp = client.get(
        reverse(
            "tasks:habit-delete",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_habit_delete_view_saves_for_owner(
    user1_character_habit: "Habit",
    user1_client: "Client",
):
    habit = user1_character_habit
    character = habit.character
    resp = user1_client.post(
        reverse(
            "tasks:habit-delete",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())
    assert not character.habit_set.exists()


def test_habit_delete_view_does_not_save_for_non_owner(
    user1_character_habit: "Habit",
    user2_client: "Client",
):
    habit = user1_character_habit
    character = habit.character
    resp = user2_client.post(
        reverse(
            "tasks:habit-delete",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not deleted.
    assert list(character.habit_set.all()) == [habit]


def test_habit_delete_view_does_not_save_for_anon_user(
    user1_character_habit: "Habit",
    client: "Client",
):
    habit = user1_character_habit
    character = habit.character
    resp = client.post(
        reverse(
            "tasks:habit-delete",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert is_login_redirect(resp)

    # Was not deleted.
    assert list(character.habit_set.all()) == [habit]


def test_habit_do_does_not_load_for_anon_user(
    user1_character_habit: "Habit",
    client: "Client",
):
    habit = user1_character_habit
    resp = client.get(
        reverse(
            "tasks:habit-do",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_habit_do_saves_for_logged_in_user(
    user1_character_habit: "Habit",
    user1_client: "Client",
):
    habit = user1_character_habit
    character = habit.character
    resp = user1_client.post(
        reverse(
            "tasks:habit-do",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())
    assert (
        character.pending_messages()
        .filter(text=Habit.COMPLETED_MESSAGE_TEMPLATE.format(name=habit.name))
        .exists()
    )


def test_habit_does_not_save_for_non_owner(
    user1_character_habit: "Habit",
    user2_client: "Client",
):
    habit = user1_character_habit
    character = habit.character
    resp = user2_client.post(
        reverse(
            "tasks:habit-do",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND
    # Habit was not done.
    assert not (
        character.pending_messages()
        .filter(text=Habit.COMPLETED_MESSAGE_TEMPLATE.format(name=habit.name))
        .exists()
    )


def test_habit_does_not_save_for_anon_user(
    user1_character_habit: "Habit",
    client: "Client",
):
    habit = user1_character_habit
    character = habit.character
    resp = client.post(
        reverse(
            "tasks:habit-do",
            kwargs={
                "pk": habit.pk,
            },
        ),
    )
    assert is_login_redirect(resp)
    # Habit was not done.
    assert not (
        character.pending_messages()
        .filter(text=Habit.COMPLETED_MESSAGE_TEMPLATE.format(name=habit.name))
        .exists()
    )
