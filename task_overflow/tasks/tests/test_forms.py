from typing import TYPE_CHECKING

import pytest

from task_overflow.tasks.forms import HabitForm
from task_overflow.tasks.forms import TodoForm

if TYPE_CHECKING:
    from task_overflow.tasks.models import Category


@pytest.mark.usefixtures("user2_character_category")
def test_habit_form_restricts_categories_to_user(
    user1_character_category: "Category",
):
    category = user1_character_category
    user = category.character.user

    form = HabitForm(user=user)
    assert list(form.fields["category"].queryset) == [category]


@pytest.mark.usefixtures("user2_character_category")
def test_todo_form_restricts_categories_to_user(
    user1_character_category: "Category",
):
    category = user1_character_category
    user = category.character.user

    form = TodoForm(user=user)
    assert list(form.fields["category"].queryset) == [category]
