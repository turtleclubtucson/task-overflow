from typing import TYPE_CHECKING

from task_overflow.tasks.utils import calculate_damage
from task_overflow.tasks.utils import todo_path_with_filters

from .factories import TodoFactory

if TYPE_CHECKING:
    from task_overflow.characters.models import Character


def test_calculate_damage_uses_xp_to_calculate(user1_character: "Character"):
    todo = TodoFactory(character=user1_character, experience_points=1000)
    # damage is 1000 // 100
    assert calculate_damage(todo) == 10


def test_calculate_damage_adds_bonus_when_rolled(
    mocker,
    user1_character: "Character",
):
    todo = TodoFactory(character=user1_character, experience_points=123)

    # remainder is 23, so roll must be <= 23
    mocker.patch("random.randint", return_value=23)

    # 1 HP of bonus damage.
    assert calculate_damage(todo) == 2


def test_calculate_damage_does_not_add_bonus_when_not_rolled(
    mocker,
    user1_character: "Character",
):
    todo = TodoFactory(character=user1_character, experience_points=123)

    # remainder is 23, so roll must be <= 23
    mocker.patch("random.randint", return_value=24)

    # No bonus damage.
    assert calculate_damage(todo) == 1


def test_calculate_damage_is_at_least_one_hp(user1_character: "Character"):
    todo = TodoFactory(character=user1_character, experience_points=1)
    # XP // 100 is zero, but there is still damage.
    assert calculate_damage(todo) == 1


def test_todo_path_with_filters_applies_named_filter_param():
    assert todo_path_with_filters("/foo/", f="bar") == "/foo/?f=bar"


def test_todo_path_with_filters_applies_category_param():
    assert todo_path_with_filters("/foo/", c="boo") == "/foo/?c=boo"


def test_todo_path_with_filters_applies_both_params_when_specified():
    assert todo_path_with_filters("/foo/", f="bar", c="boo") == "/foo/?f=bar&c=boo"


def test_todo_path_with_filters_does_not_apply_any_filters_when_none_specified():
    assert todo_path_with_filters("/foo/") == "/foo/?"
