from datetime import timedelta
from typing import TYPE_CHECKING

import pytest
from dateutil.relativedelta import relativedelta
from django.utils import timezone

from task_overflow.characters.tests.factories import CharacterDayFactory
from task_overflow.characters.tests.factories import CharacterFactory
from task_overflow.tasks.models import Habit
from task_overflow.tasks.models import Todo
from task_overflow.tasks.models import TodoQuerySet

from .factories import CategoryFactory
from .factories import HabitDoneFactory
from .factories import HabitFactory
from .factories import TodoFactory

if TYPE_CHECKING:
    from task_overflow.characters.models import Character
    from task_overflow.users.models import User


@pytest.mark.usefixtures("_user1_character_todo_is_due_today")
def test_todo_query_set_default_filter_is_pending_filter():
    default = Todo.objects.default()
    assert default.exists()
    assert list(default) == list(Todo.objects.pending())


@pytest.mark.usefixtures("user1_character_todo")
def test_todo_query_set_apply_filter_method_looks_up_filter_by_name():
    applied = Todo.objects.apply_filter("backlog")
    backlogged = Todo.objects.backlog()
    assert applied.exists()
    assert list(applied) == list(backlogged)


@pytest.mark.usefixtures("db")
def test_todo_query_set_apply_filter_method_only_allows_specified_filters():
    with pytest.raises(TodoQuerySet.InvalidFilterError):
        Todo.objects.apply_filter("delete")


def test_todo_query_set_backlog_filter_includes_todos_without_due_date_or_due_later(
    user1_character: "Character",
):
    character = user1_character
    cutoff = timezone.now().date() + timedelta(days=2)

    # Create out of order to check ordering.
    no_due_date = TodoFactory(character=character)
    due_on_cutoff = TodoFactory(character=character, due_date=cutoff)
    no_due_date_created_later = TodoFactory(character=character)
    due_later_than_cutoff = TodoFactory(
        character=character,
        due_date=cutoff + timedelta(days=1),
    )

    # Due before cutoff is not included.
    TodoFactory(character=character, due_date=cutoff - timedelta(days=1))
    # Already completed is not included.
    TodoFactory(character=character, is_complete=True)

    assert list(Todo.objects.backlog()) == [
        due_on_cutoff,
        due_later_than_cutoff,
        no_due_date,
        no_due_date_created_later,
    ]


def test_todo_query_set_pending_filter_includes_todos_due_tomorrow_or_earlier(
    user1_character: "Character",
):
    character = user1_character
    today = timezone.now().date()

    # Create out of order to check ordering.
    due_tomorrow = TodoFactory(character=character, due_date=today + timedelta(days=1))
    due_today = TodoFactory(character=character, due_date=today)
    due_yesterday = TodoFactory(character=character, due_date=today - timedelta(days=1))

    # Due in time but already completed is not included.
    TodoFactory(
        character=character,
        due_date=today,
        is_complete=True,
    )
    # Due after tomorrow is not included.
    TodoFactory(character=character, due_date=today + timedelta(days=2))
    # Without due date is not included.
    TodoFactory(character=character)

    assert list(Todo.objects.pending()) == [
        due_yesterday,
        due_today,
        due_tomorrow,
    ]


def test_todo_query_set_for_category_restricts_listing_to_category(
    user1_character: "Character",
):
    character = user1_character

    # Uncategorized todos are not included.
    TodoFactory(character=character)

    # Todos matching category are included.
    cat1 = CategoryFactory(
        character=character,
        name="cat1",
    )
    todo = TodoFactory(character=character, category=cat1)

    # Todos not matching category are not included.
    cat2 = CategoryFactory(
        character=character,
        name="cat2",
    )
    TodoFactory(character=character, category=cat2)

    assert list(Todo.objects.for_category("cat1")) == [todo]


def test_todo_complete_adds_experience_points_sets_todo_fields_adds_character_message(
    user1: "User",
):
    character = CharacterFactory(user=user1, experience_points=15)
    todo = TodoFactory(
        character=character,
        experience_points=25,
    )
    expected_xp = 40

    todo.complete()

    todo.refresh_from_db()
    assert todo.is_complete is True

    character.refresh_from_db()
    assert character.experience_points == expected_xp
    assert character.charactermessage_set.count() == 1
    message = character.charactermessage_set.first()
    assert message.text == Todo.COMPLETED_MESSAGE_TEMPLATE.format(name=todo.name)


def test_todo_complete_resets_due_date_if_todo_is_on_recurring_schedule(
    user1_character: "Character",
):
    character = user1_character
    old_xp = character.experience_points
    today = character.user.today()
    todo = TodoFactory(
        character=character,
        due_date=today,
        recurring_schedule=Todo.RecurringSchedule.WEEKLY,
    )
    todo.complete()

    todo.refresh_from_db()
    assert todo.is_complete is False
    assert todo.due_date == today + timedelta(weeks=1)

    # XP was still added.
    character.refresh_from_db()
    assert character.experience_points > old_xp


@pytest.mark.parametrize(
    ("schedule", "delta"),
    [
        (Todo.RecurringSchedule.WEEKLY, relativedelta(weeks=1)),
        (Todo.RecurringSchedule.MONTHLY, relativedelta(months=1)),
        (Todo.RecurringSchedule.YEARLY, relativedelta(years=1)),
    ],
)
def test_todo_caclulate_next_recurring_schedule_due_date_works_with_all_schedules(
    schedule: str,
    delta: "relativedelta",
    user1_character_todo: "Todo",
):
    todo = user1_character_todo
    today = todo.character.user.today()
    expected = today + delta
    assert todo._calculate_next_recurring_schedule_due_date(today, schedule) == expected


def test_todo_fail_deducts_hit_points_sets_todo_fields_adds_character_message(
    user1: "User",
):
    character = CharacterFactory(user=user1, hit_points=12, level=2)
    old_hp = character.hit_points
    todo = TodoFactory(
        character=character,
        experience_points=100,
    )

    todo.fail()

    todo.refresh_from_db()
    assert todo.is_failed is True

    character.refresh_from_db()
    assert character.hit_points < old_hp
    assert character.charactermessage_set.count() == 1
    message = character.charactermessage_set.first()
    assert message.text == Todo.FAILURE_MESSAGE_TEMPLATE.format(name=todo.name)


def test_habit_do_adds_xp_and_record_of_habit_done(user1: "User"):
    character = CharacterFactory(
        user=user1,
        experience_points=5,
    )
    habit = HabitFactory(
        character=character,
        experience_points=10,
    )

    habit.do()

    assert habit.habitdone_set.count() == 1

    character.refresh_from_db()
    assert character.experience_points == 15
    assert (
        character.pending_messages()
        .filter(text=Habit.COMPLETED_MESSAGE_TEMPLATE.format(name=habit.name))
        .exists()
    )


def test_habit_do_adds_streak_bonus_to_xp(user1: "User"):
    user = user1
    character = CharacterFactory(
        user=user1,
        experience_points=5,
    )
    habit = HabitFactory(
        character=character,
        experience_points=10,
    )

    today = user.today()
    yesterday = CharacterDayFactory(character=character, day=today - timedelta(days=1))
    HabitDoneFactory(habit=habit, character_day=yesterday)
    two_days_ago = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=2),
    )
    HabitDoneFactory(habit=habit, character_day=two_days_ago)

    habit.do()

    character.refresh_from_db()
    # 20 extra XP for streak bonus
    assert character.experience_points == 35


def test_habit_fail_deducts_hit_points_adds_message(user1: "User"):
    character = CharacterFactory(
        user=user1,
        hit_points=11,
        level=1,
    )
    old_hp = character.hit_points
    habit = HabitFactory(
        character=character,
        experience_points=100,
    )

    habit.fail()

    character.refresh_from_db()
    assert character.hit_points < old_hp
    assert character.pending_messages().count() == 1
    assert (
        character.pending_messages().first().text
        == Habit.FAILED_MESSAGE_TEMPLATE.format(name=habit.name)
    )


def test_habit_streak_counts_consecutive_days_starting_with_today(
    user1_character_habit: "Habit",
):
    habit = user1_character_habit
    character = habit.character
    user = character.user
    day = user.today()

    today = CharacterDayFactory(
        character=character,
        day=day,
    )
    HabitDoneFactory(habit=habit, character_day=today)
    yesterday = CharacterDayFactory(
        character=character,
        day=day - timedelta(days=1),
    )
    HabitDoneFactory(habit=habit, character_day=yesterday)

    assert habit.streak() == 2


def test_habit_streak_counts_consecutive_days_starting_with_yesterday_if_today_not_done(
    user1_character_habit: "Habit",
):
    habit = user1_character_habit
    character = habit.character
    user = character.user
    today = user.today()

    yesterday = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=1),
    )
    HabitDoneFactory(habit=habit, character_day=yesterday)
    two_days_ago = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=2),
    )
    HabitDoneFactory(habit=habit, character_day=two_days_ago)
    three_days_ago = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=3),
    )
    HabitDoneFactory(habit=habit, character_day=three_days_ago)

    assert habit.streak() == 3


def test_habit_streak_is_broken_if_day_was_missed(
    user1_character_habit: "Habit",
):
    habit = user1_character_habit
    character = habit.character
    user = character.user
    today = user.today()

    yesterday = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=1),
    )
    HabitDoneFactory(habit=habit, character_day=yesterday)

    three_days_ago = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=3),
    )
    HabitDoneFactory(habit=habit, character_day=three_days_ago)

    four_days_ago = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=4),
    )
    HabitDoneFactory(habit=habit, character_day=four_days_ago)

    assert habit.streak() == 1


def test_habit_streak_is_zero_when_never_done_before(user1_character_habit: "Habit"):
    habit = user1_character_habit
    assert habit.streak() == 0


def test_habit_streak_is_zero_if_yesterday_was_not_done(
    user1_character_habit: "Habit",
):
    habit = user1_character_habit
    character = habit.character
    user = character.user
    today = user.today()

    two_days_ago = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=2),
    )
    HabitDoneFactory(habit=habit, character_day=two_days_ago)

    three_days_ago = CharacterDayFactory(
        character=character,
        day=today - timedelta(days=3),
    )
    HabitDoneFactory(habit=habit, character_day=three_days_ago)

    assert habit.streak() == 0


def test_habit_done_assigns_itself_to_current_character_day_when_created(
    user1_character_habit: "Habit",
):
    habit = user1_character_habit
    character_day = habit.character.character_day()

    habit_done = HabitDoneFactory(habit=habit)
    assert habit_done.character_day == character_day
