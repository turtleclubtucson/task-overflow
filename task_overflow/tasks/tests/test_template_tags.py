from datetime import timedelta
from typing import TYPE_CHECKING

from django.urls import reverse
from django.utils import timezone

from task_overflow.tasks.templatetags.filter_style import ACTIVE_FILTER_STYLE
from task_overflow.tasks.templatetags.filter_style import INACTIVE_FILTER_STYLE
from task_overflow.tasks.templatetags.filter_style import filter_style
from task_overflow.tasks.templatetags.todo_style import BASIC_TODO_STYLE
from task_overflow.tasks.templatetags.todo_style import FUTURE_TODO_STYLE
from task_overflow.tasks.templatetags.todo_style import IMMEDIATE_TODO_STYLE
from task_overflow.tasks.templatetags.todo_style import todo_style
from task_overflow.tasks.templatetags.url_with_todo_filters import url_with_todo_filters
from task_overflow.tasks.tests.factories import TodoFactory
from task_overflow.tasks.utils import todo_path_with_filters

if TYPE_CHECKING:
    from task_overflow.characters.models import Character


TASK_PATH = "tasks:character-todo-hx-list"


def test_filter_style_when_named_filter_matches_request(rf):
    req = rf.get("/hello/?f=foo")
    assert (
        filter_style(
            {
                "request": req,
            },
            f="foo",
        )
        == ACTIVE_FILTER_STYLE
    )


def test_filter_style_when_category_matches_request(rf):
    req = rf.get("/hello/?c=foo")
    assert (
        filter_style(
            {
                "request": req,
            },
            c="foo",
        )
        == ACTIVE_FILTER_STYLE
    )


def test_filter_style_when_no_params_set(rf):
    req = rf.get("/hello/")
    assert (
        filter_style(
            {
                "request": req,
            },
            c="foo",
        )
        == INACTIVE_FILTER_STYLE
    )


def test_filter_style_when_named_filter_does_not_match_request(rf):
    req = rf.get("/hello/?f=bar")
    assert (
        filter_style(
            {
                "request": req,
            },
            f="foo",
        )
        == INACTIVE_FILTER_STYLE
    )


def test_filter_style_when_category_does_not_match_request(rf):
    req = rf.get("/hello/?c=bar")
    assert (
        filter_style(
            {
                "request": req,
            },
            c="foo",
        )
        == INACTIVE_FILTER_STYLE
    )


def test_filter_style_when_named_fitler_and_category_does_not_match_request(rf):
    req = rf.get("/hello/?f=foo&c=bar")
    assert (
        filter_style(
            {
                "request": req,
            },
            f="bar",
            c="foo",
        )
        == INACTIVE_FILTER_STYLE
    )


def test_todo_style_when_todo_has_no_due_date(
    user1_character: "Character",
):
    character = user1_character

    todo = TodoFactory(
        character=character,
        due_date=None,
    )
    assert todo_style(todo) == BASIC_TODO_STYLE


def test_todo_style_when_todo_has_due_date_in_future(
    user1_character: "Character",
):
    character = user1_character

    todo = TodoFactory(
        character=character,
        due_date=timezone.now().date() + timedelta(days=1),
    )
    assert todo_style(todo) == FUTURE_TODO_STYLE


def test_todo_style_when_todo_is_due_today(
    user1_character: "Character",
):
    character = user1_character

    todo = TodoFactory(
        character=character,
        due_date=timezone.now().date(),
    )
    assert todo_style(todo) == IMMEDIATE_TODO_STYLE


def test_url_with_todo_filters_no_filters_and_previously_unfiltered(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
    )
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
    )


def test_url_with_todo_filters_named_filter_and_previously_unfiltered(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
        f="foo",
    )
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
        f="foo",
    )


def test_url_with_todo_filters_named_filter_replaces_previous_named_filter(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/?f=bar")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
        f="foo",
    )
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
        f="foo",
    )


def test_url_with_todo_filters_named_filter_does_not_impact_category_filter(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/?c=bar")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
        f="foo",
    )
    # Category filter is not changed.
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
        f="foo",
        c="bar",
    )


def test_url_with_todo_filters_named_filter_matches_previous_named_filter(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/?f=foo")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
        f="foo",
    )
    # Named filter is untoggled when it maches existing filter.
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
    )


def test_url_with_todo_filters_unset_named_filter_uses_previously_named_filter(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/?f=foo")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
    )
    # Existing filter is retained.
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
        f="foo",
    )


def test_url_with_todo_filters_category_filter_and_previously_unfiltered(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
        c="foo",
    )
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
        c="foo",
    )


def test_url_with_todo_filters_category_filter_replaces_previous_category_filter(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/?c=bar")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
        c="foo",
    )
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
        c="foo",
    )


def test_url_with_todo_filters_category_filter_does_not_impact_named_filter(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/?f=bar")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
        c="foo",
    )
    # Category filter is not changed.
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
        f="bar",
        c="foo",
    )


def test_url_with_todo_filters_category_filter_matches_previous_category_filter(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/?c=foo")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
        c="foo",
    )
    # Category filter is untoggled when it maches existing filter.
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
    )


def test_url_with_todo_filters_unset_category_filter_uses_previous_category_filter(
    rf,
    user1_character: "Character",
):
    character = user1_character
    req = rf.get("/hello/?c=foo")
    result = url_with_todo_filters(
        {
            "request": req,
        },
        TASK_PATH,
        character=character.pk,
    )
    # Existing filter is retained.
    assert result == todo_path_with_filters(
        reverse(
            TASK_PATH,
            kwargs={
                "character": character.pk,
            },
        ),
        c="foo",
    )
