import urllib
from typing import TYPE_CHECKING

from task_overflow.utils import d100

if TYPE_CHECKING:
    from .models import Habit
    from .models import Todo


def calculate_damage(task: "Todo | Habit") -> int:
    if task.experience_points <= 100:
        return 1

    base_damage = task.experience_points // 100
    bonus_chance = task.experience_points % 100
    if bonus_chance and d100() <= bonus_chance:
        return base_damage + 1
    return base_damage


def todo_path_with_filters(
    path,
    f: str | None = None,
    c: str | None = None,
) -> str:
    kwargs = {}
    if f is not None:
        kwargs["f"] = f
    if c is not None:
        kwargs["c"] = c
    params = urllib.parse.urlencode(kwargs)
    return f"{path}?{params}"
