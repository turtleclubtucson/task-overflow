from django import template
from django.urls import reverse

from task_overflow.tasks.utils import todo_path_with_filters

register = template.Library()

UNSET = "UNSET"


@register.simple_tag(takes_context=True)
def url_with_todo_filters(
    context,
    path_name,
    *,
    f: str | None = "UNSET",
    c: str | None = "UNSET",
    **kwargs,
) -> str:
    request = context["request"]
    path = reverse(path_name, kwargs=kwargs)

    request_f = request.GET.get("f")
    if f == UNSET:
        f = request_f
    elif f == request_f:
        f = None

    request_c = request.GET.get("c")
    if c == UNSET:
        c = request_c
    elif c == request_c:
        c = None

    return todo_path_with_filters(path, f=f, c=c)
