from typing import TYPE_CHECKING

from django import template
from django.utils import timezone

register = template.Library()

if TYPE_CHECKING:
    from task_overflow.tasks.models import Todo


BASIC_TODO_STYLE = "basic-todo"
FUTURE_TODO_STYLE = "future-todo"
IMMEDIATE_TODO_STYLE = "immediate-todo"
FAILED_TODO_STYLE = "failed-todo"


@register.filter(name="todo_style")
def todo_style(todo: "Todo") -> str:
    if todo.is_failed:
        return FAILED_TODO_STYLE
    if todo.due_date:
        if todo.due_date <= timezone.now().date():
            return IMMEDIATE_TODO_STYLE
        return FUTURE_TODO_STYLE
    return BASIC_TODO_STYLE
