from django import template

register = template.Library()

ACTIVE_FILTER_STYLE = "active-filter"
INACTIVE_FILTER_STYLE = "inactive-filter"


@register.simple_tag(takes_context=True)
def filter_style(
    context,
    f: str | None = None,
    c: str | None = None,
) -> str:
    request = context["request"]
    if (f and f == request.GET.get("f")) or (c and c == request.GET.get("c")):
        return ACTIVE_FILTER_STYLE
    return INACTIVE_FILTER_STYLE
