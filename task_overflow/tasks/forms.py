from typing import TYPE_CHECKING

from django import forms

from .models import Habit
from .models import Todo

if TYPE_CHECKING:
    from task_overflow.users.models import User


class DatePickerInput(forms.DateInput):
    input_type = "date"


class HabitForm(forms.ModelForm):
    def __init__(self, user: "User", *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.fields["category"].queryset = user.categories()

    class Meta:
        model = Habit
        fields = [
            "category",
            "name",
            "experience_points",
        ]


class TodoForm(forms.ModelForm):
    def __init__(self, user: "User", *args, **kwargs) -> None:
        super().__init__(*args, **kwargs)
        self.fields["category"].queryset = user.categories()

    class Meta:
        model = Todo
        fields = [
            "category",
            "name",
            "experience_points",
            "due_date",
            "recurring_schedule",
            "notes",
        ]
        widgets = {
            "due_date": DatePickerInput(),
        }
