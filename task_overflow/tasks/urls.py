from django.urls import path

from .views import category_create_view
from .views import category_detail_view
from .views import habit_create_view
from .views import habit_delete_view
from .views import habit_do_view
from .views import habit_update_view
from .views import todo_hx_complete_view
from .views import todo_hx_create_view
from .views import todo_hx_delete_view
from .views import todo_hx_due_today_view
from .views import todo_hx_fail_view
from .views import todo_hx_list_item_view
from .views import todo_hx_list_view
from .views import todo_hx_notes_hide_view
from .views import todo_hx_notes_show_view
from .views import todo_hx_update_view

app_name = "tasks"
urlpatterns = [
    path(
        "<int:character>/categories/~create/",
        view=category_create_view,
        name="character-category-create",
    ),
    path(
        "categories/<int:pk>/",
        view=category_detail_view,
        name="category-detail",
    ),
    path(
        "<int:character>/habits/~create/",
        view=habit_create_view,
        name="character-habit-create",
    ),
    path("habits/<int:pk>/~update/", view=habit_update_view, name="habit-update"),
    path("habits/<int:pk>/~delete/", view=habit_delete_view, name="habit-delete"),
    path("habits/<int:pk>/~do/", view=habit_do_view, name="habit-do"),
    path(
        "hx/<int:character>/todos/~create/",
        view=todo_hx_create_view,
        name="character-todo-hx-create",
    ),
    path(
        "hx/<int:character>/todos/",
        view=todo_hx_list_view,
        name="character-todo-hx-list",
    ),
    path(
        "hx/todos/<int:pk>/",
        view=todo_hx_list_item_view,
        name="todo-hx-list-item",
    ),
    path(
        "hx/todos/<int:pk>/notes/~hide/",
        view=todo_hx_notes_hide_view,
        name="todo-hx-notes-hide",
    ),
    path(
        "hx/todos/<int:pk>/notes/~show/",
        view=todo_hx_notes_show_view,
        name="todo-hx-notes-show",
    ),
    path(
        "hx/todos/<int:pk>/~update/",
        view=todo_hx_update_view,
        name="todo-hx-update",
    ),
    path(
        "hx/todos/<int:pk>/~complete/",
        view=todo_hx_complete_view,
        name="todo-hx-complete",
    ),
    path(
        "hx/todos/<int:pk>/~fail/",
        view=todo_hx_fail_view,
        name="todo-hx-fail",
    ),
    path(
        "hx/todos/<int:pk>/~due-today/",
        view=todo_hx_due_today_view,
        name="todo-hx-due-today",
    ),
    path(
        "hx/todos/<int:pk>/~delete/",
        view=todo_hx_delete_view,
        name="todo-hx-delete",
    ),
]
