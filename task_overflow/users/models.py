from datetime import datetime
from typing import TYPE_CHECKING
from typing import ClassVar

from django.contrib.auth.models import AbstractUser
from django.db.models import CharField
from django.db.models import EmailField
from django.db.models.query import QuerySet
from django.urls import reverse
from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from timezone_field import TimeZoneField

from task_overflow.characters.models import CharacterDay
from task_overflow.characters.models import CharacterLevelUp
from task_overflow.encounters.models import Encounter
from task_overflow.items.models import CharacterItem
from task_overflow.tasks.models import Category
from task_overflow.tasks.models import Habit
from task_overflow.tasks.models import Todo
from task_overflow.trackers.models import Adjustment
from task_overflow.trackers.models import Tracker

from .managers import UserManager

if TYPE_CHECKING:
    from datetime import date


class User(AbstractUser):
    """
    Default custom user model for task overflow.
    If adding fields that need to be filled at user signup,
    check forms.SignupForm and forms.SocialSignupForms accordingly.
    """

    # First and last name do not cover name patterns around the globe
    name = CharField(_("Name of User"), blank=True, max_length=255)
    first_name = None  # type: ignore[assignment]
    last_name = None  # type: ignore[assignment]
    email = EmailField(_("email address"), unique=True)
    timezone = TimeZoneField(default="America/Phoenix")
    username = None  # type: ignore[assignment]

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects: ClassVar[UserManager] = UserManager()

    def categories(self) -> QuerySet["Category"]:
        return Category.objects.filter(character__in=self.character_set.all())

    def character_days(self) -> QuerySet["CharacterDay"]:
        return CharacterDay.objects.filter(character__in=self.character_set.all())

    def character_items(self) -> QuerySet["CharacterItem"]:
        return CharacterItem.objects.filter(character__in=self.character_set.all())

    def character_level_ups(self) -> QuerySet["CharacterLevelUp"]:
        return CharacterLevelUp.objects.filter(character__in=self.character_set.all())

    def encounters(self) -> QuerySet["Encounter"]:
        return Encounter.objects.filter(character_day__in=self.character_days())

    def habits(self) -> QuerySet["Habit"]:
        return Habit.objects.filter(character__in=self.character_set.all())

    def todos(self) -> QuerySet["Todo"]:
        return Todo.objects.filter(character__in=self.character_set.all())

    def incomplete_todos(self) -> QuerySet["Todo"]:
        return self.todos().filter(
            is_complete=False,
        )

    def trackers(self) -> QuerySet["Tracker"]:
        return Tracker.objects.filter(character__in=self.character_set.all())

    def tracker_adjustments(self) -> QuerySet["Adjustment"]:
        return Adjustment.objects.filter(tracker__in=self.trackers())

    def get_absolute_url(self) -> str:
        """Get URL for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"pk": self.id})

    def local_time(self, dt: "datetime") -> "datetime":
        return dt.astimezone(self.timezone)

    def now(self) -> "datetime":
        return self.local_time(timezone.now())

    def today(self) -> "date":
        return self.now().date()

    def convert_day_to_local_time(self, day: "date") -> "datetime":
        return self.local_time(datetime.combine(day, datetime.min.time()))
