from django.urls import resolve
from django.urls import reverse

from task_overflow.users.models import User


def test_detail(user1: User):
    assert reverse("users:detail", kwargs={"pk": user1.pk}) == f"/users/{user1.pk}/"
    assert resolve(f"/users/{user1.pk}/").view_name == "users:detail"


def test_update():
    assert reverse("users:update") == "/users/~update/"
    assert resolve("/users/~update/").view_name == "users:update"


def test_redirect():
    assert reverse("users:redirect") == "/users/~redirect/"
    assert resolve("/users/~redirect/").view_name == "users:redirect"
