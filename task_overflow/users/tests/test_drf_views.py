import pytest
from rest_framework.test import APIRequestFactory

from task_overflow.users.api.views import UserViewSet
from task_overflow.users.models import User


class TestUserViewSet:
    @pytest.fixture()
    def api_rf(self) -> APIRequestFactory:
        return APIRequestFactory()

    def test_get_queryset(self, user1: User, api_rf: APIRequestFactory):
        view = UserViewSet()
        request = api_rf.get("/fake-url/")
        request.user = user1

        view.request = request

        assert user1 in view.get_queryset()

    def test_me(self, user1: User, api_rf: APIRequestFactory):
        view = UserViewSet()
        request = api_rf.get("/fake-url/")
        request.user = user1

        view.request = request

        response = view.me(request)  # type: ignore[call-arg, arg-type, misc]

        assert response.data == {
            "url": f"http://testserver/api/users/{user1.pk}/",
            "name": user1.name,
        }
