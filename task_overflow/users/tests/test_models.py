from datetime import datetime
from typing import TYPE_CHECKING

import pytest
from django.utils import timezone

if TYPE_CHECKING:
    from task_overflow.characters.models import CharacterDay
    from task_overflow.characters.models import CharacterLevelUp
    from task_overflow.encounters.models import Encounter
    from task_overflow.items.models import CharacterItem
    from task_overflow.tasks.models import Category
    from task_overflow.tasks.models import Habit
    from task_overflow.tasks.models import Todo
    from task_overflow.trackers.models import Adjustment
    from task_overflow.trackers.models import Tracker
    from task_overflow.users.models import User


@pytest.mark.usefixtures("user2_character_category")
def test_user_character_category_includes_only_categories_owned_by_users_characters(
    user1_character_category: "Category",
):
    category = user1_character_category
    user = category.character.user

    assert list(user.categories()) == [category]


@pytest.mark.usefixtures("user2_character_day")
def test_user_character_days_includes_only_character_days_owned_by_users_characters(
    user1_character_day: "CharacterDay",
):
    character_day = user1_character_day
    user = character_day.character.user

    assert list(user.character_days()) == [character_day]


@pytest.mark.usefixtures("user2_character_item")
def test_user_character_items_includes_only_character_items_owned_by_users_characters(
    user1_character_item: "CharacterItem",
):
    character_item = user1_character_item
    user = character_item.character.user

    assert list(user.character_items()) == [character_item]


@pytest.mark.usefixtures("user2_character_level_up")
def test_user_character_level_ups_includes_only_level_ups_owned_by_users_characters(
    user1_character_level_up: "CharacterLevelUp",
):
    level_up = user1_character_level_up
    user = level_up.character.user

    assert list(user.character_level_ups()) == [level_up]


@pytest.mark.usefixtures("user2_character_day_encounter")
def test_user_encounters_includes_only_character_days_owned_by_users_characters(
    user1_character_day_encounter: "Encounter",
):
    encounter = user1_character_day_encounter
    user = encounter.character_day.character.user

    assert list(user.encounters()) == [encounter]


@pytest.mark.usefixtures("user2_character_habit")
def test_user_habits_includes_only_habits_owned_by_users_characters(
    user1_character_habit: "Habit",
):
    habit = user1_character_habit
    user = habit.character.user

    assert list(user.habits()) == [habit]


@pytest.mark.usefixtures("user2_character_todo")
def test_user_todos_includes_only_todos_owned_by_users_characters(
    user1_character_todo: "Todo",
):
    todo = user1_character_todo
    user = todo.character.user

    assert list(user.todos()) == [
        todo,
    ]


@pytest.mark.usefixtures("user2_character_todo")
def test_user_incomplete_todos_includes_only_incomplete_todos_owned_by_users_characters(
    user1_character_todo: "Todo",
):
    incomplete = user1_character_todo
    character = incomplete.character
    user = character.user
    # Completed todos are not included
    character.todo_set.create(
        name="Finished",
        experience_points=5,
        is_complete=True,
    )
    assert list(user.incomplete_todos()) == [
        incomplete,
    ]


@pytest.mark.usefixtures("user2_character_tracker")
def test_user_trackers_includes_only_trackers_owned_by_users_characters(
    user1_character_tracker: "Tracker",
):
    tracker = user1_character_tracker
    user = tracker.character.user

    assert list(user.trackers()) == [tracker]


@pytest.mark.usefixtures("user2_character_tracker_adjustment")
def test_user_tracker_adjustments__includes_only_adjustments_owned_by_users_characters(
    user1_character_tracker_adjustment: "Adjustment",
):
    adjustment = user1_character_tracker_adjustment
    user = adjustment.tracker.character.user

    assert list(user.tracker_adjustments()) == [adjustment]


def test_user_get_absolute_url(user1: "User"):
    assert user1.get_absolute_url() == f"/users/{user1.pk}/"


def test_user_local_time_returns_datetime_in_users_timezone(user1: "User"):
    user = user1
    dt = datetime(2020, 1, 1)
    assert user.local_time(dt) == dt.astimezone(user.timezone)


@pytest.mark.usefixtures("freezer")
def test_user_now_gets_now_in_users_timezone(user1: "User"):
    user = user1
    assert user.now() == timezone.now().astimezone(user.timezone)


def test_user_today_gets_today_in_users_timezone(user1: "User"):
    user = user1
    assert user.today() == timezone.now().astimezone(user.timezone).date()


def test_user_convert_day_to_local_time(user1: "User"):
    user = user1
    day = timezone.now().date()
    assert user.convert_day_to_local_time(day) == datetime.combine(
        day,
        datetime.min.time(),
    ).astimezone(user.timezone)
