import random

from django.http import HttpResponse


def empty_response() -> HttpResponse:
    return HttpResponse()


def roll_dice(num_sides: int, num_dice: int) -> int:
    return sum([random.randint(1, num_sides) for _ in range(num_dice)])


def d3(num: int = 1) -> int:
    return roll_dice(
        num_sides=3,
        num_dice=num,
    )


def d6(num: int = 1) -> int:
    return roll_dice(
        num_sides=6,
        num_dice=num,
    )


def d10(num: int = 1) -> int:
    return roll_dice(
        num_sides=10,
        num_dice=num,
    )


def d20(num: int = 1) -> int:
    return roll_dice(
        num_sides=20,
        num_dice=num,
    )


def d100(num: int = 1) -> int:
    return roll_dice(num_sides=100, num_dice=num)
