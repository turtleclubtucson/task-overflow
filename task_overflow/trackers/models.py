from django.db import models
from django.dispatch import receiver
from model_utils.models import TimeStampedModel


class Tracker(TimeStampedModel):
    character = models.ForeignKey(
        "characters.Character",
        on_delete=models.CASCADE,
    )
    name = models.CharField(max_length=127)
    balance = models.IntegerField(default=0)
    daily_adjustment = models.IntegerField(default=0)


class Adjustment(TimeStampedModel):
    tracker = models.ForeignKey(
        "Tracker",
        on_delete=models.CASCADE,
    )
    value = models.IntegerField()


@receiver(models.signals.pre_save, sender=Adjustment)
def apply_adjustment_to_tracker_balance_when_created(sender, instance, **kwargs):
    if not instance.pk:
        instance.tracker.balance += instance.value
        instance.tracker.save()


@receiver(models.signals.pre_delete, sender=Adjustment)
def revoke_adjustment_from_tracker_balance_when_deleted(sender, instance, **kwargs):
    instance.tracker.balance -= instance.value
    instance.tracker.save()
