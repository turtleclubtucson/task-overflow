from django import forms

from .models import Adjustment


class NegativeAdjustmentForm(forms.ModelForm):
    def clean_value(self):
        return self.cleaned_data["value"] * -1

    class Meta:
        model = Adjustment
        fields = ["value"]
