from django.urls import path

from .views import adjustment_delete_view
from .views import adjustment_list_view
from .views import negative_adjustment_create_view
from .views import tracker_create_view

app_name = "trackers"
urlpatterns = [
    path(
        "<int:character>/~create/",
        view=tracker_create_view,
        name="character-tracker-create",
    ),
    path(
        "trackers/<int:tracker>/adjustments/negative/~create/",
        view=negative_adjustment_create_view,
        name="tracker-negative-adjustment-create",
    ),
    path(
        "trackers/<int:tracker>/adjustments/",
        view=adjustment_list_view,
        name="tracker-adjustment-list",
    ),
    path(
        "adjustments/<int:pk>/~delete/",
        view=adjustment_delete_view,
        name="adjustment-delete",
    ),
]
