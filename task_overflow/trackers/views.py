from typing import TYPE_CHECKING

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404
from django.views.generic.edit import CreateView
from django.views.generic.edit import DeleteView
from django.views.generic.list import ListView

from task_overflow.mixins import CharacterRelatedDataViewMixin
from task_overflow.mixins import DefaultFormViewMixin

from .forms import NegativeAdjustmentForm
from .models import Adjustment
from .models import Tracker

if TYPE_CHECKING:
    from task_overflow.characters.models import Character


class TrackerCreateView(
    LoginRequiredMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    CreateView,
):
    model = Tracker
    fields = ["name", "balance", "daily_adjustment"]
    form_header = "Create Tracker"
    form_instance_assignments = {"character": "character"}
    redirect_to_character_on_success = True

    @property
    def form_message(self) -> str:
        return self.character().name


tracker_create_view = TrackerCreateView.as_view()


class NegativeAdjustmentCreateView(
    LoginRequiredMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    CreateView,
):
    model = Adjustment
    form_class = NegativeAdjustmentForm
    form_header = "Create Negative Adjustment"
    redirect_to_character_on_success = True
    form_instance_assignments = {"tracker": "tracker"}

    @property
    def form_message(self) -> str:
        return f"Create for {self.tracker().name}?"

    def tracker(self) -> "Tracker":
        return get_object_or_404(
            self.request.user.trackers(),
            pk=self.kwargs["tracker"],
        )

    def character(self) -> "Character":
        return self.tracker().character

    def get_context_data(self, *args, **kwargs) -> dict:
        context = super().get_context_data(*args, **kwargs)
        context["tracker"] = self.tracker()
        return context


negative_adjustment_create_view = NegativeAdjustmentCreateView.as_view()


class AdjustmentListView(LoginRequiredMixin, ListView):
    model = Adjustment

    def tracker(self) -> "Tracker":
        return get_object_or_404(
            self.request.user.trackers(),
            pk=self.kwargs["tracker"],
        )

    def get_queryset(self) -> QuerySet["Adjustment"]:
        return Adjustment.objects.filter(tracker=self.tracker()).order_by("-created")

    def get_context_data(self, *args, **kwargs) -> dict:
        context = super().get_context_data(*args, **kwargs)
        context["tracker"] = self.tracker()
        return context


adjustment_list_view = AdjustmentListView.as_view()


class AdjustmentDeleteView(
    LoginRequiredMixin,
    CharacterRelatedDataViewMixin,
    DeleteView,
):
    model = Adjustment
    redirect_to_character_on_success = True

    def character(self) -> "Character":
        return self.object.tracker.character

    def get_queryset(self) -> QuerySet["Adjustment"]:
        return self.request.user.tracker_adjustments()


adjustment_delete_view = AdjustmentDeleteView.as_view()
