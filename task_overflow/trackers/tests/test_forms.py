from task_overflow.trackers.forms import NegativeAdjustmentForm


def test_negative_adjustment_form_sets_provided_value_to_negative():
    form = NegativeAdjustmentForm(
        data={
            "value": 123,
        },
    )
    assert form.is_valid()
    assert form.cleaned_data["value"] == -123
