from http import HTTPStatus
from typing import TYPE_CHECKING

from django.urls import reverse

from tests.utils import is_login_redirect
from tests.utils import is_redirect

if TYPE_CHECKING:
    from django.test.client import Client

    from task_overflow.characters.models import Character
    from task_overflow.trackers.models import Adjustment
    from task_overflow.trackers.models import Tracker


def test_tracker_create_view_loads_for_character_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    resp = user1_client.get(
        reverse(
            "trackers:character-tracker-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert character.name.encode() in resp.content


def test_tracker_create_view_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    resp = user2_client.get(
        reverse(
            "trackers:character-tracker-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_tracker_create_view_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    resp = client.get(
        reverse(
            "trackers:character-tracker-create",
            kwargs={
                "character": character.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_tracker_create_view_saves_for_character_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    data = {
        "name": "Trackit",
        "balance": 123,
        "daily_adjustment": 456,
    }
    resp = user1_client.post(
        reverse(
            "trackers:character-tracker-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    assert character.tracker_set.count() == 1
    assert character.tracker_set.filter(**data).exists()


def test_tracker_create_view_does_not_save_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    data = {
        "name": "Trackit",
        "balance": 123,
        "daily_adjustment": 456,
    }
    resp = user2_client.post(
        reverse(
            "trackers:character-tracker-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved.
    assert character.tracker_set.count() == 0


def test_tracker_create_view_does_not_save_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    data = {
        "name": "Trackit",
        "balance": 123,
        "daily_adjustment": 456,
    }
    resp = client.post(
        reverse(
            "trackers:character-tracker-create",
            kwargs={
                "character": character.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(resp)

    # Was not saved.
    assert character.tracker_set.count() == 0


def test_negative_adjustment_create_view_loads_for_logged_in_owner(
    user1_character_tracker: "Tracker",
    user1_client: "Client",
):
    tracker = user1_character_tracker
    resp = user1_client.get(
        reverse(
            "trackers:tracker-negative-adjustment-create",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert tracker.name.encode() in resp.content


def test_negative_adjustment_create_view_does_not_load_for_non_owner(
    user1_character_tracker: "Tracker",
    user2_client: "Client",
):
    tracker = user1_character_tracker
    resp = user2_client.get(
        reverse(
            "trackers:tracker-negative-adjustment-create",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_negative_adjustment_create_view_does_not_load_for_anon_user(
    user1_character_tracker: "Tracker",
    client: "Client",
):
    tracker = user1_character_tracker
    resp = client.get(
        reverse(
            "trackers:tracker-negative-adjustment-create",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_negative_adjustment_create_view_saves_for_logged_in_owner(
    user1_character_tracker: "Tracker",
    user1_client: "Client",
):
    tracker = user1_character_tracker
    character = tracker.character
    data = {"value": 111}

    resp = user1_client.post(
        reverse(
            "trackers:tracker-negative-adjustment-create",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
        data=data,
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    # Adjustment was saved with negative value.
    assert tracker.adjustment_set.count() == 1
    assert tracker.adjustment_set.filter(value=-111).exists()


def test_negative_adjustment_create_view_does_not_save_for_non_owner(
    user1_character_tracker: "Tracker",
    user2_client: "Client",
):
    tracker = user1_character_tracker
    data = {"value": 111}

    resp = user2_client.post(
        reverse(
            "trackers:tracker-negative-adjustment-create",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved.
    assert tracker.adjustment_set.count() == 0


def test_negative_adjustment_create_view_does_not_save_for_anon_user(
    user1_character_tracker: "Tracker",
    client: "Client",
):
    tracker = user1_character_tracker
    data = {"value": 111}

    resp = client.post(
        reverse(
            "trackers:tracker-negative-adjustment-create",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(resp)

    # Was not saved.
    assert tracker.adjustment_set.count() == 0


def test_tracker_adjustment_list_loads_for_owner(
    user1_character_tracker_adjustment: "Adjustment",
    user1_client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    tracker = adjustment.tracker

    resp = user1_client.get(
        reverse(
            "trackers:tracker-adjustment-list",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert str(adjustment.value).encode() in resp.content


def test_tracker_adjustment_list_does_not_load_for_non_owner(
    user1_character_tracker_adjustment: "Adjustment",
    user2_client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    tracker = adjustment.tracker

    resp = user2_client.get(
        reverse(
            "trackers:tracker-adjustment-list",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_tracker_adjustment_list_does_not_load_for_anon_user(
    user1_character_tracker_adjustment: "Adjustment",
    client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    tracker = adjustment.tracker

    resp = client.get(
        reverse(
            "trackers:tracker-adjustment-list",
            kwargs={
                "tracker": tracker.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_adjustment_delete_loads_for_logged_in_owner(
    user1_character_tracker_adjustment: "Adjustment",
    user1_client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    tracker = adjustment.tracker
    resp = user1_client.get(
        reverse(
            "trackers:adjustment-delete",
            kwargs={
                "pk": adjustment.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert tracker.name.encode() in resp.content


def test_adjustment_delete_does_not_load_for_non_owner(
    user1_character_tracker_adjustment: "Adjustment",
    user2_client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    resp = user2_client.get(
        reverse(
            "trackers:adjustment-delete",
            kwargs={
                "pk": adjustment.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_adjustment_delete_does_not_load_for_anon_user(
    user1_character_tracker_adjustment: "Adjustment",
    client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    resp = client.get(
        reverse(
            "trackers:adjustment-delete",
            kwargs={
                "pk": adjustment.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_adjustment_delete_saves_for_logged_in_owner(
    user1_character_tracker_adjustment: "Adjustment",
    user1_client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    tracker = adjustment.tracker
    character = tracker.character
    resp = user1_client.post(
        reverse(
            "trackers:adjustment-delete",
            kwargs={
                "pk": adjustment.pk,
            },
        ),
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    assert tracker.adjustment_set.count() == 0


def test_adjustment_delete_does_not_save_for_non_owner(
    user1_character_tracker_adjustment: "Adjustment",
    user2_client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    tracker = adjustment.tracker
    resp = user2_client.post(
        reverse(
            "trackers:adjustment-delete",
            kwargs={
                "pk": adjustment.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved.
    assert tracker.adjustment_set.count() == 1
    assert tracker.adjustment_set.first() == adjustment


def test_adjustment_delete_does_not_save_for_anon_user(
    user1_character_tracker_adjustment: "Adjustment",
    client: "Client",
):
    adjustment = user1_character_tracker_adjustment
    tracker = adjustment.tracker
    resp = client.post(
        reverse(
            "trackers:adjustment-delete",
            kwargs={
                "pk": adjustment.pk,
            },
        ),
    )
    assert is_login_redirect(resp)

    # Was not saved.
    assert tracker.adjustment_set.count() == 1
    assert tracker.adjustment_set.first() == adjustment
