from factory import Faker
from factory.django import DjangoModelFactory

from task_overflow.trackers.models import Adjustment
from task_overflow.trackers.models import Tracker


class TrackerFactory(DjangoModelFactory):
    name = Faker("name")
    balance = Faker("pyint", min_value=1, max_value=100)
    daily_adjustment = Faker("pyint", min_value=1, max_value=100)

    class Meta:
        model = Tracker


class AdjustmentFactory(DjangoModelFactory):
    value = Faker("pyint", min_value=1, max_value=100)

    class Meta:
        model = Adjustment
