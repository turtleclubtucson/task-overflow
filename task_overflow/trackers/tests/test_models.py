from typing import TYPE_CHECKING

from .factories import AdjustmentFactory
from .factories import TrackerFactory

if TYPE_CHECKING:
    from task_overflow.characters.models import Character


def test_adjustment_updates_tracker_balance_when_being_created(
    user1_character: "Character",
):
    tracker = TrackerFactory(character=user1_character, balance=1)
    AdjustmentFactory(tracker=tracker, value=2)

    tracker.refresh_from_db()
    assert tracker.balance == 3


def test_adjustment_updates_tracker_balance_when_being_deleted(
    user1_character: "Character",
):
    tracker = TrackerFactory(character=user1_character, balance=1)
    adjustment = AdjustmentFactory(tracker=tracker, value=2)
    adjustment.delete()

    tracker.refresh_from_db()
    assert tracker.balance == 1
