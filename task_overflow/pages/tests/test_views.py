from http import HTTPStatus
from typing import TYPE_CHECKING

import pytest
from django.urls import reverse

from tests.utils import is_redirect

if TYPE_CHECKING:
    from django.test.client import Client

    from task_overflow.characters.models import Character


@pytest.mark.django_db()
def test_home_view_renders_home_page_if_not_logged_in(client: "Client"):
    resp = client.get(reverse("pages:home"))
    assert resp.status_code == HTTPStatus.OK
    assert b"Use this document" in resp.content


def test_home_view_redirects_to_character_create_if_logged_in_user_has_no_character(
    user1_client: "Client",
):
    resp = user1_client.get(reverse("pages:home"))
    assert is_redirect(
        resp,
        for_path=reverse("characters:create"),
    )


def test_home_view_redirects_to_character_detail_if_logged_in_user_has_character(
    user1_character: "Character",
    user1_client: "Client",
):
    resp = user1_client.get(reverse("pages:home"))
    assert is_redirect(
        resp,
        for_path=reverse(
            "characters:detail",
            kwargs={
                "pk": user1_character.pk,
            },
        ),
    )
