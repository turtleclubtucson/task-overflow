from django.shortcuts import redirect
from django.shortcuts import render
from django.urls import reverse


def home_view(request):
    if request.user.is_authenticated:
        if request.user.character_set.exists():
            character = request.user.character_set.first()
            return redirect(
                reverse(
                    "characters:detail",
                    kwargs={
                        "pk": character.pk,
                    },
                ),
            )
        return redirect(
            reverse("characters:create"),
        )
    return render(request, "pages/home.html")
