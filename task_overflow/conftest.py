from typing import TYPE_CHECKING

import pytest
from django.utils import timezone

from task_overflow.characters.models import CharacterLevelUp
from task_overflow.characters.tests.factories import CharacterDayFactory
from task_overflow.characters.tests.factories import CharacterFactory
from task_overflow.characters.tests.factories import CharacterLevelUpFactory
from task_overflow.characters.tests.factories import CharacterMessageFactory
from task_overflow.encounters.tests.factories import EncounterFactory
from task_overflow.items.models import Item
from task_overflow.items.tests.factories import CharacterItemFactory
from task_overflow.items.tests.factories import ItemFactory
from task_overflow.monsters.tests.factories import MonsterFactory
from task_overflow.tasks.tests.factories import CategoryFactory
from task_overflow.tasks.tests.factories import HabitFactory
from task_overflow.tasks.tests.factories import TodoFactory
from task_overflow.trackers.tests.factories import AdjustmentFactory
from task_overflow.trackers.tests.factories import TrackerFactory
from task_overflow.users.tests.factories import UserFactory

if TYPE_CHECKING:
    from datetime import date

    from django.test.client import Client

    from task_overflow.characters.models import Character
    from task_overflow.characters.models import CharacterDay
    from task_overflow.characters.models import CharacterMessage
    from task_overflow.encounters.models import Encounter
    from task_overflow.items.models import CharacterItem
    from task_overflow.monsters.models import Monster
    from task_overflow.tasks.models import Category
    from task_overflow.tasks.models import Habit
    from task_overflow.tasks.models import Todo
    from task_overflow.trackers.models import Adjustment
    from task_overflow.trackers.models import Tracker
    from task_overflow.users.models import User


@pytest.fixture(autouse=True)
def _media_storage(settings, tmpdir) -> None:
    settings.MEDIA_ROOT = tmpdir.strpath


@pytest.fixture()
def small_med_pack(db) -> "Item":
    return Item.objects.get(name="Small Med Pack")


@pytest.fixture()
def fists(db) -> "Item":
    return Item.objects.fists()


@pytest.fixture()
def knife(db) -> "Item":
    return Item.objects.knife()


@pytest.fixture()
def item(db) -> "Item":
    return ItemFactory()


@pytest.fixture()
def user1(db) -> "User":
    return UserFactory()


@pytest.fixture()
def user1_today(user1: "User") -> "date":
    return timezone.now().astimezone(user1.timezone).date()


@pytest.fixture()
def user1_character(user1: "User") -> "Character":
    return CharacterFactory(user=user1)


@pytest.fixture()
def user1_character_fists(
    fists: "Item",
    user1_character: "Character",
) -> "CharacterItem":
    return CharacterItemFactory(
        item=fists,
        character=user1_character,
    )


@pytest.fixture()
def _user1_character_fists_were_equipped(
    user1_character_fists: "CharacterItem",
) -> "CharacterItem":
    character_fists = user1_character_fists
    character_fists.is_equipped = True
    character_fists.save()


@pytest.fixture()
def user1_character_knife(
    knife: "Item",
    user1_character: "Character",
) -> "CharacterItem":
    return CharacterItemFactory(
        item=knife,
        character=user1_character,
    )


@pytest.fixture()
def user1_character_message(user1_character: "Character") -> "CharacterMessage":
    return CharacterMessageFactory(character=user1_character)


@pytest.fixture()
def user1_character_day(
    user1_character: "Character",
    user1_today: "date",
) -> "CharacterDay":
    return CharacterDayFactory(
        character=user1_character,
        day=user1_today,
    )


@pytest.fixture()
def user1_character_level_up(user1_character: "Character") -> "CharacterLevelUp":
    return CharacterLevelUpFactory(character=user1_character)


@pytest.fixture()
def _user1_character_level_up_was_completed(
    user1_character_level_up: "CharacterLevelUp",
) -> None:
    level_up = user1_character_level_up
    level_up.status = CharacterLevelUp.Status.COMPLETED
    level_up.save()


@pytest.fixture()
def user1_character_day_encounter(user1_character_day: "CharacterDay") -> "Encounter":
    return EncounterFactory(character_day=user1_character_day)


@pytest.fixture()
def _user1_character_day_encounter_was_completed(
    user1_character_day_encounter: "Encounter",
) -> None:
    encounter = user1_character_day_encounter
    encounter.is_active = False
    encounter.save()


@pytest.fixture()
def _user1_character_day_encounter_attacker_always_hits(
    user1_character_day_encounter: "Encounter",
) -> None:
    encounter = user1_character_day_encounter
    encounter._check_if_hit = lambda *_, **__: True


@pytest.fixture()
def user1_character_day_encounter_monster(
    user1_character_day_encounter: "Encounter",
) -> "Monster":
    return MonsterFactory(encounter=user1_character_day_encounter)


@pytest.fixture()
def user1_character_category(user1_character: "Character") -> "Category":
    return CategoryFactory(character=user1_character)


@pytest.fixture()
def user1_character_todo(user1_character: "Character") -> "Todo":
    return TodoFactory(character=user1_character)


@pytest.fixture()
def _user1_character_todo_is_due_today(user1_character_todo: "Todo") -> None:
    todo = user1_character_todo
    user = todo.character.user

    todo.due_date = user.today()
    todo.save()


@pytest.fixture()
def _user1_character_todo_was_completed(user1_character_todo: "Todo") -> None:
    todo = user1_character_todo

    todo.is_complete = True
    todo.save()


@pytest.fixture()
def user1_character_habit(user1_character: "Character") -> "Habit":
    return HabitFactory(character=user1_character)


@pytest.fixture()
def user1_character_tracker(user1_character: "Character") -> "Tracker":
    return TrackerFactory(character=user1_character)


@pytest.fixture()
def user1_character_tracker_adjustment(
    user1_character_tracker: "Tracker",
) -> "Adjustment":
    return AdjustmentFactory(tracker=user1_character_tracker)


@pytest.fixture()
def user1_character_item(
    item: "Item",
    user1_character: "Character",
) -> "CharacterItem":
    return CharacterItemFactory(
        character=user1_character,
        item=item,
    )


@pytest.fixture()
def _user1_character_item_was_all_used(user1_character_item: "CharacterItem") -> None:
    character_item = user1_character_item
    character_item.quantity = 0
    character_item.save()


@pytest.fixture()
def user1_client(client: "Client", user1: "User") -> "Client":
    client.force_login(user1)
    return client


@pytest.fixture()
def user2(db) -> "User":
    return UserFactory()


@pytest.fixture()
def user2_today(user2: "User") -> "date":
    return timezone.now().astimezone(user2.timezone).date()


@pytest.fixture()
def user2_character(user2: "User") -> "Character":
    return CharacterFactory(user=user2)


@pytest.fixture()
def user2_character_day(
    user2_character: "Character",
    user2_today: "date",
) -> "CharacterDay":
    return CharacterDayFactory(
        character=user2_character,
        day=user2_today,
    )


@pytest.fixture()
def user2_character_level_up(user2_character: "Character") -> "CharacterLevelUp":
    return CharacterLevelUpFactory(character=user2_character)


@pytest.fixture()
def user2_character_day_encounter(user2_character_day: "CharacterDay") -> "Encounter":
    return EncounterFactory(character_day=user2_character_day)


@pytest.fixture()
def user2_character_habit(user2_character: "Character") -> "Habit":
    return HabitFactory(character=user2_character)


@pytest.fixture()
def user2_character_tracker(user2_character: "Character") -> "Tracker":
    return TrackerFactory(character=user2_character)


@pytest.fixture()
def user2_character_tracker_adjustment(
    user2_character_tracker: "Tracker",
) -> "Adjustment":
    return AdjustmentFactory(tracker=user2_character_tracker)


@pytest.fixture()
def user2_character_category(user2_character: "Character") -> "Category":
    return CategoryFactory(character=user2_character)


@pytest.fixture()
def user2_character_todo(user2_character: "Character") -> "Todo":
    return TodoFactory(character=user2_character)


@pytest.fixture()
def user2_character_item(
    item: "Item",
    user2_character: "Character",
) -> "CharacterItem":
    return CharacterItemFactory(
        item=item,
        character=user2_character,
    )


@pytest.fixture()
def user2_client(client: "Client", user2: "User") -> "Client":
    client.force_login(user2)
    return client
