from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models.query import QuerySet
from django.shortcuts import get_object_or_404
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView

from task_overflow.mixins import CharacterRelatedDataViewMixin
from task_overflow.mixins import DefaultFormViewMixin

from .models import CharacterItem
from .models import Item


class CharacterItemCreateView(
    LoginRequiredMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    CreateView,
):
    model = CharacterItem
    fields = [
        "quantity",
    ]
    form_header = "Purchase Item"
    redirect_to_character_on_success = True
    form_instance_assignments = {"character": "character"}

    def item(self) -> "Item":
        return get_object_or_404(Item.objects.all(), pk=self.kwargs["item"])

    @property
    def form_message(self) -> str:
        item = self.item()
        creds = self.character().credits
        return f"{item.name}, {item.base_price} ({creds} cryptobucks)"

    @property
    def cancel_path(self) -> str:
        return self.character().get_absolute_url()

    def get(self, *args, **kwargs):
        # Check if item pk is valid.
        self.item()
        return super().get(*args, **kwargs)

    def form_valid(self, form):
        item = self.item()
        if self.character().credits < (item.base_price * form.cleaned_data["quantity"]):
            form.add_error(
                "quantity",
                CharacterItem.CANNOT_AFFORD_PURCHASE_ERROR_MESSAGE,
            )
            return super().form_invalid(form)
        form.instance.item_id = item.pk
        resp = super().form_valid(form)
        self.character().credits -= item.base_price * form.instance.quantity
        self.character().save()
        return resp


character_item_create_view = CharacterItemCreateView.as_view()


class CharacterItemEquipMeleeWeaponView(
    LoginRequiredMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    UpdateView,
):
    model = CharacterItem
    fields = []
    form_header = "Equip Item"
    redirect_to_character_on_success = True

    def item(self) -> "Item":
        return self.object.item

    @property
    def form_message(self) -> str:
        return f"Are you sure you want to equip {self.item().name}?"

    def get_queryset(self) -> QuerySet["CharacterItem"]:
        return self.request.user.character_items().filter(
            item__in=Item.objects.melee_weapons(),
        )

    def form_valid(self, form):
        resp = super().form_valid(form)
        self.character().melee_weapons().update(is_equipped=False)
        form.instance.is_equipped = True
        form.instance.save()
        return resp


character_item_equip_melee_weapon_view = CharacterItemEquipMeleeWeaponView.as_view()


class CharacterItemUseView(
    LoginRequiredMixin,
    DefaultFormViewMixin,
    CharacterRelatedDataViewMixin,
    UpdateView,
):
    model = CharacterItem
    fields = []
    form_header = "Use Item"
    redirect_to_character_on_success = True

    def item(self) -> "Item":
        return self.object.item

    @property
    def form_message(self) -> str:
        return f"Are you sure you want to use {self.item().name}?"

    def get_queryset(self) -> QuerySet["CharacterItem"]:
        return self.request.user.character_items().filter(quantity__gte=1)

    def form_valid(self, form):
        resp = super().form_valid(form)
        form.instance.use()
        return resp


character_item_use_view = CharacterItemUseView.as_view()
