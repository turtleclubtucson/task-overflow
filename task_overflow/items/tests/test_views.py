from http import HTTPStatus
from typing import TYPE_CHECKING

import pytest
from django.urls import reverse

from task_overflow.items.models import CharacterItem
from tests.utils import is_login_redirect
from tests.utils import is_redirect

if TYPE_CHECKING:
    from django.test.client import Client

    from task_overflow.characters.models import Character
    from task_overflow.items.models import Item


def test_character_item_create_view_loads_for_logged_in_owner(
    small_med_pack: "Item",
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    resp = user1_client.get(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": small_med_pack.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert small_med_pack.name.encode() in resp.content
    assert str(small_med_pack.base_price).encode() in resp.content


def test_character_item_create_view_does_not_load_for_invalid_item_id(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    resp = user1_client.get(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": 99999,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_item_create_view_does_not_load_for_non_owner(
    small_med_pack: "Item",
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    resp = user2_client.get(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": small_med_pack.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_item_create_view_does_not_load_for_anon_user(
    small_med_pack: "Item",
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    resp = client.get(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": small_med_pack.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_character_item_create_view_saves_for_logged_in_owner(
    small_med_pack: "Item",
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    expected_credits = character.credits - (small_med_pack.base_price * 2)
    data = {
        "quantity": 2,
    }
    resp = user1_client.post(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": small_med_pack.pk,
            },
        ),
        data=data,
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    character.refresh_from_db()
    assert character.characteritem_set.count() == 1
    assert character.characteritem_set.filter(
        item_id=small_med_pack.id,
        quantity=2,
    ).exists()
    assert character.credits == expected_credits


def test_character_item_create_view_does_not_save_for_invalid_item_id(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    data = {
        "quantity": 2,
    }
    resp = user1_client.post(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": 99999,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved
    assert character.characteritem_set.count() == 0


def test_character_item_create_view_does_not_save_when_character_cannot_afford_it(
    small_med_pack: "Item",
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    data = {
        "quantity": 99999,
    }
    resp = user1_client.post(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": small_med_pack.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.OK
    assert CharacterItem.CANNOT_AFFORD_PURCHASE_ERROR_MESSAGE.encode() in resp.content

    # Was not saved
    assert character.characteritem_set.count() == 0


def test_character_item_create_view_does_not_save_for_non_owner(
    small_med_pack: "Item",
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    data = {
        "quantity": 1,
    }
    resp = user2_client.post(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": small_med_pack.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not saved
    assert character.characteritem_set.count() == 0


def test_character_item_create_view_does_not_save_for_anon_user(
    small_med_pack: "Item",
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    data = {
        "quantity": 1,
    }
    resp = client.post(
        reverse(
            "items:character-item-create",
            kwargs={
                "character": character.pk,
                "item": small_med_pack.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(resp)

    # Was not saved
    assert character.characteritem_set.count() == 0


def test_character_melee_weapon_equip_loads_for_logged_in_owner(
    user1_character_fists: "CharacterItem",
    user1_client: "Client",
):
    character_fists = user1_character_fists
    fists = character_fists.item
    resp = user1_client.get(
        reverse(
            "items:character-melee-weapon-equip",
            kwargs={
                "pk": character_fists.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert fists.name.encode() in resp.content


def test_character_melee_weapon_equip_does_not_load_for_non_melee_weapon(
    user1_character_item: "CharacterItem",
    user1_client: "Client",
):
    character_item = user1_character_item
    resp = user1_client.get(
        reverse(
            "items:character-melee-weapon-equip",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_melee_weapon_equip_does_not_load_for_non_owner(
    user1_character_fists: "CharacterItem",
    user2_client: "Client",
):
    character_fists = user1_character_fists
    resp = user2_client.get(
        reverse(
            "items:character-melee-weapon-equip",
            kwargs={
                "pk": character_fists.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_melee_weapon_equip_does_not_load_for_anon_user(
    user1_character_fists: "CharacterItem",
    client: "Client",
):
    character_fists = user1_character_fists
    resp = client.get(
        reverse(
            "items:character-melee-weapon-equip",
            kwargs={
                "pk": character_fists.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


@pytest.mark.usefixtures("_user1_character_fists_were_equipped")
def test_character_melee_weapon_equip_saves_for_logged_in_owner(
    user1_character_fists: "CharacterItem",
    user1_character_knife: "CharacterItem",
    user1_client: "Client",
):
    character_fists = user1_character_fists
    character_knife = user1_character_knife
    character = character_knife.character

    resp = user1_client.post(
        reverse(
            "items:character-melee-weapon-equip",
            kwargs={
                "pk": character_knife.pk,
            },
        ),
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    # Previously equipped melee weapon is uneqipped.
    character_fists.refresh_from_db()
    assert character_fists.is_equipped is False

    # Specified melee weapon is now equipped.
    character_knife.refresh_from_db()
    assert character_knife.is_equipped is True


def test_character_melee_weapon_equip_does_not_save_for_non_melee_weapon(
    user1_character_item: "CharacterItem",
    user1_client: "Client",
):
    character_item = user1_character_item
    resp = user1_client.post(
        reverse(
            "items:character-melee-weapon-equip",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_melee_weapon_equip_does_not_save_for_non_owner(
    user1_character_fists: "CharacterItem",
    user2_client: "Client",
):
    character_fists = user1_character_fists
    resp = user2_client.post(
        reverse(
            "items:character-melee-weapon-equip",
            kwargs={
                "pk": character_fists.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_melee_weapon_equip_does_not_save_for_anon_user(
    user1_character_fists: "CharacterItem",
    client: "Client",
):
    character_fists = user1_character_fists
    resp = client.post(
        reverse(
            "items:character-melee-weapon-equip",
            kwargs={
                "pk": character_fists.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_character_item_use_loads_for_logged_in_owner(
    user1_character_item: "CharacterItem",
    user1_client: "Client",
):
    character_item = user1_character_item
    resp = user1_client.get(
        reverse(
            "items:character-item-use",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert character_item.item.name.encode() in resp.content


@pytest.mark.usefixtures("_user1_character_item_was_all_used")
def test_character_item_use_does_not_load_for_out_of_stock_item(
    user1_character_item: "CharacterItem",
    user1_client: "Client",
):
    character_item = user1_character_item
    resp = user1_client.get(
        reverse(
            "items:character-item-use",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_item_use_does_not_load_for_non_owner(
    user1_character_item: "CharacterItem",
    user2_client: "Client",
):
    character_item = user1_character_item
    resp = user2_client.get(
        reverse(
            "items:character-item-use",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_item_use_does_not_load_for_anon_user(
    user1_character_item: "CharacterItem",
    client: "Client",
):
    character_item = user1_character_item
    resp = client.get(
        reverse(
            "items:character-item-use",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_character_item_saves_for_logged_in_owner(
    user1_character_item: "CharacterItem",
    user1_client: "Client",
):
    character_item = user1_character_item
    expected_quantity = character_item.quantity - 1
    character = character_item.character
    resp = user1_client.post(
        reverse(
            "items:character-item-use",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    # Was saved
    character_item.refresh_from_db()
    assert character_item.quantity == expected_quantity


@pytest.mark.usefixtures("_user1_character_item_was_all_used")
def test_character_item_does_not_save_for_used_up_item(
    user1_character_item: "CharacterItem",
    user1_client: "Client",
):
    character_item = user1_character_item
    resp = user1_client.post(
        reverse(
            "items:character-item-use",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not used.
    character_item.refresh_from_db()
    assert character_item.quantity == 0


def test_character_item_does_not_save_for_non_owner(
    user1_character_item: "CharacterItem",
    user2_client: "Client",
):
    character_item = user1_character_item
    expected_quantity = character_item.quantity
    resp = user2_client.post(
        reverse(
            "items:character-item-use",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not used.
    character_item.refresh_from_db()
    assert character_item.quantity == expected_quantity


def test_character_item_does_not_save_for_anon_user(
    user1_character_item: "CharacterItem",
    client: "Client",
):
    character_item = user1_character_item
    expected_quantity = character_item.quantity
    resp = client.post(
        reverse(
            "items:character-item-use",
            kwargs={
                "pk": character_item.pk,
            },
        ),
    )
    assert is_login_redirect(resp)

    # Was not used.
    character_item.refresh_from_db()
    assert character_item.quantity == expected_quantity
