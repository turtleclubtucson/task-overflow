from typing import TYPE_CHECKING

import pytest

from task_overflow.characters.tests.factories import CharacterFactory
from task_overflow.items.models import CharacterItem
from task_overflow.items.models import Item
from task_overflow.items.models import OneTimeStatAdjustment
from task_overflow.items.tests.factories import CharacterItemFactory
from task_overflow.items.tests.factories import ItemFactory
from task_overflow.items.tests.factories import OneTimeStatAdjustmentFactory

if TYPE_CHECKING:
    from task_overflow.characters.models import Character
    from task_overflow.users.models import User


def test_one_time_stat_adjustment_apply_to_character_increases_characer_stat(
    item: "Item",
    user1: "User",
):
    character = CharacterFactory(user=user1, dexterity=1)
    stat_adjustment = OneTimeStatAdjustmentFactory(
        item=item,
        stat=OneTimeStatAdjustment.Stats.DEXTERITY,
        adjustment=2,
    )
    stat_adjustment.apply_to_character(character)

    character.refresh_from_db()
    assert character.dexterity == 3


@pytest.mark.usefixtures("db")
def test_item_manager_fists_returns_fists_item():
    fists = Item.objects.fists()
    assert fists.name == "Fists"


@pytest.mark.usefixtures("db")
def test_item_manager_fists_returns_knife_item():
    fists = Item.objects.knife()
    assert fists.name == "Knife"


@pytest.mark.usefixtures("db")
def test_item_manager_for_sale_scope_does_not_include_items_marked_as_not_for_sale():
    not_for_sale = ItemFactory(list_for_sale=False)
    assert not_for_sale not in list(Item.objects.for_sale())


@pytest.mark.usefixtures("db")
def test_item_manager_single_use_items_scope_includes_only_one_time_stat_adjust_items():
    adjusting_item = ItemFactory()
    OneTimeStatAdjustmentFactory(
        item=adjusting_item,
        stat=OneTimeStatAdjustment.Stats.STRENGTH,
        adjustment=1,
    )
    non_adjusting_item = ItemFactory()

    assert adjusting_item in Item.objects.single_use_items()
    assert non_adjusting_item not in Item.objects.single_use_items()


@pytest.mark.usefixtures("db")
def test_item_manager_melee_weapons_scope_includes_only_items_with_melee_damage():
    melee_weapon = ItemFactory(melee_damage="1d23")
    potion = ItemFactory()

    assert melee_weapon in Item.objects.melee_weapons()
    assert potion not in Item.objects.melee_weapons()


@pytest.mark.usefixtures("db")
def test_item_calculate_melee_damage_parses_die_roll_info_from_field(mocker):
    mocked_random_randint = mocker.patch("random.randint", return_value=1)

    weapon = ItemFactory(melee_damage="3d18")
    assert weapon.calculate_melee_damage() == 3

    assert mocked_random_randint.call_count == 3
    mocked_random_randint.assert_called_with(1, 18)


@pytest.mark.usefixtures("db")
def test_item_calculate_melee_damage_raises_error_if_not_a_melee_weapon():
    item = ItemFactory(melee_damage="")
    with pytest.raises(Item.NoMeleeDamageSpecifiedError):
        item.calculate_melee_damage()


def test_item_use_on_character_applies_one_time_stat_adjustments_to_character(
    item: "Item",
    user1: "User",
):
    character = CharacterFactory(
        user=user1,
        strength=1,
        dexterity=1,
    )

    OneTimeStatAdjustmentFactory(
        item=item,
        stat=OneTimeStatAdjustment.Stats.STRENGTH,
        adjustment=1,
    )
    OneTimeStatAdjustmentFactory(
        item=item,
        stat=OneTimeStatAdjustment.Stats.DEXTERITY,
        adjustment=2,
    )

    item.use_on_character(character)

    character.refresh_from_db()
    assert character.strength == 2
    assert character.dexterity == 3


@pytest.mark.usefixtures("_user1_character_fists_were_equipped")
def test_character_item_only_one_melee_weapon_can_be_equipped_at_the_same_time(
    user1_character_fists: "CharacterItem",
):
    character = user1_character_fists.character
    knife = Item.objects.get(name="Knife")

    with pytest.raises(CharacterItem.CannotEquipMultipleMeleeWeaponsError):
        CharacterItemFactory(
            item=knife,
            character=character,
            is_equipped=True,
        )


def test_character_item_use_adds_adjustments_and_reduces_quantity(
    small_med_pack: "Item",
    user1: "User",
):
    character = CharacterFactory(
        user=user1,
        hit_points=3,
    )
    character_item = CharacterItemFactory(
        character=character,
        item=small_med_pack,
        quantity=2,
    )
    character_item.use()

    # Med pack added 1 HP to character
    character.refresh_from_db()
    assert character.hit_points == 4

    # Quantity of character item was reduced by one.
    assert character_item.quantity == 1


def test_character_item_use_raises_error_if_out_of_item(
    small_med_pack: "Item",
    user1_character: "Character",
):
    character = user1_character

    character_item = CharacterItemFactory(
        character=character,
        item=small_med_pack,
        quantity=0,
    )
    with pytest.raises(CharacterItem.OutOfItemError):
        character_item.use()
