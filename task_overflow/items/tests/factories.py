from factory import Faker
from factory.django import DjangoModelFactory

from task_overflow.items.models import CharacterItem
from task_overflow.items.models import Item
from task_overflow.items.models import OneTimeStatAdjustment


class ItemFactory(DjangoModelFactory):
    name = Faker("name")
    base_price = Faker(
        "pyint",
        min_value=10,
        max_value=20,
    )

    class Meta:
        model = Item


class OneTimeStatAdjustmentFactory(DjangoModelFactory):
    class Meta:
        model = OneTimeStatAdjustment


class CharacterItemFactory(DjangoModelFactory):
    quantity = Faker(
        "pyint",
        min_value=2,
        max_value=10,
    )

    class Meta:
        model = CharacterItem
