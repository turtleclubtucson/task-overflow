from django.urls import path

from .views import character_item_create_view
from .views import character_item_equip_melee_weapon_view
from .views import character_item_use_view

app_name = "items"
urlpatterns = [
    path(
        "<int:character>/character-items/<int:item>/~create/",
        view=character_item_create_view,
        name="character-item-create",
    ),
    path(
        "character-items/melee-weapons/<int:pk>/~equip/",
        view=character_item_equip_melee_weapon_view,
        name="character-melee-weapon-equip",
    ),
    path(
        "character-items/<int:pk>/~use/",
        view=character_item_use_view,
        name="character-item-use",
    ),
]
