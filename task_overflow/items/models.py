from typing import TYPE_CHECKING

from django.db import models
from django.dispatch import receiver
from model_utils.models import TimeStampedModel

from task_overflow.utils import roll_dice

from .managers import ItemManager

if TYPE_CHECKING:
    from task_overflow.characters.models import Character


class Item(TimeStampedModel):
    class NoMeleeDamageSpecifiedError(Exception):
        pass

    name = models.CharField(max_length=127, unique=True)
    base_price = models.IntegerField()
    list_for_sale = models.BooleanField(default=True)
    melee_damage = models.CharField(max_length=7, default="")
    icon_name = models.CharField(default="")

    objects = ItemManager()

    def calculate_melee_damage(self) -> int:
        if not self.melee_damage:
            raise Item.NoMeleeDamageSpecifiedError(self.pk)
        num, sides = self.melee_damage.split("d")
        num = int(num)
        sides = int(sides)
        return roll_dice(num_sides=sides, num_dice=num)

    def use_on_character(self, character: "Character"):
        for adjustment in self.onetimestatadjustment_set.all():
            adjustment.apply_to_character(character)


class OneTimeStatAdjustment(TimeStampedModel):
    class Stats(models.TextChoices):
        STRENGTH = "strength", "STRENGTH"
        DEXTERITY = "dexterity", "DEXTERITY"
        HIT_POINTS = "hit_points", "HIT_POINTS"
        EXPERIENCE_POINTS = "experience_points", "EXPERIENCE_POINTS"

    item = models.ForeignKey(
        "Item",
        on_delete=models.CASCADE,
    )
    stat = models.CharField(
        choices=Stats.choices,
    )
    adjustment = models.IntegerField()

    def apply_to_character(self, character: "Character") -> None:
        existing_stat = getattr(character, self.stat)
        new_stat = existing_stat + self.adjustment
        setattr(character, self.stat, new_stat)
        character.save()


class CharacterItem(TimeStampedModel):
    class CannotEquipMultipleMeleeWeaponsError(Exception):
        pass

    CANNOT_AFFORD_PURCHASE_ERROR_MESSAGE = "Cannot afford that purchase"

    class OutOfItemError(Exception):
        pass

    character = models.ForeignKey(
        "characters.Character",
        on_delete=models.CASCADE,
    )
    item = models.ForeignKey(
        "Item",
        on_delete=models.RESTRICT,
    )
    quantity = models.IntegerField(default=1)
    is_equipped = models.BooleanField(default=False)

    def use(self) -> None:
        if self.quantity <= 0:
            msg = f"character {self.character.pk} characteritem {self.pk}"
            raise CharacterItem.OutOfItemError(msg)
        self.item.use_on_character(self.character)
        self.quantity -= 1
        self.save()


@receiver(models.signals.pre_save, sender=CharacterItem)
def prevent_equipping_multiple_melee_weapons(sender, instance, **kwargs):
    if instance.is_equipped:
        if (
            CharacterItem.objects.filter(
                is_equipped=True,
                item__in=Item.objects.melee_weapons(),
            )
            .exclude(pk=instance.pk)
            .exists()
        ):
            raise CharacterItem.CannotEquipMultipleMeleeWeaponsError
