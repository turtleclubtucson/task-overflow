# Generated by Django 4.2.11 on 2024-08-30 15:48

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='characteritem',
            name='quantity',
            field=models.IntegerField(default=0),
        ),
    ]
