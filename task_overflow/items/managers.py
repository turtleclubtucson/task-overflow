from typing import TYPE_CHECKING

from django.db import models

if TYPE_CHECKING:
    from .models import Item


class ItemManager(models.Manager):
    def fists(self) -> "Item":
        return self.get(name="Fists")

    def for_sale(self) -> models.query.QuerySet["Item"]:
        return self.filter(list_for_sale=True)

    def melee_weapons(self) -> models.query.QuerySet["Item"]:
        return self.exclude(melee_damage="")

    def knife(self) -> "Item":
        return self.get(name="Knife")

    def single_use_items(self) -> models.query.QuerySet["Item"]:
        item_pks = [
            item.pk for item in self.all() if item.onetimestatadjustment_set.exists()
        ]
        return self.filter(pk__in=item_pks)
