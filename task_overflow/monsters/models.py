from django.db import models
from django.dispatch import receiver
from model_utils.models import TimeFramedModel


class Monster(TimeFramedModel):
    DEATH_MESSAGE_TEMPLATE = "{name} died!"

    class AvatarImage(models.TextChoices):
        DEFAULT = "default.jpg", "default.jpg"
        CYBERPUNK_CYBORG = "cyberpunk-cyborg.jpg", "cyberpunk-cyborg.jpg"
        PUNK_CYBERPUNK = "punk-cyberpunk.png", "punk-cyberpunk."

    encounter = models.ForeignKey(
        "encounters.Encounter",
        on_delete=models.CASCADE,
    )
    weapon = models.ForeignKey(
        "items.Item",
        on_delete=models.RESTRICT,
    )
    name = models.CharField(max_length=127)
    avatar_image = models.CharField(
        max_length=127,
        choices=AvatarImage.choices,
        default=AvatarImage.DEFAULT,
    )
    strength = models.IntegerField()
    dexterity = models.IntegerField()
    initiative = models.IntegerField()
    hit_points = models.IntegerField()
    is_active = models.BooleanField(default=True)

    def die(self) -> None:
        self.is_active = False
        self.save()

        event_text = Monster.DEATH_MESSAGE_TEMPLATE.format(name=self.name)
        self.encounter.event_set.create(text=event_text)


@receiver(models.signals.post_save, sender=Monster)
def die_when_hp_drops_to_zero(sender, instance, **kwargs) -> None:
    if instance.is_active and instance.hit_points <= 0:
        instance.die()
