import random

from task_overflow.items.models import Item
from task_overflow.utils import d6

from .models import Monster

NAMES = [
    "Cap Server",
    "Jean Piker",
    "Gai Callister",
    "Ren Bakerclinton",
    "Sara Cetmer",
    "Stephen Key",
    "Rina Wscraper",
    "Golda Yyler",
    "Pip Rowe",
    "Link Budos",
    "Triniti Portalla",
    "Quade Sammet",
    "Alexa Nine",
    "Simula Protoface",
    "Superhans Bozhenko",
    "Leeloo Baconsmith",
]


def generate_monster_params(character) -> dict:
    name = _generate_name()
    weapon = _choose_weapon_for_character_level(character.level)
    avatar_image = _choose_avatar_image()
    strength = character.level + d6()
    dexterity = character.level + d6()
    initiative = character.level + d6()
    hit_points = character.level + d6()
    return {
        "weapon": weapon,
        "name": name,
        "avatar_image": avatar_image,
        "strength": strength,
        "dexterity": dexterity,
        "initiative": initiative,
        "hit_points": hit_points,
        "is_active": True,
    }


def _generate_name() -> str:
    return random.choice(NAMES)


def _choose_weapon_for_character_level(level: int) -> "Item":
    return Item.objects.fists()


def _choose_avatar_image() -> str:
    return random.choice(Monster.AvatarImage.choices)[0]
