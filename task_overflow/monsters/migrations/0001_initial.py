# Generated by Django 4.2.11 on 2024-08-23 15:38

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('encounters', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Monster',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('start', models.DateTimeField(blank=True, null=True, verbose_name='start')),
                ('end', models.DateTimeField(blank=True, null=True, verbose_name='end')),
                ('name', models.CharField(max_length=127)),
                ('strength', models.IntegerField()),
                ('dexterity', models.IntegerField()),
                ('hit_points', models.IntegerField()),
                ('is_active', models.BooleanField(default=True)),
                ('encounter', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='encounters.encounter')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
