# Generated by Django 4.2.11 on 2024-09-01 17:44

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('items', '0010_set_default_character_items'),
        ('monsters', '0002_monster_avatar_image'),
    ]

    operations = [
        migrations.AddField(
            model_name='monster',
            name='weapon',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.RESTRICT, to='items.item'),
            preserve_default=False,
        ),
    ]
