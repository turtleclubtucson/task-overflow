from task_overflow.monsters.models import Monster


def test_monster_dies_when_hit_points_drop_to_zero(
    user1_character_day_encounter_monster: "Monster",
):
    monster = user1_character_day_encounter_monster

    monster.hit_points = 0
    monster.save()
    assert monster.is_active is False


def test_monster_die_sets_active_status_and_encounter_message(
    user1_character_day_encounter_monster: "Monster",
):
    monster = user1_character_day_encounter_monster
    encounter = monster.encounter

    monster.die()
    monster.refresh_from_db()
    assert monster.is_active is False
    assert encounter.event_set.count() == 1
    event = encounter.event_set.first()
    assert event.text == Monster.DEATH_MESSAGE_TEMPLATE.format(name=monster.name)
