from typing import TYPE_CHECKING

from task_overflow.monsters.utils import generate_monster_params

if TYPE_CHECKING:
    from task_overflow.characters.models import Character


def test_generate_monster_params_generates_monster_params_based_on_character_level(
    user1_character: "Character",
):
    character = user1_character
    params = generate_monster_params(character)
    for attr in ("strength", "dexterity", "hit_points"):
        assert params[attr] >= character.level
        assert params[attr] <= character.level + 6
