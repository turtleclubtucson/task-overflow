from factory import Faker
from factory import Iterator
from factory.django import DjangoModelFactory

from task_overflow.items.models import Item
from task_overflow.monsters.models import Monster


class MonsterFactory(DjangoModelFactory):
    name = Faker("name")
    strength = Faker(
        "pyint",
        min_value=1,
        max_value=10,
    )
    weapon = Iterator(Item.objects.melee_weapons())
    dexterity = Faker(
        "pyint",
        min_value=1,
        max_value=10,
    )
    initiative = Faker(
        "pyint",
        min_value=1,
        max_value=10,
    )
    hit_points = Faker(
        "pyint",
        min_value=100,
        max_value=1000,
    )

    class Meta:
        model = Monster
