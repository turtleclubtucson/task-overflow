import re
from pathlib import Path
from typing import TYPE_CHECKING

from task_overflow.monsters.templatetags.monster_avatar_url import (
    BASE_MONSTER_IMAGE_PATH,
)
from task_overflow.monsters.templatetags.monster_avatar_url import monster_avatar_url

if TYPE_CHECKING:
    from task_overflow.monsters.models import Monster


def test_monster_avatar_url_returns_url_for_image(
    settings,
    user1_character_day_encounter_monster: "Monster",
):
    monster = user1_character_day_encounter_monster
    img_path = Path(monster.avatar_image)
    img_name = img_path.stem
    img_ext = img_path.suffix
    pattern = re.compile(
        f"{settings.STATIC_URL}{BASE_MONSTER_IMAGE_PATH}/{img_name}.(\\w)+{img_ext}",
    )
    assert pattern.match(monster_avatar_url(monster))
