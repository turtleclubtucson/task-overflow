from typing import TYPE_CHECKING

from django import template
from django.templatetags.static import static

register = template.Library()

if TYPE_CHECKING:
    from task_overflow.monsters.models import Monster


BASE_MONSTER_IMAGE_PATH = "images/monsters"


@register.filter(name="monster_avatar_url")
def monster_avatar_url(monster: "Monster") -> str:
    return static(f"{BASE_MONSTER_IMAGE_PATH}/{monster.avatar_image}")
