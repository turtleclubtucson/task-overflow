from typing import TYPE_CHECKING

from django.db import models

from task_overflow.items.models import Item

if TYPE_CHECKING:
    from .models import Character


class CharacterManager(models.Manager):
    def create_with_default_items(self, *args, **kwargs) -> "Character":
        character = self.create(*args, **kwargs)
        character.characteritem_set.create(item=Item.objects.fists())
        return character
