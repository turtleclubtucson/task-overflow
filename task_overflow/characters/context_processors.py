from django.conf import settings


def unlimited_encounters_mode_setting(request) -> dict:
    return {"UNLIMITED_ENCOUNTER_MODE": settings.UNLIMITED_ENCOUNTER_MODE}
