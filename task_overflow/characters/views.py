from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from django.urls import reverse
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView
from django.views.generic.edit import UpdateView

from task_overflow.hx.decorators import hx_login_required
from task_overflow.hx.mixins import HXLoginRequiredMixin
from task_overflow.hx.utils import htmx_redirect
from task_overflow.items.models import Item
from task_overflow.utils import empty_response

from .models import Character
from .models import CharacterLevelUp
from .models import CharacterMessage


def display_message(request, message: "CharacterMessage") -> None:
    messages.add_message(
        request,
        level=message.level,
        message=message.text,
    )
    message.status = CharacterMessage.Status.VIEWED
    message.save()


class CharacterCreateView(LoginRequiredMixin, CreateView):
    model = Character
    fields = [
        "name",
        "avatar",
    ]
    success_message = "Character created"

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


character_create_view = CharacterCreateView.as_view()


class CharacterDetailView(LoginRequiredMixin, DetailView):
    model = Character

    def get_queryset(self):
        return self.request.user.character_set.all()

    def get_object(self, *args, **kwargs) -> "Character":
        obj = super().get_object(*args, **kwargs)
        for death in obj.pending_deaths():
            death.die()
        for message in obj.ordered_pending_messages():
            display_message(self.request, message=message)
        return obj

    def get_context_data(self, *args, **kwargs) -> dict:
        context = super().get_context_data(*args, **kwargs)
        context["items_for_sale"] = Item.objects.for_sale()
        return context


character_detail_view = CharacterDetailView.as_view()


@hx_login_required
def character_hx_fetch_messages_view(request, pk: int):
    character = get_object_or_404(request.user.character_set, pk=pk)
    for message in character.ordered_pending_messages():
        display_message(request, message=message)
    return render(request, "includes/messages.html")


@hx_login_required
def character_hx_check_for_active_encounter_view(request, pk: int, force: int = 0):
    force = force == 1
    character = get_object_or_404(request.user.character_set, pk=pk)
    active_encounter = character.get_or_create_active_encounter(force=force)
    if active_encounter:
        messages.add_message(
            request,
            message="You were attacked!",
            level=messages.ERROR,
        )
        return htmx_redirect(
            for_path=reverse(
                "encounters:detail",
                kwargs={
                    "pk": active_encounter.pk,
                },
            ),
        )
    return empty_response()


class CharacterHXFetchDetailsView(HXLoginRequiredMixin, DetailView):
    model = Character
    template_name = "characters/includes/character_details.html"

    def get_queryset(self):
        return self.request.user.character_set.all()

    def get_object(self, *args, **kwargs) -> "Character":
        obj = super().get_object(*args, **kwargs)
        for death in obj.pending_deaths():
            death.die()
        return obj


character_hx_fetch_details_view = CharacterHXFetchDetailsView.as_view()


class CharacterUpdateView(LoginRequiredMixin, UpdateView):
    model = Character
    fields = [
        "name",
        "avatar",
    ]
    success_message = "Character updated"

    def get_queryset(self):
        return self.request.user.character_set.all()


character_update_view = CharacterUpdateView.as_view()


class CharacterLevelUpCompleteView(LoginRequiredMixin, UpdateView):
    model = CharacterLevelUp
    fields = [
        "hit_points",
        "strength_points",
        "dexterity_points",
        "initiative_points",
    ]

    @property
    def character(self) -> "Character":
        return self.object.character

    def get_queryset(self):
        return self.request.user.character_level_ups().filter(
            status=CharacterLevelUp.Status.PENDING,
        )

    def get_success_url(self) -> str:
        return self.character.get_absolute_url()

    def form_valid(self, form):
        resp = super().form_valid(form)
        form.instance.complete()
        return resp


character_level_up_complete_view = CharacterLevelUpCompleteView.as_view()
