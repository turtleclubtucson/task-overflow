from django.urls import path

from .views import character_create_view
from .views import character_detail_view
from .views import character_hx_check_for_active_encounter_view
from .views import character_hx_fetch_details_view
from .views import character_hx_fetch_messages_view
from .views import character_level_up_complete_view
from .views import character_update_view

app_name = "characters"
urlpatterns = [
    path("~create/", view=character_create_view, name="create"),
    path("<int:pk>/~update/", view=character_update_view, name="update"),
    path("<int:pk>/", view=character_detail_view, name="detail"),
    path(
        "level-ups/<int:pk>/~complete/",
        view=character_level_up_complete_view,
        name="level-up-complete",
    ),
    path(
        "hx/characters/<int:pk>/",
        view=character_hx_fetch_details_view,
        name="character-hx-fetch-details",
    ),
    path(
        "hx/characters/<int:pk>/messages/",
        view=character_hx_fetch_messages_view,
        name="character-hx-fetch-messages",
    ),
    path(
        "hx/characters/<int:pk>/active-encounter/",
        view=character_hx_check_for_active_encounter_view,
        name="character-hx-check-for-active-encounter",
    ),
    path(
        "hx/characters/<int:pk>/active-encounter/<int:force>/",
        view=character_hx_check_for_active_encounter_view,
        name="character-hx-force-active-encounter",
    ),
]
