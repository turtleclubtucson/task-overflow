from datetime import timedelta
from typing import TYPE_CHECKING

from django.conf import settings
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.core.validators import MinValueValidator
from django.db import models
from django.dispatch import receiver
from django.urls import reverse
from model_utils.models import TimeStampedModel

from task_overflow.encounters.models import Encounter
from task_overflow.items.models import CharacterItem
from task_overflow.items.models import Item
from task_overflow.monsters.utils import generate_monster_params
from task_overflow.utils import d10

from .managers import CharacterManager

if TYPE_CHECKING:
    from task_overflow.tasks.models import Category
    from task_overflow.tasks.models import Habit
    from task_overflow.tasks.models import Todo


NEW_ENCOUNTER_VALUE = 1


MESSAGE_LEVEL_CHOICES = [
    (messages.INFO, "INFO"),
    (messages.WARNING, "WARNING"),
    (messages.ERROR, "ERROR"),
]


class Character(TimeStampedModel):
    BASE_HIT_POINTS = 10
    DEFAULT_STRENGTH = 10
    DEFAULT_DEXTERITY = 10
    DEFAULT_INITIATIVE = 10
    DEFAULT_EXPERIENCE_POINTS = 0
    DEFAULT_LEVEL = 0
    DEFAULT_CREDITS = 100

    DAILY_HEALTH_REGENERATED_MESSAGE_TEMPLATE = "{name} recovered health with rest."
    READY_TO_LEVEL_UP_MESSAGE_TEMPLATE = "{name} ready to level up!"

    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.RESTRICT,
    )
    avatar = models.ImageField(
        null=True,
        blank=True,
        default=None,
    )
    name = models.CharField(
        max_length=127,
    )
    strength = models.IntegerField(default=DEFAULT_STRENGTH)
    dexterity = models.IntegerField(default=DEFAULT_STRENGTH)
    initiative = models.IntegerField(default=DEFAULT_INITIATIVE)
    hit_points = models.IntegerField(default=BASE_HIT_POINTS)
    max_hit_points = models.IntegerField(default=BASE_HIT_POINTS)
    experience_points = models.IntegerField(
        default=0,
        validators=[MinValueValidator(0)],
    )
    level = models.IntegerField(default=0)
    credits = models.IntegerField(default=DEFAULT_CREDITS)

    objects = CharacterManager()

    @property
    def daily_regenerated_hit_points(self) -> int:
        return self.level + 1

    @property
    def maximum_daily_encounters(self) -> int:
        if settings.UNLIMITED_ENCOUNTER_MODE:
            return 10000
        return 1 + self.level

    @property
    def experience_points_for_next_level(self) -> int:
        return 1000 + (self.level * 100)

    @property
    def weapon(self) -> "Item":
        character_item = self.melee_weapons().first()
        if not character_item:
            msg = "No weapon found"
            raise CharacterItem.DoesNotExist(msg)
        return character_item.item

    @property
    def is_active(self) -> bool:
        return not self.characterdeath_set.filter(
            status=CharacterDeath.Status.PENDING,
        ).exists()

    def available_character_items(self) -> models.query.QuerySet["CharacterItem"]:
        return self.characteritem_set.filter(
            quantity__gte=1,
            item__in=Item.objects.single_use_items(),
        )

    def melee_weapons(self) -> models.query.QuerySet["CharacterItem"]:
        return self.characteritem_set.filter(
            item__in=Item.objects.melee_weapons(),
        ).order_by("-is_equipped")

    def ordered_categories(self) -> models.query.QuerySet["Category"]:
        return self.category_set.order_by("-priority")

    def ordered_pending_messages(self) -> models.query.QuerySet["CharacterMessage"]:
        return self.pending_messages().order_by("created")

    def pending_deaths(self) -> models.query.QuerySet["CharacterDeath"]:
        return self.characterdeath_set.filter(status=CharacterDeath.Status.PENDING)

    def pending_messages(self) -> models.query.QuerySet["CharacterMessage"]:
        return self.charactermessage_set.filter(
            status=CharacterMessage.Status.PENDING,
        )

    def ordered_completed_daily_habits(self) -> models.query.QuerySet["Habit"]:
        return (
            self.character_day()
            .completed_habits()
            .order_by(
                models.F("category__priority").desc(nulls_last=True),
            )
        )

    def ordered_pending_daily_habits(self) -> models.query.QuerySet["Habit"]:
        return (
            self.character_day()
            .uncompleted_habits()
            .order_by(
                models.F("category__priority").desc(nulls_last=True),
            )
        )

    def pending_level_up(self) -> "CharacterLevelUp | None":
        return self.characterlevelup_set.filter(
            status=CharacterLevelUp.Status.PENDING,
        ).first()

    def most_recently_completed_level_up(self) -> "CharacterLevelUp":
        return (
            self.characterlevelup_set.filter(
                status=CharacterLevelUp.Status.COMPLETED,
            )
            .order_by("-created")
            .first()
        )

    def ordered_incomplete_todos(self) -> models.query.QuerySet["Todo"]:
        return self.todo_set.filter(is_complete=False).order_by(
            "due_date",
            models.F("category__priority").desc(nulls_last=True),
            "created",
        )

    def default_todos(self) -> models.query.QuerySet["Todo"]:
        return self.ordered_incomplete_todos().default()

    def past_due_todos(self) -> models.query.QuerySet["Todo"]:
        cutoff_date = self.user.today()
        return self.ordered_incomplete_todos().filter(
            is_failed=False,
            due_date__lt=cutoff_date,
        )

    def add_message(
        self,
        text: str,
        level: int = messages.INFO,
    ) -> "CharacterMessage":
        return self.charactermessage_set.create(
            text=text,
            level=level,
        )

    def active_encounter(self) -> "Encounter | None":
        return Encounter.objects.filter(
            is_active=True,
            character_day__in=self.characterday_set.all(),
        ).first()

    def get_or_create_active_encounter(
        self,
        *,
        force: bool = False,
    ) -> "Encounter | None":
        return self.character_day().get_or_create_active_encounter(force=force)

    def character_day(self) -> "CharacterDay":
        return self.get_or_create_character_day()

    def get_or_create_character_day(self) -> "CharacterDay":
        return self.characterday_set.get_or_create(day=self.user.today())[0]

    def regenerate_daily_health(self) -> None:
        self.hit_points += self.daily_regenerated_hit_points
        self.save()
        message_text = Character.DAILY_HEALTH_REGENERATED_MESSAGE_TEMPLATE.format(
            name=self.name,
        )
        self.add_message(message_text)

    def reset(self) -> None:
        # Convenience to reset all character stats to defaults.
        self.strength = Character.DEFAULT_STRENGTH
        self.dexterity = Character.DEFAULT_DEXTERITY
        self.initiative = Character.DEFAULT_INITIATIVE
        self.hit_points = Character.BASE_HIT_POINTS
        self.max_hit_points = Character.BASE_HIT_POINTS
        self.credits = Character.DEFAULT_CREDITS
        self.experience_points = Character.DEFAULT_EXPERIENCE_POINTS
        self.level = Character.DEFAULT_LEVEL
        self.characterlevelup_set.all().delete()
        self.characterday_set.all().delete()
        self.save()

    def get_absolute_url(self) -> str:
        return reverse(
            "characters:detail",
            kwargs={
                "pk": self.pk,
            },
        )


@receiver(models.signals.pre_save, sender=Character)
def xp_does_not_drop_below_zero(sender, instance, **kwargs) -> None:
    if instance.experience_points < 0:
        instance.experience_points = 0


@receiver(models.signals.pre_save, sender=Character)
def level_does_not_drop_below_zero(sender, instance, **kwargs) -> None:
    if instance.level < 0:
        instance.level = 0


@receiver(models.signals.post_save, sender=Character)
def die_when_hp_drops_to_zero(sender, instance, **kwargs) -> None:
    if instance.hit_points <= 0:
        instance.characterdeath_set.create()


@receiver(models.signals.post_save, sender=Character)
def level_up_if_experience_points_are_high_enough(sender, instance, **kwargs) -> None:
    if (
        instance.experience_points > instance.experience_points_for_next_level
        and not instance.pending_level_up()
    ):
        message_text = Character.READY_TO_LEVEL_UP_MESSAGE_TEMPLATE.format(
            name=instance.name,
        )
        instance.add_message(message_text)
        instance.characterlevelup_set.create(total_stat_points=instance.level + 1)


@receiver(models.signals.pre_save, sender=Character)
def limit_hp_to_maximum_for_level(sender, instance, **kwargs) -> None:
    if instance.hit_points > instance.max_hit_points:
        instance.hit_points = instance.max_hit_points


class CharacterMessage(TimeStampedModel):
    class Status(models.TextChoices):
        PENDING = "P", "PENDING"
        VIEWED = "V", "VIEWED"

    character = models.ForeignKey(
        "Character",
        on_delete=models.CASCADE,
    )
    text = models.TextField()
    level = models.IntegerField(
        choices=MESSAGE_LEVEL_CHOICES,
        default=messages.INFO,
    )
    status = models.CharField(
        max_length=1,
        choices=Status.choices,
        default=Status.PENDING,
    )


class CharacterDay(TimeStampedModel):
    character = models.ForeignKey(
        "Character",
        on_delete=models.CASCADE,
    )
    day = models.DateField()

    def active_encounter(self) -> "Encounter | None":
        return self.encounter_set.filter(is_active=True).first()

    def get_or_create_active_encounter(
        self,
        *,
        force: bool = False,
    ) -> "Encounter | None":
        existing = self.active_encounter()
        if existing:
            return existing

        if force is True or (
            self.encounter_set.count() < self.character.maximum_daily_encounters
            and d10() == NEW_ENCOUNTER_VALUE
        ):
            return self._create_new_encounter()

        return None

    def _create_new_encounter(self) -> "Encounter":
        new_encounter = self.encounter_set.create()
        monster_params = generate_monster_params(self.character)
        monster = new_encounter.monster_set.create(**monster_params)
        new_encounter.event_set.create(text=f"You were attacked by {monster.name}!")
        return new_encounter

    def completed_habits(self) -> models.QuerySet["Habit"]:
        return self.character.habit_set.filter(
            pk__in=self.habitdone_set.values("habit_id"),
        )

    def uncompleted_habits(self) -> models.QuerySet["Habit"]:
        return self.character.habit_set.exclude(
            pk__in=self.completed_habits().values("pk"),
        )


@receiver(models.signals.pre_save, sender=CharacterDay)
def regenerate_character_hp_when_created(sender, instance, **kwargs) -> None:
    # Not saved to db yet, this is being created.
    if not instance.pk:
        instance.character.regenerate_daily_health()
        instance.character.save()


@receiver(models.signals.pre_save, sender=CharacterDay)
def adjust_trackers_when_created(sender, instance, **kwargs) -> None:
    # Not saved to db yet, this is being created.
    if not instance.pk:
        for tracker in instance.character.tracker_set.all():
            tracker.adjustment_set.create(value=tracker.daily_adjustment)


@receiver(models.signals.pre_save, sender=CharacterDay)
def fail_past_due_todos_when_created(sender, instance, **kwargs) -> None:
    # Not saved to db yet, this is being created.
    if not instance.pk:
        for past_due in instance.character.past_due_todos():
            past_due.fail()


@receiver(models.signals.pre_save, sender=CharacterDay)
def fail_lapsed_habits_when_created(sender, instance, **kwargs) -> None:
    # Not saved to db yet, this is being created.
    if not instance.pk:
        try:
            yesterday = instance.character.characterday_set.get(
                day=instance.day - timedelta(days=1),
            )
        except CharacterDay.DoesNotExist:
            return
        for uncompleted_habit in yesterday.uncompleted_habits():
            uncompleted_habit.fail()


class CharacterLevelUp(TimeStampedModel):
    INVALID_STAT_ALLOCATION_ERROR_MESSAGE = "Invalid stat allocation"
    COMPLETED_MESSAGE_TEMPLATE = "{name} leveled up!"

    class AlreadyCompletedError(Exception):
        pass

    class StillPendingError(Exception):
        pass

    class Status(models.TextChoices):
        PENDING = "P", "PENDING"
        COMPLETED = "C", "COMPLETED"
        REVERSED = "R", "REVERSED"

    character = models.ForeignKey(
        "Character",
        on_delete=models.CASCADE,
    )
    status = models.CharField(
        choices=Status.choices,
        default=Status.PENDING,
    )
    total_stat_points = models.IntegerField()

    strength_points = models.IntegerField(
        default=0,
        blank=True,
    )
    dexterity_points = models.IntegerField(
        default=0,
        blank=True,
    )
    initiative_points = models.IntegerField(
        default=0,
        blank=True,
    )
    hit_points = models.IntegerField(
        default=0,
        blank=True,
    )

    @property
    def allocated_points(self) -> int:
        return sum(
            [
                self.strength_points,
                self.dexterity_points,
                self.initiative_points,
                self.hit_points,
            ],
        )

    def clean(self):
        result = super().clean()
        if self.allocated_points != self.total_stat_points:
            raise ValidationError(
                CharacterLevelUp.INVALID_STAT_ALLOCATION_ERROR_MESSAGE,
            )
        return result

    def complete(self) -> None:
        if self.status == CharacterLevelUp.Status.COMPLETED:
            raise CharacterLevelUp.AlreadyCompletedError(self.pk)

        self.character.experience_points -= (
            self.character.experience_points_for_next_level
        )
        self.character.level += 1
        self.character.max_hit_points += self.hit_points
        self.character.hit_points += self.hit_points
        self.character.strength += self.strength_points
        self.character.dexterity += self.dexterity_points
        self.character.initiative += self.initiative_points
        self.character.save()

        self.status = CharacterLevelUp.Status.COMPLETED
        self.save()

        message_text = CharacterLevelUp.COMPLETED_MESSAGE_TEMPLATE.format(
            name=self.character.name,
        )
        self.character.add_message(message_text)

    def reverse(self) -> None:
        if self.status != CharacterLevelUp.Status.COMPLETED:
            raise CharacterLevelUp.StillPendingError(self.pk)

        self.character.level -= 1
        self.character.max_hit_points -= self.hit_points
        self.character.strength -= self.strength_points
        self.character.dexterity -= self.dexterity_points
        self.character.initiative -= self.initiative_points
        self.character.save()

        self.status = CharacterLevelUp.Status.REVERSED
        self.save()


class CharacterDeath(TimeStampedModel):
    DEATH_MESSAGE_TEMPLATE = "{name} has died!"

    class Status(models.TextChoices):
        PENDING = "P", "PENDING"
        COMPLETED = "C", "COMPLETED"

    character = models.ForeignKey(
        "Character",
        on_delete=models.CASCADE,
    )
    status = models.CharField(
        max_length=1,
        choices=Status.choices,
        default=Status.PENDING,
    )

    def die(self) -> None:
        self.character.experience_points = 0
        self.character.hit_points = self.character.max_hit_points // 2
        self.character.save()

        level_up = self.character.most_recently_completed_level_up()
        if level_up:
            level_up.reverse()

        message_text = CharacterDeath.DEATH_MESSAGE_TEMPLATE.format(
            name=self.character.name,
        )
        self.character.add_message(text=message_text, level=messages.ERROR)

        self.status = CharacterDeath.Status.COMPLETED
        self.save()
