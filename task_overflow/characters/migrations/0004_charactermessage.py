# Generated by Django 4.2.11 on 2024-08-23 21:48

from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import model_utils.fields


class Migration(migrations.Migration):

    dependencies = [
        ('characters', '0003_character_dexterity'),
    ]

    operations = [
        migrations.CreateModel(
            name='CharacterMessage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', model_utils.fields.AutoCreatedField(default=django.utils.timezone.now, editable=False, verbose_name='created')),
                ('modified', model_utils.fields.AutoLastModifiedField(default=django.utils.timezone.now, editable=False, verbose_name='modified')),
                ('text', models.TextField()),
                ('status', models.CharField(choices=[('P', 'PENDING'), ('V', 'VIEWED')], default='P', max_length=1)),
                ('character', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='characters.character')),
            ],
            options={
                'abstract': False,
            },
        ),
    ]
