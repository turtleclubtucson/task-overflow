from typing import TYPE_CHECKING

from django import template
from django.templatetags.static import static

register = template.Library()

if TYPE_CHECKING:
    from task_overflow.characters.models import Character


DEFAULT_AVATAR = "images/default-avatar.gif"


@register.filter(name="character_avatar_url")
def character_avatar_url(character: "Character") -> str:
    if character.avatar:
        return character.avatar.url
    return static(DEFAULT_AVATAR)
