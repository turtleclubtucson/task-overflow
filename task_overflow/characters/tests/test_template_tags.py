import re
from pathlib import Path
from typing import TYPE_CHECKING

import pytest
from django.core.files import File

from task_overflow.characters.templatetags.character_avatar_url import DEFAULT_AVATAR
from task_overflow.characters.templatetags.character_avatar_url import (
    character_avatar_url,
)
from tests.data import TEST_IMAGE_FILE

from .factories import CharacterFactory

if TYPE_CHECKING:
    from task_overflow.characters.models import Character
    from task_overflow.users.models import User


@pytest.mark.usefixtures("db")
def test_character_avatar_url_returns_avatar_url_if_set(user1: "User"):
    with TEST_IMAGE_FILE.open(mode="rb") as f:
        character = CharacterFactory(user=user1, avatar=File(f, name="avi.jpg"))
    assert character_avatar_url(character) == character.avatar.url


def test_character_avatar_url_returns_default_avatar_url_if_no_avatar_is_set(
    settings,
    user1_character: "Character",
):
    character = user1_character

    img_path = Path(DEFAULT_AVATAR)
    img_name = img_path.stem
    img_ext = img_path.suffix
    pattern = re.compile(f"{settings.STATIC_URL}images/{img_name}.(\\w)+{img_ext}")
    assert pattern.match(character_avatar_url(character))
