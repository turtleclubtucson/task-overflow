from http import HTTPStatus
from typing import TYPE_CHECKING

import pytest
from django.urls import reverse

from task_overflow.characters.models import NEW_ENCOUNTER_VALUE
from task_overflow.characters.models import CharacterDeath
from task_overflow.characters.models import CharacterLevelUp
from task_overflow.characters.models import CharacterMessage
from tests.utils import is_login_redirect
from tests.utils import is_redirect

from .factories import CharacterDeathFactory
from .factories import CharacterMessageFactory

if TYPE_CHECKING:
    from django.test.client import Client

    from task_overflow.characters.models import Character
    from task_overflow.users.models import User


def test_create_character_view_loads_for_logged_in_user(
    user1: "User",
    user1_client: "Client",
):
    resp = user1_client.get(reverse("characters:create"))
    assert resp.status_code == HTTPStatus.OK
    assert user1.email.encode() in resp.content


@pytest.mark.django_db()
def test_create_character_view_does_not_load_for_anon_user(
    client: "Client",
):
    resp = client.get(reverse("characters:create"))
    assert is_login_redirect(resp)


def test_create_character_view_saves_for_logged_in_user(
    user1: "User",
    user1_client: "Client",
):
    user = user1
    data = {"name": "Krux Aftershave"}
    resp = user1_client.post(reverse("characters:create"), data=data)
    assert is_redirect(
        resp,
        for_path=reverse(
            "characters:detail",
            kwargs={"pk": user.character_set.first().pk},
        ),
    )

    assert user1.character_set.count() == 1
    assert user1.character_set.filter(name="Krux Aftershave").exists()


@pytest.mark.django_db()
def test_create_character_view_does_not_save_for_anon_user(
    client: "Client",
):
    data = {"name": "Krux Aftershave"}
    resp = client.post(reverse("characters:create"), data=data)
    assert is_login_redirect(resp)


def test_character_update_view_loads_for_logged_in_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character

    resp = user1_client.get(
        reverse(
            "characters:update",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert character.name.encode() in resp.content


def test_character_update_view_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character

    resp = user2_client.get(
        reverse(
            "characters:update",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_update_view_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character

    resp = client.get(
        reverse(
            "characters:update",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_character_update_view_saves_for_logged_in_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    data = {
        "name": "Updated Name",
    }

    resp = user1_client.post(
        reverse(
            "characters:update",
            kwargs={
                "pk": character.pk,
            },
        ),
        data=data,
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    character.refresh_from_db()
    assert character.name == "Updated Name"


def test_character_update_view_does_not_save_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    original_name = character.name
    data = {
        "name": "Updated Name",
    }

    resp = user2_client.post(
        reverse(
            "characters:update",
            kwargs={
                "pk": character.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Was not updated.
    character.refresh_from_db()
    assert character.name == original_name


def test_character_update_view_does_not_save_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    original_name = character.name
    data = {
        "name": "Updated Name",
    }

    resp = client.post(
        reverse(
            "characters:update",
            kwargs={
                "pk": character.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(resp)

    # Was not updated.
    character.refresh_from_db()
    assert character.name == original_name


def test_character_detail_view_loads_for_logged_in_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character

    resp = user1_client.get(reverse("characters:detail", kwargs={"pk": character.pk}))
    assert character.name.encode() in resp.content
    assert str(character.experience_points).encode() in resp.content


def test_character_detail_view_shows_pending_character_messages(
    user1_character_message: "CharacterMessage",
    user1_client: "Client",
):
    message = user1_character_message
    character = message.character

    resp = user1_client.get(reverse("characters:detail", kwargs={"pk": character.pk}))
    assert character.name.encode() in resp.content
    assert message.text.encode() in resp.content
    assert str(character.experience_points).encode() in resp.content

    # Message gets marked as viewed.
    message.refresh_from_db()
    assert message.status == CharacterMessage.Status.VIEWED


def test_character_detail_view_runs_pending_character_deaths(
    user1_character_message: "CharacterMessage",
    user1_client: "Client",
):
    message = user1_character_message
    character = message.character
    CharacterDeathFactory(character=character, status=CharacterDeath.Status.PENDING)

    resp = user1_client.get(reverse("characters:detail", kwargs={"pk": character.pk}))
    message = CharacterDeath.DEATH_MESSAGE_TEMPLATE.format(name=character.name)
    assert message.encode() in resp.content


def test_character_detail_view_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    resp = user2_client.get(
        reverse(
            "characters:detail",
            kwargs={"pk": user1_character.pk},
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_detail_view_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    resp = client.get(
        reverse(
            "characters:detail",
            kwargs={"pk": user1_character.pk},
        ),
    )
    assert is_login_redirect(resp)


def test_character_hx_fetch_messages_view_loads_for_logged_in_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    # Pending messages are included.
    CharacterMessageFactory(character=character, text="hello")
    # Already seen messages are not included.
    CharacterMessageFactory(
        character=character,
        text="goodbye",
        status=CharacterMessage.Status.VIEWED,
    )
    resp = user1_client.get(
        reverse(
            "characters:character-hx-fetch-messages",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert b"hello" in resp.content
    assert b"goodbye" not in resp.content


def test_character_hx_fetch_messages_view_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    # Pending messages do not leak.
    CharacterMessageFactory(character=character, text="hello")
    resp = user2_client.get(
        reverse(
            "characters:character-hx-fetch-messages",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND
    assert b"hello" not in resp.content


def test_character_hx_fetch_messages_view_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    # Pending messages do not leak.
    CharacterMessageFactory(character=character, text="hello")
    resp = client.get(
        reverse(
            "characters:character-hx-fetch-messages",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )
    assert b"hello" not in resp.content


def test_character_hx_check_for_active_encounter_view_when_no_encounter(
    mocker,
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    # Do not roll for new active encounter.
    mocker.patch("random.randint", return_value=character.level + 1)
    resp = user1_client.get(
        reverse(
            "characters:character-hx-check-for-active-encounter",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert not resp.content


def test_character_hx_check_for_active_encounter_view_when_active_encounter_exists(
    mocker,
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    # Roll for new active encounter.
    mocker.patch("random.randint", return_value=NEW_ENCOUNTER_VALUE)
    user = character.user
    resp = user1_client.get(
        reverse(
            "characters:character-hx-check-for-active-encounter",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    encounter = user.encounters().first()
    assert is_redirect(
        resp,
        for_path=reverse(
            "encounters:detail",
            kwargs={
                "pk": encounter.pk,
            },
        ),
        for_htmx=True,
    )


def test_character_hx_check_for_active_encounter_view_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character
    resp = user2_client.get(
        reverse(
            "characters:character-hx-check-for-active-encounter",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_hx_check_for_active_encounter_view_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character
    resp = client.get(
        reverse(
            "characters:character-hx-check-for-active-encounter",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_character_hx_fetch_details_view_loads_for_logged_in_owner(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character

    resp = user1_client.get(
        reverse(
            "characters:character-hx-fetch-details",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert character.name.encode() in resp.content
    assert str(character.experience_points).encode() in resp.content


def test_character_hx_fetch_details_view_runs_pending_character_deaths(
    user1_character: "Character",
    user1_client: "Client",
):
    character = user1_character
    death = CharacterDeathFactory(
        character=character,
        status=CharacterDeath.Status.PENDING,
    )

    resp = user1_client.get(
        reverse(
            "characters:character-hx-fetch-details",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK

    # Death was completed.
    death.refresh_from_db()
    assert death.status == CharacterDeath.Status.COMPLETED


def test_character_hx_fetch_details_view_does_not_load_for_non_owner(
    user1_character: "Character",
    user2_client: "Client",
):
    character = user1_character

    resp = user2_client.get(
        reverse(
            "characters:character-hx-fetch-details",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_hx_fetch_details_view_does_not_load_for_anon_user(
    user1_character: "Character",
    client: "Client",
):
    character = user1_character

    resp = client.get(
        reverse(
            "characters:character-hx-fetch-details",
            kwargs={
                "pk": character.pk,
            },
        ),
    )
    assert is_login_redirect(
        resp,
        for_htmx=True,
    )


def test_character_level_up_complete_view_loads_for_logged_in_user(
    user1_character_level_up: "CharacterLevelUp",
    user1_client: "Client",
):
    level_up = user1_character_level_up

    resp = user1_client.get(
        reverse(
            "characters:level-up-complete",
            kwargs={
                "pk": level_up.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.OK
    assert str(level_up.total_stat_points).encode() in resp.content


@pytest.mark.usefixtures("_user1_character_level_up_was_completed")
def test_character_level_up_complete_view_does_not_load_if_level_up_already_completed(
    user1_character_level_up: "CharacterLevelUp",
    user1_client: "Client",
):
    level_up = user1_character_level_up

    resp = user1_client.get(
        reverse(
            "characters:level-up-complete",
            kwargs={
                "pk": level_up.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_level_up_complete_view_does_not_load_for_non_owner(
    user1_character_level_up: "CharacterLevelUp",
    user2_client: "Client",
):
    level_up = user1_character_level_up

    resp = user2_client.get(
        reverse(
            "characters:level-up-complete",
            kwargs={
                "pk": level_up.pk,
            },
        ),
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND


def test_character_level_up_complete_view_does_not_load_for_anon_user(
    user1_character_level_up: "CharacterLevelUp",
    client: "Client",
):
    level_up = user1_character_level_up

    resp = client.get(
        reverse(
            "characters:level-up-complete",
            kwargs={
                "pk": level_up.pk,
            },
        ),
    )
    assert is_login_redirect(resp)


def test_character_level_up_complete_view_saves_for_logged_in_user(
    user1_character_level_up: "CharacterLevelUp",
    user1_client: "Client",
):
    level_up = user1_character_level_up
    character = level_up.character
    data = {"strength_points": level_up.total_stat_points}

    resp = user1_client.post(
        reverse(
            "characters:level-up-complete",
            kwargs={
                "pk": level_up.pk,
            },
        ),
        data=data,
    )
    assert is_redirect(resp, for_path=character.get_absolute_url())

    level_up.refresh_from_db()
    assert level_up.status == CharacterLevelUp.Status.COMPLETED


@pytest.mark.usefixtures("_user1_character_level_up_was_completed")
def test_character_level_up_complete_view_does_not_save_for_already_completed(
    user1_character_level_up: "CharacterLevelUp",
    user1_client: "Client",
):
    level_up = user1_character_level_up
    data = {"strength_points": level_up.total_stat_points}

    resp = user1_client.post(
        reverse(
            "characters:level-up-complete",
            kwargs={
                "pk": level_up.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Level up is still completed.
    level_up.refresh_from_db()
    assert level_up.status == CharacterLevelUp.Status.COMPLETED


def test_character_level_up_complete_view_does_not_save_for_non_owner(
    user1_character_level_up: "CharacterLevelUp",
    user2_client: "Client",
):
    level_up = user1_character_level_up
    data = {"strength_points": level_up.total_stat_points}

    resp = user2_client.post(
        reverse(
            "characters:level-up-complete",
            kwargs={
                "pk": level_up.pk,
            },
        ),
        data=data,
    )
    assert resp.status_code == HTTPStatus.NOT_FOUND

    # Level up was not completed.
    level_up.refresh_from_db()
    assert level_up.status != CharacterLevelUp.Status.COMPLETED


def test_character_level_up_complete_view_does_not_save_for_anon_user(
    user1_character_level_up: "CharacterLevelUp",
    client: "Client",
):
    level_up = user1_character_level_up
    data = {"strength_points": level_up.total_stat_points}

    resp = client.post(
        reverse(
            "characters:level-up-complete",
            kwargs={
                "pk": level_up.pk,
            },
        ),
        data=data,
    )
    assert is_login_redirect(resp)

    # Level up was not completed.
    level_up.refresh_from_db()
    assert level_up.status != CharacterLevelUp.Status.COMPLETED
