from datetime import date
from datetime import timedelta
from typing import TYPE_CHECKING

import pytest
from django.contrib import messages
from django.core.exceptions import ValidationError
from django.utils import timezone

from task_overflow.characters.models import NEW_ENCOUNTER_VALUE
from task_overflow.characters.models import Character
from task_overflow.characters.models import CharacterDeath
from task_overflow.characters.models import CharacterLevelUp
from task_overflow.characters.models import CharacterMessage
from task_overflow.characters.tests.factories import CharacterDayFactory
from task_overflow.characters.tests.factories import CharacterDeathFactory
from task_overflow.characters.tests.factories import CharacterFactory
from task_overflow.characters.tests.factories import CharacterLevelUpFactory
from task_overflow.characters.tests.factories import CharacterMessageFactory
from task_overflow.encounters.tests.factories import EncounterFactory
from task_overflow.items.tests.factories import CharacterItemFactory
from task_overflow.items.tests.factories import ItemFactory
from task_overflow.tasks.models import Habit
from task_overflow.tasks.models import Todo
from task_overflow.tasks.tests.factories import CategoryFactory
from task_overflow.tasks.tests.factories import HabitDoneFactory
from task_overflow.tasks.tests.factories import HabitFactory
from task_overflow.tasks.tests.factories import TodoFactory
from task_overflow.trackers.tests.factories import TrackerFactory

if TYPE_CHECKING:
    from task_overflow.characters.models import CharacterDay
    from task_overflow.encounters.models import Encounter
    from task_overflow.items.models import Item
    from task_overflow.users.models import User


def test_character_create_with_default_items_creates_character_with_expected_items(
    fists: "Item",
    user1: "User",
):
    character = Character.objects.create_with_default_items(user=user1, name="Example")
    assert character.characteritem_set.count() == 1
    assert character.characteritem_set.filter(item=fists).exists()


def test_character_xp_does_not_drop_below_zero(user1_character: "Character"):
    character = user1_character
    character.experience_points = -1
    character.save()
    character.refresh_from_db()
    assert character.experience_points == 0


def test_character_death_created_when_hit_points_drop_to_zero(
    user1_character: "Character",
):
    character = user1_character

    character.hit_points = 0
    character.save()

    assert character.characterdeath_set.count() == 1


def test_character_will_create_level_up_if_experience_points_exceed_threshold(
    user1_character: "Character",
):
    character = user1_character

    character.experience_points = ((character.level + 1) * 1000) + 1
    character.save()

    assert character.characterlevelup_set.count() == 1
    assert character.characterlevelup_set.filter(
        total_stat_points=character.level + 1,
    ).exists()
    # Pending level up message is created.
    assert (
        character.pending_messages()
        .filter(
            text=Character.READY_TO_LEVEL_UP_MESSAGE_TEMPLATE.format(
                name=character.name,
            ),
        )
        .exists()
    )


def test_character_will_not_create_level_up_if_xp_exceeds_threshold_but_one_exists(
    user1_character_level_up: "CharacterLevelUp",
):
    level_up = user1_character_level_up
    character = level_up.character

    character.experience_points = character.experience_points_for_next_level + 1
    character.save()

    # No new level up was created.
    assert character.characterlevelup_set.count() == 1
    assert character.characterlevelup_set.first() == level_up


def test_character_hit_points_will_not_exceed_maximum(
    user1_character: "Character",
):
    character = user1_character

    expected_hp = character.max_hit_points
    character.hit_points = character.max_hit_points + 1
    character.save()

    assert character.hit_points == expected_hp


def test_character_daily_regenerated_hit_points_calculates_based_on_level(
    user1: "User",
):
    character = CharacterFactory(user=user1, level=2)
    # Level + 1
    assert character.daily_regenerated_hit_points == 3


def test_character_maximum_daily_encounters_calculates_based_on_level(user1: "User"):
    character = CharacterFactory(user=user1, level=3)
    assert character.maximum_daily_encounters == 4


def test_character_experience_points_for_next_level_calculates_based_on_level(
    user1: "User",
):
    character = CharacterFactory(user=user1, level=3)
    # 1000 + (level x 100)
    assert character.experience_points_for_next_level == 1300


def test_character_weapon_chooses_equipped_melee_weapon(
    fists: "Item",
    item: "Item",
    knife: "Item",
    user1_character: "Character",
):
    character = user1_character
    # Non-melee weapon item will not be chosen.
    CharacterItemFactory(character=character, item=item)

    # Not equipped melee weapon will not be chosen.
    CharacterItemFactory(
        character=character,
        item=knife,
        is_equipped=False,
    )

    # Equipped melee weapon will be chosen.
    CharacterItemFactory(
        character=character,
        item=fists,
        is_equipped=True,
    )

    assert character.weapon == fists


def test_character_is_active_when_no_character_deaths_exist(
    user1_character: "Character",
):
    character = user1_character
    assert character.is_active is True


def test_character_is_active_when_only_completed_character_deaths_exist(
    user1_character: "Character",
):
    character = user1_character
    CharacterDeathFactory(
        character=character,
        status=CharacterDeath.Status.COMPLETED,
    )
    assert character.is_active is True


def test_character_is_active_when_only_pending_character_death_exists(
    user1_character: "Character",
):
    character = user1_character
    CharacterDeathFactory(
        character=character,
        status=CharacterDeath.Status.COMPLETED,
    )
    CharacterDeathFactory(
        character=character,
        status=CharacterDeath.Status.PENDING,
    )
    assert character.is_active is False


@pytest.mark.usefixtures("user2_character_item")
def test_character_available_character_items_only_includes_usable_character_items_left(
    small_med_pack: "Item",
    user1_character: "Character",
):
    character = user1_character
    character_item = CharacterItemFactory(
        item=small_med_pack,
        character=character,
        quantity=1,
    )
    # Items character is out of are not included.
    CharacterItemFactory(
        item=small_med_pack,
        character=character,
        quantity=0,
    )
    # Items without single use adjustments not included.
    non_adjusting_item = ItemFactory()
    CharacterItemFactory(
        item=non_adjusting_item,
        character=character,
        quantity=1,
    )
    assert list(character.available_character_items()) == [character_item]


def test_character_melee_weapons_includes_only_melee_weapons_owned_by_character(
    fists: "Item",
    item: "Item",
    user1_character: "Character",
    user2_character: "Character",
):
    character = user1_character
    character_melee = CharacterItemFactory(character=character, item=fists)

    # Non melee weapon character item not included.
    character_potion = CharacterItemFactory(
        character=character,
        item=item,
    )
    # Other user melee weapon not included.
    other_character_melee = CharacterItemFactory(
        character=user2_character,
        item=fists,
    )

    assert character_melee in character.melee_weapons()
    assert character_potion not in character.melee_weapons()
    assert other_character_melee not in character.melee_weapons()


def test_character_ordered_categories_returns_categories_ordered_by_priority(
    user1_character: "Character",
):
    character = user1_character
    low_priority = CategoryFactory(character=character, priority=1)
    high_priority = CategoryFactory(character=character, priority=100)
    assert list(character.ordered_categories()) == [high_priority, low_priority]


def test_character_pending_deaths_includes_only_deaths_not_yet_completed(
    user1_character: "Character",
):
    character = user1_character

    # Already completed death not included.
    CharacterDeathFactory(character=character, status=CharacterDeath.Status.COMPLETED)

    death = CharacterDeathFactory(
        character=character,
        status=CharacterDeath.Status.PENDING,
    )
    assert list(character.pending_deaths()) == [death]


def test_character_ordered_pending_messages_includes_only_messages_not_yet_viewed(
    user1_character: "Character",
):
    character = user1_character
    pending1 = CharacterMessageFactory(
        character=character,
        status=CharacterMessage.Status.PENDING,
    )
    pending2 = CharacterMessageFactory(
        character=character,
        status=CharacterMessage.Status.PENDING,
    )

    # Viewed messages are not included.
    CharacterMessageFactory(
        character=character,
        status=CharacterMessage.Status.VIEWED,
    )

    assert list(character.ordered_pending_messages()) == [pending1, pending2]


@pytest.mark.usefixtures("user2_character_habit")
def test_character_ordered_completed_daily_habits_has_done_habits_by_category(
    user1_character: "Character",
):
    character = user1_character
    low_priority = CategoryFactory(character=character, priority=1)
    high_priority = CategoryFactory(character=character, priority=100)

    low_habit = HabitFactory(
        character=character,
        category=low_priority,
    )
    HabitDoneFactory(habit=low_habit)
    high_habit = HabitFactory(
        character=character,
        category=high_priority,
    )
    HabitDoneFactory(habit=high_habit)
    uncategorized = HabitFactory(character=character)
    HabitDoneFactory(habit=uncategorized)

    # Not done habits are not included.
    HabitFactory(character=character)

    assert list(character.ordered_completed_daily_habits()) == [
        high_habit,
        low_habit,
        uncategorized,
    ]


@pytest.mark.usefixtures("user2_character_habit")
def test_character_ordered_pending_daily_habits_has_not_done_habits_by_category(
    user1_character: "Character",
):
    character = user1_character
    low_priority = CategoryFactory(character=character, priority=1)
    high_priority = CategoryFactory(character=character, priority=100)

    already_done = HabitFactory(character=character)
    HabitDoneFactory(habit=already_done)
    low_habit = HabitFactory(
        character=character,
        category=low_priority,
    )
    high_habit = HabitFactory(
        character=character,
        category=high_priority,
    )
    uncategorized = HabitFactory(character=character)

    assert list(character.ordered_pending_daily_habits()) == [
        high_habit,
        low_habit,
        uncategorized,
    ]


@pytest.mark.usefixtures("user2_character_level_up")
def test_character_pending_level_up_returns_characters_pending_level_up_if_exists(
    user1_character_level_up: "CharacterLevelUp",
):
    level_up = user1_character_level_up
    character = level_up.character
    assert character.pending_level_up() == level_up


@pytest.mark.usefixtures("_user1_character_level_up_was_completed")
def test_character_pending_level_up_returns_none_if_level_up_was_completed(
    user1_character_level_up: "CharacterLevelUp",
):
    level_up = user1_character_level_up
    character = level_up.character
    assert character.pending_level_up() is None


def test_character_pending_level_up_returns_characters_none_if_no_level_up_exists(
    user1_character: "Character",
):
    character = user1_character
    assert character.pending_level_up() is None


def test_character_most_recently_completed_level_up_returns_most_recently_completed(
    user1_character: "Character",
):
    character = user1_character
    CharacterLevelUpFactory(
        character=character,
        status=CharacterLevelUp.Status.COMPLETED,
    )
    level_up = CharacterLevelUpFactory(
        character=character,
        status=CharacterLevelUp.Status.COMPLETED,
    )
    # Pending level up not included
    CharacterLevelUpFactory(
        character=character,
        status=CharacterLevelUp.Status.PENDING,
    )
    # Reversed level up not included
    CharacterLevelUpFactory(
        character=character,
        status=CharacterLevelUp.Status.REVERSED,
    )

    assert character.most_recently_completed_level_up() == level_up


@pytest.mark.usefixtures(
    "_user1_character_todo_is_due_today",
)
def test_character_default_todos_matches_default_todo_query_set_filter(
    user1_character: "Character",
):
    character = user1_character
    default_todos = character.default_todos()
    assert default_todos.exists()
    assert list(default_todos) == list(character.todo_set.default())


@pytest.mark.usefixtures("user2_character_todo")
def test_character_past_due_todos_only_includes_unfailed_todos_with_past_due_dates(
    user1_character: "Character",
):
    character = user1_character
    user = character.user

    # Todos without due date are not included.
    TodoFactory(
        character=user1_character,
    )
    # Todos with due date today ore not included.
    TodoFactory(
        character=user1_character,
        due_date=timezone.now().astimezone(user.timezone).date(),
    )
    # Todos that have already been marked as failed are not included.
    TodoFactory(
        character=user1_character,
        due_date=timezone.now().astimezone(user.timezone).date() - timedelta(days=1),
        is_failed=True,
    )

    # Todo due yesterday or earlier is included if they have already failed.
    past_due = TodoFactory(
        character=user1_character,
        due_date=timezone.now().astimezone(user.timezone).date() - timedelta(days=1),
        is_failed=False,
    )

    assert list(character.past_due_todos()) == [past_due]


def test_character_add_message_creates_character_message_with_text_and_level(
    user1_character: "Character",
):
    character = user1_character

    message = character.add_message(
        text="hello world",
        level=2,
    )
    assert message.character == character
    assert message.text == "hello world"
    assert message.level == 2


def test_character_get_or_create_character_day_creates_todays_day(
    user1_character: "Character",
):
    character = user1_character
    user = character.user

    character_day = character.get_or_create_character_day()
    assert character_day.character == character
    assert character_day.day == timezone.now().astimezone(user.timezone).date()


def test_character_get_or_create_character_day_gets_todays_day_if_exists(
    user1_character_day: "CharacterDay",
):
    character_day = user1_character_day
    character = character_day.character

    assert character.get_or_create_character_day() == character_day


def test_character_active_encounter_returns_existing_active_encounter(
    user1_character_day_encounter: "Encounter",
):
    encounter = user1_character_day_encounter
    character = encounter.character_day.character

    assert character.active_encounter() == encounter


def test_character_active_encounter_returns_none_if_no_encounter_exists(
    user1_character: "Character",
):
    character = user1_character
    assert character.active_encounter() is None


def test_character_get_or_create_active_encounter_returns_existing_active_encounter(
    user1_character_day_encounter: "Encounter",
):
    encounter = user1_character_day_encounter
    character = encounter.character_day.character

    assert character.get_or_create_active_encounter() == encounter


def test_character_regenerate_daily_health_adds_to_hit_points_and_creates_message(
    user1: "User",
):
    character = CharacterFactory(
        user=user1,
        level=10,
        hit_points=1,
    )
    old_hp = character.hit_points
    character.regenerate_daily_health()
    character.refresh_from_db()
    assert character.hit_points > old_hp
    assert character.charactermessage_set.count() == 1
    message = character.charactermessage_set.first()
    assert message.text == Character.DAILY_HEALTH_REGENERATED_MESSAGE_TEMPLATE.format(
        name=character.name,
    )


def test_character_day_regenerates_character_hp_when_created(
    user1_today: date,
    user1_character: "Character",
):
    character = user1_character
    old_hp = character.hit_points

    CharacterDayFactory(
        character=character,
        day=user1_today,
    )

    character.refresh_from_db()
    assert character.hit_points > old_hp


def test_character_day_adjusts_trackers_when_created(
    user1_character: "Character",
):
    character = user1_character
    user = character.user

    tracker = TrackerFactory(
        character=character,
        balance=111,
        daily_adjustment=222,
    )

    CharacterDayFactory(character=character, day=user.today())

    assert tracker.adjustment_set.count() == 1
    assert tracker.adjustment_set.filter(value=222).exists()


def test_character_day_fails_pending_habits_when_created(
    user1_character_habit: "Habit",
):
    habit = user1_character_habit
    character = habit.character
    user = character.user

    # Habit was created long enough ago to fail.
    habit.created = user.now() - timedelta(days=2)
    habit.save()

    # Character day exists for yesterday.
    CharacterDayFactory(
        character=character,
        day=user.today() - timedelta(days=1),
    )
    CharacterDayFactory(
        character=character,
        day=user.today(),
    )

    assert (
        character.pending_messages()
        .filter(text=Habit.FAILED_MESSAGE_TEMPLATE.format(name=habit.name))
        .exists()
    )


def test_character_day_skips_failing_habits_not_old_enough_to_fail(
    user1_character_habit: "Habit",
):
    habit = user1_character_habit
    character = habit.character
    user = character.user

    CharacterDayFactory(
        character=character,
        day=user.today(),
    )

    assert (
        not character.pending_messages()
        .filter(text=Habit.FAILED_MESSAGE_TEMPLATE.format(name=habit.name))
        .exists()
    )


def test_character_day_fails_past_due_todos_when_created(
    user1_character: "Character",
):
    character = user1_character
    user = character.user

    past_due = TodoFactory(
        character=character,
        due_date=timezone.now().astimezone(user.timezone).date() - timedelta(days=1),
    )

    # Character day exists for yesterday.
    CharacterDayFactory(
        character=character,
        day=user.today() - timedelta(days=1),
    )

    past_due.refresh_from_db()
    assert past_due.is_failed is True

    assert (
        character.pending_messages()
        .filter(text=Todo.FAILURE_MESSAGE_TEMPLATE.format(name=past_due.name))
        .exists()
    )


def test_character_day_active_encounter_returns_encounter_if_active_encounter_exists(
    user1_character_day_encounter: "Encounter",
):
    encounter = user1_character_day_encounter
    character_day = encounter.character_day

    assert character_day.active_encounter() == encounter


@pytest.mark.usefixtures("_user1_character_day_encounter_was_completed")
def test_character_day_active_encounter_returns_none_if_encounter_not_active(
    user1_character_day_encounter: "Encounter",
):
    encounter = user1_character_day_encounter
    character_day = encounter.character_day

    assert character_day.active_encounter() is None


def test_character_day_active_encounter_returns_none_if_no_encounter_exists(
    user1_character_day: "CharacterDay",
):
    character_day = user1_character_day
    assert character_day.active_encounter() is None


def test_character_day_get_or_create_active_encounter_returns_encounter_if_exists(
    user1_character_day_encounter: "Encounter",
):
    encounter = user1_character_day_encounter
    character_day = encounter.character_day

    assert character_day.get_or_create_active_encounter() == encounter


def test_character_day_get_or_create_active_encounter_creates_new_encounter_if_rolled(
    mocker,
    user1_character_day: "CharacterDay",
):
    # Rolls to encounter monster.
    mocker.patch("random.randint", return_value=NEW_ENCOUNTER_VALUE)
    character_day = user1_character_day

    encounter = character_day.get_or_create_active_encounter()
    assert encounter.monster_set.count() == 1
    monster = encounter.monster_set.first()
    assert encounter.event_set.count() == 1
    assert monster.name in encounter.event_set.first().text
    assert character_day.encounter_set.count() == 1


def test_character_day_get_or_create_active_encounter_returns_none_if_not_rolled(
    mocker,
    user1_character_day: "CharacterDay",
):
    character = user1_character_day.character

    # Does not roll to encounter monster.
    mocker.patch("random.randint", return_value=character.level + 1)
    character_day = user1_character_day

    assert character_day.get_or_create_active_encounter() is None


def test_character_day_get_or_create_active_encounter_returns_none_if_max_num_reached(
    user1_character_day: "CharacterDay",
):
    character_day = user1_character_day
    character = character_day.character

    # Max encounters already exist.
    EncounterFactory.create_batch(
        character.maximum_daily_encounters,
        character_day=character_day,
        is_active=False,
    )

    assert character_day.get_or_create_active_encounter(force=True)


def test_character_day_get_or_create_active_encounter_can_be_forced_to_create(
    mocker,
    user1_character_day: "CharacterDay",
):
    character_day = user1_character_day
    character = character_day.character

    EncounterFactory.create_batch(
        character.maximum_daily_encounters,
        character_day=character_day,
        is_active=False,
    )

    assert character_day.get_or_create_active_encounter() is None


def test_character_day_completed_habits_returns_only_habits_completed_on_day(
    user1_character_habit: "Habit",
    user1_character_day: "CharacterDay",
):
    habit = user1_character_habit
    character_day = user1_character_day
    character = character_day.character

    yesterday = CharacterDayFactory(
        character=character,
        day=character_day.day - timedelta(days=1),
    )

    # Habits done on that day are included.
    HabitDoneFactory(habit=habit, character_day=character_day)

    # Habits not done on that day are not included.
    not_today = HabitFactory(character=character)
    HabitDoneFactory(habit=not_today, character_day=yesterday)

    # Habits not done at all are not included.
    HabitFactory(character=character)

    assert list(character_day.completed_habits()) == [habit]


def test_character_day_uncompleted_habits_returns_only_habits_not_completed_on_day(
    user1_character_habit: "Habit",
    user1_character_day: "CharacterDay",
):
    habit = user1_character_habit
    character_day = user1_character_day
    character = character_day.character

    yesterday = CharacterDayFactory(
        character=character,
        day=character_day.day - timedelta(days=1),
    )

    # Habits done on that day are not included.
    HabitDoneFactory(habit=habit, character_day=character_day)

    # Habits not done on that day are included.
    not_today = HabitFactory(character=character)
    HabitDoneFactory(habit=not_today, character_day=yesterday)

    # Habits not done at all are included.
    not_ever = HabitFactory(character=character)

    assert list(character_day.uncompleted_habits()) == [not_today, not_ever]


def test_character_level_up_cannot_have_more_points_allocated_than_available(
    user1_character: "Character",
):
    character = user1_character

    level_up = CharacterLevelUpFactory(
        character=character,
        total_stat_points=2,
        strength_points=2,
        dexterity_points=2,
    )
    with pytest.raises(ValidationError):
        level_up.clean()


def test_character_level_up_cannot_have_less_points_allocated_than_available(
    user1_character: "Character",
):
    character = user1_character

    level_up = CharacterLevelUpFactory(
        character=character,
        total_stat_points=2,
        strength_points=2,
        dexterity_points=1,
    )
    with pytest.raises(ValidationError):
        level_up.clean()


def test_character_level_up_can_have_same_amount_of_points_allocated_as_available(
    user1_character: "Character",
):
    character = user1_character

    level_up = CharacterLevelUpFactory(
        character=character,
        total_stat_points=2,
        strength_points=1,
        dexterity_points=1,
    )
    level_up.clean()


def test_character_level_up_complete_allocates_stat_points_increases_level_resets_xp_hp(
    user1: "User",
):
    character = CharacterFactory(
        user=user1,
        level=0,
        experience_points=1001,
        hit_points=1,
        max_hit_points=2,
        strength=1,
        dexterity=1,
        initiative=1,
    )
    level_up = CharacterLevelUpFactory(
        character=character,
        total_stat_points=4,
        hit_points=1,
        strength_points=1,
        dexterity_points=1,
        initiative_points=1,
    )

    level_up.complete()
    level_up.refresh_from_db()

    assert level_up.status == CharacterLevelUp.Status.COMPLETED
    assert character.experience_points == 1
    assert character.level == 1
    assert character.hit_points == 2
    assert character.max_hit_points == 3
    assert character.strength == 2
    assert character.dexterity == 2
    assert character.initiative == 2

    assert (
        character.pending_messages()
        .filter(
            text=CharacterLevelUp.COMPLETED_MESSAGE_TEMPLATE.format(
                name=character.name,
            ),
        )
        .exists()
    )


@pytest.mark.usefixtures("_user1_character_level_up_was_completed")
def test_character_level_up_complete_cannot_be_called_on_already_completed_level_up(
    user1_character_level_up: "CharacterLevelUp",
):
    level_up = user1_character_level_up
    with pytest.raises(CharacterLevelUp.AlreadyCompletedError):
        level_up.complete()


def test_character_level_up_reverse_deducts_points_from_character(user1: "User"):
    character = CharacterFactory(
        user=user1,
        level=3,
        max_hit_points=2,
        strength=2,
        dexterity=2,
        initiative=2,
    )
    level_up = CharacterLevelUpFactory(
        character=character,
        total_stat_points=2,
        hit_points=1,
        strength_points=1,
        dexterity_points=1,
        initiative_points=1,
        status=CharacterLevelUp.Status.COMPLETED,
    )

    level_up.reverse()

    level_up.refresh_from_db()
    assert level_up.status == CharacterLevelUp.Status.REVERSED

    character.refresh_from_db()
    assert character.level == 2
    assert character.max_hit_points == 1
    assert character.strength == 1
    assert character.dexterity == 1
    assert character.initiative == 1


def test_character_level_up_reverse_cannot_be_called_on_pending_level_up(
    user1_character: "Character",
):
    character = user1_character
    level_up = CharacterLevelUpFactory(
        character=character,
        status=CharacterLevelUp.Status.PENDING,
    )

    with pytest.raises(CharacterLevelUp.StillPendingError):
        level_up.reverse()


def test_character_death_die_resets_character_xp_hp_reverses_level_up_creates_message(
    user1: "User",
):
    character = CharacterFactory(
        user=user1,
        hit_points=1,
        experience_points=123,
        level=3,
    )
    # Completed level up exists to reverse.
    CharacterLevelUpFactory(
        character=character,
        status=CharacterLevelUp.Status.COMPLETED,
    )
    character_death = CharacterDeathFactory(character=character)
    character_death.die()
    character.refresh_from_db()

    # Level up was reversed.
    assert character.level == 2

    # HP and XP were reset.
    assert character.hit_points == character.max_hit_points // 2
    assert character.experience_points == 0

    # Message was added.
    assert (
        character.pending_messages()
        .filter(
            text=CharacterDeath.DEATH_MESSAGE_TEMPLATE.format(name=character.name),
            level=messages.ERROR,
        )
        .exists()
    )

    # Character death status was set.
    assert character_death.status == CharacterDeath.Status.COMPLETED
