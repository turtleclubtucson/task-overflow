from factory import Faker
from factory.django import DjangoModelFactory

from task_overflow.characters.models import Character
from task_overflow.characters.models import CharacterDay
from task_overflow.characters.models import CharacterDeath
from task_overflow.characters.models import CharacterLevelUp
from task_overflow.characters.models import CharacterMessage


class CharacterFactory(DjangoModelFactory):
    name = Faker("name")
    experience_points = Faker(
        "pyint",
        min_value=1,
        max_value=10,
    )
    hit_points = Faker(
        "pyint",
        min_value=10,
        max_value=15,
    )
    max_hit_points = Faker(
        "pyint",
        min_value=20,
        max_value=30,
    )
    level = Faker(
        "pyint",
        min_value=5,
        max_value=10,
    )
    credits = Faker(
        "pyint",
        min_value=12345,
        max_value=54321,
    )

    class Meta:
        model = Character


class CharacterMessageFactory(DjangoModelFactory):
    text = Faker("name")

    class Meta:
        model = CharacterMessage


class CharacterDayFactory(DjangoModelFactory):
    class Meta:
        model = CharacterDay


class CharacterLevelUpFactory(DjangoModelFactory):
    total_stat_points = Faker(
        "pyint",
        min_value=1,
        max_value=2,
    )

    class Meta:
        model = CharacterLevelUp


class CharacterDeathFactory(DjangoModelFactory):
    class Meta:
        model = CharacterDeath
