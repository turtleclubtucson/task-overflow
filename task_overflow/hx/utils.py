from django.http import HttpResponse
from django.http import HttpResponseRedirect


def htmx_redirect(
    for_path: str = "",
    response: "HttpResponse | HttpResponseRedirect | None" = None,
) -> HttpResponse:
    if response:
        for_path = for_path or response.url
    resp = response or HttpResponse()
    resp.headers["HX-Redirect"] = for_path
    return resp


def htmx_trigger(
    *trigger_names,
    response: "HttpResponse | None" = None,
) -> "HttpResponse":
    resp = response or HttpResponse()
    resp.headers["HX-Trigger"] = ",".join(trigger_names)
    return resp
