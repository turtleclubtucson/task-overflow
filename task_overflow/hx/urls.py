from django.urls import path

from .views import empty_view

app_name = "hx"
urlpatterns = [
    path("empty/", empty_view, name="empty"),
]
