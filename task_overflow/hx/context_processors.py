from .triggers import CHARACTER_DETAILS_CHANGED
from .triggers import TODO_ADDED


def hx_triggers(request) -> dict:
    return {
        CHARACTER_DETAILS_CHANGED: CHARACTER_DETAILS_CHANGED,
        TODO_ADDED: TODO_ADDED,
    }
