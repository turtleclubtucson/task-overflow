from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.urls import reverse

from task_overflow.mixins import DefaultFormViewMixin

from .utils import htmx_redirect
from .utils import htmx_trigger


class HXLoginRequiredMixin(LoginRequiredMixin):
    def handle_no_permission(self):
        return htmx_redirect(response=super().handle_no_permission())


class HXFormViewMixin(DefaultFormViewMixin):
    hx_template_name = None
    hx_triggers = None
    hx_redirect = None

    def get_context_data(self, *args, **kwargs) -> dict:
        context = super().get_context_data(*args, **kwargs)
        context["hx_target_id"] = self.hx_target_id
        context["hx_post"] = self.request.path
        context["cancel_path"] = getattr(self, "cancel_path", reverse("hx:empty"))
        return context

    def form_valid(self, form):
        resp = super().form_valid(form)
        if self.hx_template_name:
            resp = render(
                self.request,
                self.hx_template_name,
                context={
                    self.get_context_object_name(self.object): self.object,
                },
            )
        if self.hx_triggers:
            resp = htmx_trigger(*self.hx_triggers, response=resp)
        if self.hx_redirect:
            resp = htmx_redirect(response=resp)
        return resp

    def get_success_url(self) -> str:
        # Hacky: generic views often expect success URLs, but HTMX views don't use
        # traditional redirects. Define a success URL even though it isn't used.
        return "/placeholder/"
