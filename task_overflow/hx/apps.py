from django.apps import AppConfig


class HxConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "task_overflow.hx"
