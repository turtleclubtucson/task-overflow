import functools
from http import HTTPStatus

from django.contrib.auth.decorators import login_required

from .utils import htmx_redirect


def hx_login_required(function):
    @functools.wraps(function)
    def hx_login_required_inner(request, *args, **kwargs):
        resp = login_required(function)(request, *args, **kwargs)
        if resp.status_code == HTTPStatus.FOUND:
            return htmx_redirect(response=resp)
        return resp

    return hx_login_required_inner
