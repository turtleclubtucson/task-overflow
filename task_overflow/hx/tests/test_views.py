from http import HTTPStatus
from typing import TYPE_CHECKING

import pytest
from django.urls import reverse

if TYPE_CHECKING:
    from django.test.client import Client


@pytest.mark.usefixtures("db")
def test_hx_empty_returns_empty_response(client: "Client"):
    resp = client.get(reverse("hx:empty"))
    assert resp.status_code == HTTPStatus.OK
    assert not resp.content
