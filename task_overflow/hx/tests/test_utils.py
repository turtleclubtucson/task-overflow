from typing import TYPE_CHECKING

import pytest
from django.shortcuts import redirect
from django.urls import reverse

from task_overflow.hx.utils import htmx_redirect
from task_overflow.hx.utils import htmx_trigger

if TYPE_CHECKING:
    from django.test.client import Client


def test_htmx_redirect_creates_redirect_for_supplied_path():
    resp = htmx_redirect("/foo/bar/")
    assert resp.headers["HX-Redirect"] == "/foo/bar/"


def test_htmx_redirect_creates_redirect_for_supplied_response_from_existing_redirect():
    resp = redirect("/foo/bar/")
    resp = htmx_redirect(response=resp)
    assert resp.headers["HX-Redirect"] == "/foo/bar/"


@pytest.mark.usefixtures("db")
def test_htmx_redirect_creates_redirect_for_supplied_response_from_supplied_path(
    client: "Client",
):
    resp = client.get(reverse("pages:home"))
    resp = htmx_redirect(
        for_path="/foo/bar/",
        response=resp,
    )
    assert resp.headers["HX-Redirect"] == "/foo/bar/"


def test_htmx_redirect_for_supplied_response_specified_path_takes_priority():
    resp = redirect("/hello/world/")
    resp = htmx_redirect(
        for_path="/foo/bar/",
        response=resp,
    )
    assert resp.headers["HX-Redirect"] == "/foo/bar/"


def test_htmx_trigger_creates_response_with_trigger_header_set():
    resp = htmx_trigger("foo")
    assert resp.headers["HX-Trigger"] == "foo"
