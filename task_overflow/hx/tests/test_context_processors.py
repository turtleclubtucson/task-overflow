from task_overflow.hx.context_processors import hx_triggers
from task_overflow.hx.triggers import CHARACTER_DETAILS_CHANGED
from task_overflow.hx.triggers import TODO_ADDED

FAKE_REQUEST = None


def test_hx_triggers_contains_all_hx_triggers():
    assert hx_triggers(FAKE_REQUEST) == {
        CHARACTER_DETAILS_CHANGED: CHARACTER_DETAILS_CHANGED,
        TODO_ADDED: TODO_ADDED,
    }
