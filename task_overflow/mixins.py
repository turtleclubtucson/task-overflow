import contextlib
import hashlib
import time
from random import SystemRandom
from typing import TYPE_CHECKING

from django.shortcuts import get_object_or_404

if TYPE_CHECKING:
    from task_overflow.characters.models import Character


salter = SystemRandom()


def generate_key() -> str:
    ts = str(time.time())
    tok = ts + str(salter.random())
    return hashlib.sha256(tok.encode()).hexdigest()


class CharacterRelatedDataViewMixin:
    redirect_to_character_on_success = False

    def character(self) -> "Character":
        try:
            return self.object.character
        except AttributeError:
            return self._get_character_from_url_kwargs()

    def _get_character_from_url_kwargs(self) -> "Character":
        return get_object_or_404(
            self.request.user.character_set.all(),
            pk=self.kwargs["character"],
        )

    def get_context_data(self, *args, **kwargs) -> dict:
        context = super().get_context_data(*args, **kwargs)
        context["character"] = self.character()
        return context

    def get_success_url(self) -> str:
        if self.redirect_to_character_on_success:
            return self.character().get_absolute_url()
        return super().get_success_url()


class DefaultFormViewMixin:
    template_name = "default_form.html"
    cancel_path = None
    cancel_path_lookup = None
    form_instance_assignments = None

    def get_context_data(self, *args, **kwargs) -> dict:
        context = super().get_context_data(*args, **kwargs)
        context["cancel_path"] = self.get_cancel_path()
        with contextlib.suppress(AttributeError):
            context["form_header"] = self.form_header
        with contextlib.suppress(AttributeError):
            context["form_message"] = self.form_message
        return context

    def get_cancel_path(self) -> str | None:
        if self.cancel_path:
            return self.cancel_path
        lookup = getattr(self, "cancel_path_lookup", None)
        if lookup:
            return self._follow_lookup(lookup)
        return None

    def form_valid(self, form):
        assignments = getattr(self, "form_instance_assignments", None) or {}
        for attr_name, lookup in assignments.items():
            setattr(form.instance, attr_name, self._follow_lookup(lookup))
        return super().form_valid(form)

    def _follow_lookup(self, remaining: str, current=None):
        current = current or self
        first, *rest = remaining.split(".")
        value = getattr(current, first)
        if callable(value):
            value = value()
        current = value
        if rest:
            remaining = ".".join(rest)
            return self._follow_call_path(remaining, current)
        return current


class UserFormViewMixin:
    def get_form_kwargs(self, *args, **kwargs) -> dict:
        form_kwargs = super().get_form_kwargs(*args, **kwargs)
        form_kwargs["user"] = self.request.user
        return form_kwargs
